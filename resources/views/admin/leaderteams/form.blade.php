 <label for="desktop_banner" style="font-size:18px;">{{ 'Desktop Banner' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="desktop_banner" type="file" id="desktop_banner" value="{{ isset($leaderteam->desktop_banner) ? $leaderteam->desktop_banner : ''}}" >
     {!! $errors->first('desktop_banner', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($leaderteam->desktop_banner) && !empty($leaderteam->desktop_banner))
      <img src="{{asset($leaderteam->desktop_banner)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="mobile_banner" style="font-size:18px;">{{ 'Mobile Banner' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="mobile_banner" type="file" id="mobile_banner" value="{{ isset($leaderteam->mobile_banner) ? $leaderteam->mobile_banner : ''}}" >
     {!! $errors->first('mobile_banner', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($leaderteam->mobile_banner) && !empty($leaderteam->mobile_banner))
      <img src="{{asset($leaderteam->mobile_banner)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="team_page_description" style="font-size:18px;">{{ 'Team Page Description' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="team_page_description" type="textarea" id="team_page_description" >{{ isset($leaderteam->team_page_description) ? $leaderteam->team_page_description : ''}}</textarea>
     {!! $errors->first('team_page_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="meta_title" style="font-size:18px;">{{ 'Meta Title' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_title" type="text" id="meta_title" value="{{ isset($leaderteam->meta_title) ? $leaderteam->meta_title : ''}}" >
     {!! $errors->first('meta_title', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="meta_keyword" style="font-size:18px;">{{ 'Meta Keyword' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_keyword" type="text" id="meta_keyword" value="{{ isset($leaderteam->meta_keyword) ? $leaderteam->meta_keyword : ''}}" >
     {!! $errors->first('meta_keyword', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="meta_description" style="font-size:18px;">{{ 'Meta Description' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_description" type="text" id="meta_description" value="{{ isset($leaderteam->meta_description) ? $leaderteam->meta_description : ''}}" >
     {!! $errors->first('meta_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="meta_image" style="font-size:18px;">{{ 'Meta Image' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_image" type="file" id="meta_image" value="{{ isset($leaderteam->meta_image) ? $leaderteam->meta_image : ''}}" >
     {!! $errors->first('meta_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($leaderteam->meta_image) && !empty($leaderteam->meta_image))
      <img src="{{asset($leaderteam->meta_image)}}" height="100" width="100">
      @endif
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>

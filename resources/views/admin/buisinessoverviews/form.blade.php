 <label for="desktop_banner" style="font-size:18px;">{{ 'Desktop Banner' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="desktop_banner" type="file" id="desktop_banner" value="{{ isset($buisinessoverview->desktop_banner) ? $buisinessoverview->desktop_banner : ''}}" >
     {!! $errors->first('desktop_banner', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($buisinessoverview->desktop_banner))
      <img src="{{asset($buisinessoverview->desktop_banner)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="mobile_banner" style="font-size:18px;">{{ 'Mobile Banner' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="mobile_banner" type="file" id="mobile_banner" value="{{ isset($buisinessoverview->mobile_banner) ? $buisinessoverview->mobile_banner : ''}}" >
     {!! $errors->first('mobile_banner', '<p class="help-block">:message</p>') !!}
      </div>
       @if(isset($buisinessoverview->mobile_banner))
      <img src="{{asset($buisinessoverview->mobile_banner)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="overview_part" style="font-size:18px;">{{ 'Overview Part' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="overview_part" type="textarea" id="overview_part" >{{ isset($buisinessoverview->overview_part) ? $buisinessoverview->overview_part : ''}}</textarea>
     {!! $errors->first('overview_part', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="Group_in_numbers_remark" style="font-size:18px;">{{ 'Group In Numbers Remark' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="Group_in_numbers_remark" type="text" id="Group_in_numbers_remark" value="{{ isset($buisinessoverview->Group_in_numbers_remark) ? $buisinessoverview->Group_in_numbers_remark : ''}}" >
     {!! $errors->first('Group_in_numbers_remark', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="meta_title" style="font-size:18px;">{{ 'Meta Title' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_title" type="text" id="meta_title" value="{{ isset($buisinessoverview->meta_title) ? $buisinessoverview->meta_title : ''}}" >
     {!! $errors->first('meta_title', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="meta_keyword" style="font-size:18px;">{{ 'Meta Keyword' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_keyword" type="text" id="meta_keyword" value="{{ isset($buisinessoverview->meta_keyword) ? $buisinessoverview->meta_keyword : ''}}" >
     {!! $errors->first('meta_keyword', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="meta_description" style="font-size:18px;">{{ 'Meta Description' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_description" type="text" id="meta_description" value="{{ isset($buisinessoverview->meta_description) ? $buisinessoverview->meta_description : ''}}" >
     {!! $errors->first('meta_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="meta_image" style="font-size:18px;">{{ 'Meta Image' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_image" type="file" id="meta_image" value="{{ isset($buisinessoverview->meta_image) ? $buisinessoverview->meta_image : ''}}" >
     {!! $errors->first('meta_image', '<p class="help-block">:message</p>') !!}
      </div>
       @if(isset($buisinessoverview->meta_image))
      <img src="{{asset($buisinessoverview->meta_image)}}" height="100" width="100">
      @endif
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>

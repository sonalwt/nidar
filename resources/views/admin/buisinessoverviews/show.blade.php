@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Buisinessoverview  {{ $buisinessoverview->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/buisinessoverviews') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_buisinessoverviews', 'delete_buisinessoverviews')
                         <a href="{{ url('/admin/buisinessoverviews/' . $buisinessoverview->id . '/edit') }}" title="Edit Buisinessoverview"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/buisinessoverviews' . '/' . $buisinessoverview->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Buisinessoverview" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $buisinessoverview->id }}</td>
                                    </tr>
                                    <tr><th> Desktop Banner </th><td> {{ $buisinessoverview->desktop_banner }} </td></tr><tr><th> Mobile Banner </th><td> {{ $buisinessoverview->mobile_banner }} </td></tr><tr><th> Overview Part </th><td> {{ $buisinessoverview->overview_part }} </td></tr><tr><th> Group In Numbers Remark </th><td> {{ $buisinessoverview->Group_in_numbers_remark }} </td></tr><tr><th> Meta Title </th><td> {{ $buisinessoverview->meta_title }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

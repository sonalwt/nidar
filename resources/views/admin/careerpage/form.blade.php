 <label for="desktop_banner" style="font-size:18px;">{{ 'Desktop Banner' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="desktop_banner" type="file" id="desktop_banner" value="{{ isset($careerpage->desktop_banner) ? $careerpage->desktop_banner : ''}}" >
     {!! $errors->first('desktop_banner', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($careerpage->desktop_banner))
      <img src="{{asset($careerpage->desktop_banner)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="mobile_banner" style="font-size:18px;">{{ 'Mobile Banner' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="mobile_banner" type="file" id="mobile_banner" value="{{ isset($careerpage->mobile_banner) ? $careerpage->mobile_banner : ''}}" >
     {!! $errors->first('mobile_banner', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($careerpage->mobile_banner))
      <img src="{{asset($careerpage->mobile_banner)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="work_culture_image1" style="font-size:18px;">{{ 'Work Culture Image1' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="work_culture_image1" type="file" id="work_culture_image1" value="{{ isset($careerpage->work_culture_image1) ? $careerpage->work_culture_image1 : ''}}" >
     {!! $errors->first('work_culture_image1', '<p class="help-block">:message</p>') !!}
      </div>
       @if(isset($careerpage->work_culture_image1))
      <img src="{{asset($careerpage->work_culture_image1)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="work_culture_image2" style="font-size:18px;">{{ 'Work Culture Image2' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="work_culture_image2" type="file" id="work_culture_image2" value="{{ isset($careerpage->work_culture_image2) ? $careerpage->work_culture_image2 : ''}}" >
     {!! $errors->first('work_culture_image2', '<p class="help-block">:message</p>') !!}
      </div>
       @if(isset($careerpage->work_culture_image2))
      <img src="{{asset($careerpage->work_culture_image2)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="work_culture_image3" style="font-size:18px;">{{ 'Work Culture Image3' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="work_culture_image3" type="file" id="work_culture_image3" value="{{ isset($careerpage->work_culture_image3) ? $careerpage->work_culture_image3 : ''}}" >
     {!! $errors->first('work_culture_image3', '<p class="help-block">:message</p>') !!}
      </div>
        @if(isset($careerpage->work_culture_image3))
      <img src="{{asset($careerpage->work_culture_image3)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="work_culture_image4" style="font-size:18px;">{{ 'Work Culture Image4' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="work_culture_image4" type="file" id="work_culture_image4" value="{{ isset($careerpage->work_culture_image4) ? $careerpage->work_culture_image4 : ''}}" >
     {!! $errors->first('work_culture_image4', '<p class="help-block">:message</p>') !!}
      </div>
       @if(isset($careerpage->work_culture_image4))
      <img src="{{asset($careerpage->work_culture_image4)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="work_culture_description" style="font-size:18px;">{{ 'Work Culture Description' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="work_culture_description" type="textarea" id="work_culture_description" >{{ isset($careerpage->work_culture_description) ? $careerpage->work_culture_description : ''}}</textarea>
     {!! $errors->first('work_culture_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="equal_opportunity_employer_background_image" style="font-size:18px;">{{ 'Equal Opportunity Employer Background Image' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="equal_opportunity_employer_background_image" type="file" id="equal_opportunity_employer_background_image" value="{{ isset($careerpage->equal_opportunity_employer_background_image) ? $careerpage->equal_opportunity_employer_background_image : ''}}" >
     {!! $errors->first('equal_opportunity_employer_background_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($careerpage->equal_opportunity_employer_background_image))
      <img src="{{asset($careerpage->equal_opportunity_employer_background_image)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="equal_opportunity_employer_description" style="font-size:18px;">{{ 'Equal Opportunity Employer Description' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="equal_opportunity_employer_description" type="textarea" id="equal_opportunity_employer_description" >{{ isset($careerpage->equal_opportunity_employer_description) ? $careerpage->equal_opportunity_employer_description : ''}}</textarea>
     {!! $errors->first('equal_opportunity_employer_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="meta_title" style="font-size:18px;">{{ 'Meta Title' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_title" type="text" id="meta_title" value="{{ isset($careerpage->meta_title) ? $careerpage->meta_title : ''}}" >
     {!! $errors->first('meta_title', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="meta_keyword" style="font-size:18px;">{{ 'Meta Keyword' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_keyword" type="text" id="meta_keyword" value="{{ isset($careerpage->meta_keyword) ? $careerpage->meta_keyword : ''}}" >
     {!! $errors->first('meta_keyword', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="meta_description" style="font-size:18px;">{{ 'Meta Description' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_description" type="text" id="meta_description" value="{{ isset($careerpage->meta_description) ? $careerpage->meta_description : ''}}" >
     {!! $errors->first('meta_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="meta_image" style="font-size:18px;">{{ 'Meta Image' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_image" type="file" id="meta_image" value="{{ isset($careerpage->meta_image) ? $careerpage->meta_image : ''}}" >
     {!! $errors->first('meta_image', '<p class="help-block">:message</p>') !!}
      </div>
       @if(isset($careerpage->meta_image))
      <img src="{{asset($careerpage->meta_image)}}" height="100" width="100">
      @endif
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>

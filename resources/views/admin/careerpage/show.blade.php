@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Careerpage  {{ $careerpage->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/careerpage') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_careerpage', 'delete_careerpage')
                         <a href="{{ url('/admin/careerpage/' . $careerpage->id . '/edit') }}" title="Edit Careerpage"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/careerpage' . '/' . $careerpage->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Careerpage" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $careerpage->id }}</td>
                                    </tr>
                                    <tr><th> Desktop Banner </th><td> {{ $careerpage->desktop_banner }} </td></tr><tr><th> Mobile Banner </th><td> {{ $careerpage->mobile_banner }} </td></tr><tr><th> Work Culture Image1 </th><td> {{ $careerpage->work_culture_image1 }} </td></tr><tr><th> Work Culture Image2 </th><td> {{ $careerpage->work_culture_image2 }} </td></tr><tr><th> Work Culture Image3 </th><td> {{ $careerpage->work_culture_image3 }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Ourbrandpage  {{ $ourbrandpage->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/ourbrandpage') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_ourbrandpage', 'delete_ourbrandpage')
                         <a href="{{ url('/admin/ourbrandpage/' . $ourbrandpage->id . '/edit') }}" title="Edit Ourbrandpage"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/ourbrandpage' . '/' . $ourbrandpage->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Ourbrandpage" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $ourbrandpage->id }}</td>
                                    </tr>
                                    <tr><th> Desktop Banner </th><td> {{ $ourbrandpage->desktop_banner }} </td></tr><tr><th> Mobile Banner </th><td> {{ $ourbrandpage->mobile_banner }} </td></tr><tr><th> Overview </th><td> {{ $ourbrandpage->overview }} </td></tr><tr><th> Meta Title </th><td> {{ $ourbrandpage->meta_title }} </td></tr><tr><th> Meta Keyword </th><td> {{ $ourbrandpage->meta_keyword }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

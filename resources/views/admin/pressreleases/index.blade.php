@extends('admin.layouts.master')
<style>
    .searchBar{
        margin-right:22px !important;
    }
</style>
@section('content')
    <div class="container-fluid">
        <div class="row">
                     <div class="col-md-12">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Pressreleases
                                        </h2>
                                    </div>
                     <br>
                     <a href="{{ url('/admin/pressreleases/create') }}" class="btn btn-success btn-sm waves-effect" title="Add New Pressrelease" style="margin-left: 22px;">
                        <i class="material-icons">add_circle</i> Add New
                     </a>
                     {!! Form::open(['method' => 'GET', 'url' => '/admin/pressreleases', 'class' => 'navbar-form navbar-right searchBar', 'role' => 'search'])  !!}
                             <div class="input-group">
                             <input type="text" class="form-control" name="search" placeholder="Search..." style="border: ridge">
                             <span class="input-group-btn">
                             <button class="" type="submit">
                             <i class="material-icons" style="height: 27px !important;">search</i>
                              </button>
                              </span>
                              </div>
                     {!! Form::close() !!}
                     <div class="body">
                     <br>
                      <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Title</th><th>Image Url</th><th>Status</th>
                                        @can('view_pressreleases','edit_pressreleases', 'delete_pressreleases')
                                        <th class="text-center">Actions</th>
                                        @endcan
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($pressreleases as $key => $item)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $item->title }}</td><td>@if(isset($item->image_url) && !empty($item->image_url)) <img src="{{ asset($item->image_url) }}" height="100" width="100"> @endif </td><td>{{ $item->status }}</td>
                                        @can('view_pressreleases')
                                        <td class="text-center">
                                        <a href="{{ url('/admin/pressreleases/' . $item->id) }}" title="View Pressrelease"><button class="btn btn-info btn-xs"><i class="material-icons">remove_red_eye</i> View</button></a>
                                        @include('shared._actions', ['entity' => 'pressreleases',
                                        'id' => $item->id
                                        ])
                                         </td>
                                        @endcan
                                      </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $pressreleases->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

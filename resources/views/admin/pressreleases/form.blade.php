 <label for="title" style="font-size:18px;">{{ 'Title' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="title" type="text" id="title" value="{{ isset($pressrelease->title) ? $pressrelease->title : ''}}" required>
     {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="image_url" style="font-size:18px;">{{ 'Image Url' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="image_url" type="file" id="image_url" value="{{ isset($pressrelease->image_url) ? $pressrelease->image_url : ''}}" required>
     {!! $errors->first('image_url', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($pressrelease->image_url) && !empty($pressrelease->image_url))
      <img src="{{asset($pressrelease->image_url)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="news_link" style="font-size:18px;">{{ 'News Link' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="news_link" type="text" id="news_link" value="{{ isset($pressrelease->news_link) ? $pressrelease->news_link : ''}}" required>
     {!! $errors->first('news_link', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="release_date" style="font-size:18px;">{{ 'Release Date' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="release_date" type="date" id="release_date" value="{{ isset($pressrelease->release_date) ? $pressrelease->release_date : ''}}" required>
     {!! $errors->first('release_date', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="description" style="font-size:18px;">{{ 'Description' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="description" type="textarea" id="description" >{{ isset($pressrelease->description) ? $pressrelease->description : ''}}</textarea>
     {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="business_vertical" style="font-size:18px;">{{ 'Business Vertical' }}</label>
 <div class="">
      <div class="form-line">
         <select name="business_vertical" class="form-control" id="business_vertical" required>
    @foreach ($verticals as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($videocorner->business_vertical) && $videocorner->business_vertical == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('business_vertical', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="recent_article_status" style="font-size:18px;">{{ 'Recent Article Status' }}</label>
 <div class="">
      <div class="form-line">
        <select name="recent_article_status" class="form-control" id="recent_article_status" required>
    @foreach (json_decode('{"Enabled": "Enabled", "Disabled": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($pressrelease->recent_article_status) && $pressrelease->recent_article_status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="mostly_viewed_status" style="font-size:18px;">{{ 'Mostly Viewed Status' }}</label>
 <div class="">
      <div class="form-line">
        <select name="mostly_viewed_status" class="form-control" id="mostly_viewed_status" required>
    @foreach (json_decode('{"Enabled": "Enabled", "Disabled": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($pressrelease->mostly_viewed_status) && $pressrelease->mostly_viewed_status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="">
      <div class="form-line">
        <select name="status" class="form-control" id="status" required>
    @foreach (json_decode('{"Enabled": "Enabled", "Disabled": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($pressrelease->status) && $pressrelease->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>

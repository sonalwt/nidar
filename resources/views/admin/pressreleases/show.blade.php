@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Pressrelease  {{ $pressrelease->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/pressreleases') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_pressreleases', 'delete_pressreleases')
                         <a href="{{ url('/admin/pressreleases/' . $pressrelease->id . '/edit') }}" title="Edit Pressrelease"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/pressreleases' . '/' . $pressrelease->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Pressrelease" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $pressrelease->id }}</td>
                                    </tr>
                                    <tr><th> Title </th><td> {{ $pressrelease->title }} </td></tr><tr><th> Image Url </th><td><img src="{{ asset($pressrelease->image_url) }}" height="100" width="100"> </td></tr><tr><th> News Link </th><td> {{ $pressrelease->news_link }} </td></tr><tr><th> Release Date </th><td> {{ $pressrelease->release_date }} </td></tr>
                                    <tr><th> Description </th><td> {{ $pressrelease->description }} </td></tr>
                                     <tr><th> Status </th><td> {{ $pressrelease->status }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

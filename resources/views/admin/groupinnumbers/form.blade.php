 <label for="icon" style="font-size:18px;">{{ 'Icon' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="icon" type="file" id="icon" value="{{ isset($groupinnumber->icon) ? $groupinnumber->icon : ''}}" >
     {!! $errors->first('icon', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($groupinnumber->icon))
      <img src="{{asset($groupinnumber->icon)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="title" style="font-size:18px;">{{ 'Title' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="title" type="text" id="title" value="{{ isset($groupinnumber->title) ? $groupinnumber->title : ''}}" required>
     {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="count" style="font-size:18px;">{{ 'Count' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="count" type="text" id="count" value="{{ isset($groupinnumber->count) ? $groupinnumber->count : ''}}" required>
     {!! $errors->first('count', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="unit" style="font-size:18px;">{{ 'Unit' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="unit" type="text" id="unit" value="{{ isset($groupinnumber->unit) ? $groupinnumber->unit : ''}}" required>
     {!! $errors->first('unit', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="">
      <div class="form-line">
        <select name="status" class="form-control" id="status" required>
    @foreach (json_decode('{"Enabled": "Enabled", "Disabled": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($groupinnumber->status) && $groupinnumber->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>

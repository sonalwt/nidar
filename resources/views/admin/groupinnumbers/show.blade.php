@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Groupinnumber  {{ $groupinnumber->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/groupinnumbers') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_groupinnumbers', 'delete_groupinnumbers')
                         <a href="{{ url('/admin/groupinnumbers/' . $groupinnumber->id . '/edit') }}" title="Edit Groupinnumber"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/groupinnumbers' . '/' . $groupinnumber->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Groupinnumber" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $groupinnumber->id }}</td>
                                    </tr>
                                    <tr><th> Icon </th><td> @if(isset($groupinnumber->icon)) <img src="{{ asset($groupinnumber->icon) }}" height="100" width="100"> @endif </td></tr><tr><th> Title </th><td> {{ $groupinnumber->title }} </td></tr><tr><th> Count </th><td> {{ $groupinnumber->count }} </td></tr><tr><th> Unit </th><td> {{ $groupinnumber->unit }} </td></tr><tr><th> Status </th><td> {{ $groupinnumber->status }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

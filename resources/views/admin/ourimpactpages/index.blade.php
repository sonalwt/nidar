@extends('admin.layouts.master')
<style>
    .searchBar{
        margin-right:22px !important;
    }
</style>
@section('content')
    <div class="container-fluid">
        <div class="row">
                     <div class="col-md-12">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Ourimpactpages
                                        </h2>
                                    </div>
                     <br>
                     <a href="{{ url('/admin/ourimpactpages/create') }}" class="btn btn-success btn-sm waves-effect" title="Add New Ourimpactpage" style="margin-left: 22px;">
                        <i class="material-icons">add_circle</i> Add New
                     </a>
                     {!! Form::open(['method' => 'GET', 'url' => '/admin/ourimpactpages', 'class' => 'navbar-form navbar-right searchBar', 'role' => 'search'])  !!}
                             <div class="input-group">
                             <input type="text" class="form-control" name="search" placeholder="Search..." style="border: ridge">
                             <span class="input-group-btn">
                             <button class="" type="submit">
                             <i class="material-icons" style="height: 27px !important;">search</i>
                              </button>
                              </span>
                              </div>
                     {!! Form::close() !!}
                     <div class="body">
                     <br>
                      <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Desktop Banner</th><th>Mobile Banner</th><th>Description</th><th>The Cummunity Description</th><th>Csr Initiatives Description</th>
                                        @can('view_ourimpactpages','edit_ourimpactpages', 'delete_ourimpactpages')
                                        <th class="text-center">Actions</th>
                                        @endcan
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($ourimpactpages as $key => $item)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $item->desktop_banner }}</td><td>{{ $item->mobile_banner }}</td><td>{{ $item->description }}</td><td>{{ $item->the_cummunity_description }}</td><td>{{ $item->csr_initiatives_description }}</td>
                                        @can('view_ourimpactpages')
                                        <td class="text-center">
                                        <a href="{{ url('/admin/ourimpactpages/' . $item->id) }}" title="View Ourimpactpage"><button class="btn btn-info btn-xs"><i class="material-icons">remove_red_eye</i> View</button></a>
                                        @include('shared._actions', ['entity' => 'ourimpactpages',
                                        'id' => $item->id
                                        ])
                                         </td>
                                        @endcan
                                      </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $ourimpactpages->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

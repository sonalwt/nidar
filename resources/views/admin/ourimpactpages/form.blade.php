 <label for="desktop_banner" style="font-size:18px;">{{ 'Desktop Banner' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="desktop_banner" type="file" id="desktop_banner" value="{{ isset($ourimpactpage->desktop_banner) ? $ourimpactpage->desktop_banner : ''}}" >
     {!! $errors->first('desktop_banner', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($ourimpactpage->desktop_banner) && !empty($ourimpactpage->desktop_banner))
      <img src="{{asset($ourimpactpage->desktop_banner)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="mobile_banner" style="font-size:18px;">{{ 'Mobile Banner' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="mobile_banner" type="file" id="mobile_banner" value="{{ isset($ourimpactpage->mobile_banner) ? $ourimpactpage->mobile_banner : ''}}" >
     {!! $errors->first('mobile_banner', '<p class="help-block">:message</p>') !!}
      </div>
       @if(isset($ourimpactpage->mobile_banner) && !empty($ourimpactpage->mobile_banner))
      <img src="{{asset($ourimpactpage->mobile_banner)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="description" style="font-size:18px;">{{ 'Description' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="description" type="textarea" id="description" >{{ isset($ourimpactpage->description) ? $ourimpactpage->description : ''}}</textarea>
     {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="the_cummunity_description" style="font-size:18px;">{{ 'The Cummunity Description' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="the_cummunity_description" type="textarea" id="the_cummunity_description" >{{ isset($ourimpactpage->the_cummunity_description) ? $ourimpactpage->the_cummunity_description : ''}}</textarea>
     {!! $errors->first('the_cummunity_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <br><br><br>
 <label for="skill_development_part1" style="font-size:20px;">{{ 'The Community Images' }}</label><br>
 @php
 $i=1;
 @endphp
 <div id="cummunities">
  @if(isset($ourimpactpage->cummunity_img) && !empty($ourimpactpage->cummunity_img))
  @php
  $cummunities=json_decode($ourimpactpage->cummunity_img,true);
  @endphp
  @foreach($cummunities as $cummunity)
  <div id="cummunity{{$i}}">
   <label for="featured_image" style="font-size:18px;">{{ 'Featured Image' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="mapping[{{$i}}][featured_image]" type="file" id="featured_image" >
     {!! $errors->first('featured_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($cummunity['featured_image']) && !empty($cummunity['featured_image']))
      <input type="hidden" name="mapping[{{$i}}][featured_image_original]" value="{{$cummunity['featured_image']}}">
      <img src="{{asset($cummunity['featured_image'])}}" height="100" width="100">
      @endif
 </div><br>
 <label for="title" style="font-size:18px;">{{ 'Title' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="mapping[{{$i}}][title]" type="text" id="title" value="{{ isset($cummunity['title']) ? $cummunity['title'] : ''}}" >
     {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
      </div>
      <button type="button" name="remove" id="{{$i}}" class="btn btn-danger removecummunity bt{{$i}}">Remove</button>
     
 </div><br>
 </div>
 <hr>
 @php
 $i++;
 @endphp
 @endforeach
 @else
   <div id="cummunity{{$i}}">
   <label for="featured_image" style="font-size:18px;">{{ 'Featured Image' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="mapping[{{$i}}][featured_image]" type="file" id="featured_image" required>
     {!! $errors->first('featured_image', '<p class="help-block">:message</p>') !!}
      </div>
      
 </div><br>
 <label for="title" style="font-size:18px;">{{ 'Title' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="mapping[{{$i}}][title]" type="text" id="title" required>
     {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
      </div>
      <button type="button" name="remove" id="{{$i}}" class="btn btn-danger removecummunity bt{{$i}}">Remove</button>
     
 </div><br>
 </div>
 <hr>
 @endif
 <input type="hidden" id="cummunitycount" value="{{$i}}">
 
 </div><br>
 <input type="button" class="btn btn-warning" id="addcummunity" value="Add Cummunity Image">
 <br>
 <br>
 <label for="csr_initiatives_description" style="font-size:18px;">{{ 'Csr Initiatives Description' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="csr_initiatives_description" type="textarea" id="csr_initiatives_description" >{{ isset($ourimpactpage->csr_initiatives_description) ? $ourimpactpage->csr_initiatives_description : ''}}</textarea>
     {!! $errors->first('csr_initiatives_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="skill_development_part1" style="font-size:18px;">{{ 'Skill Development Part1' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="skill_development_part1" type="textarea" id="skill_development_part1" >{{ isset($ourimpactpage->skill_development_part1) ? $ourimpactpage->skill_development_part1 : ''}}</textarea>
     {!! $errors->first('skill_development_part1', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br><br><br>
 <label for="skill_development_part1" style="font-size:20px;">{{ 'COE Areas' }}</label><br>
 @php
 $k=1;
 @endphp
 <div id="coeareas">
  @if(isset($ourimpactpage->coeareas) && !empty($ourimpactpage->coeareas))
  @php
  $coeareas=json_decode($ourimpactpage->coeareas,true);
  @endphp
  @foreach($coeareas as $coearea)
  <div id="coearea{{$k}}">
   <label for="featured_image" style="font-size:18px;">{{ 'Featured Image' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="newmapping[{{$k}}][featured_image]" type="file" id="featured_image" >
     {!! $errors->first('featured_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($coearea['featured_image']) && !empty($coearea['featured_image']))
      <input type="hidden" name="newmapping[{{$k}}][featured_image_original]" value="{{$coearea['featured_image']}}">
      <img src="{{asset($coearea['featured_image'])}}" height="100" width="100">
      @endif
 </div><br>
 <label for="title" style="font-size:18px;">{{ 'Title' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="newmapping[{{$k}}][title]" type="text" id="title" value="{{ isset($coearea['title']) ? $coearea['title'] : ''}}" >
     {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
      </div>
      <button type="button" name="remove" id="{{$k}}" class="btn btn-danger removecoearea bt{{$k}}">Remove</button>
     
 </div><br>
 </div>
 <hr>
 @php
 $k++;
 @endphp
 @endforeach
 @else
   <div id="coearea{{$k}}">
   <label for="featured_image" style="font-size:18px;">{{ 'Featured Image' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="newmapping[{{$k}}][featured_image]" type="file" id="featured_image" >
     {!! $errors->first('featured_image', '<p class="help-block">:message</p>') !!}
      </div>
      
 </div><br>
 <label for="title" style="font-size:18px;">{{ 'Title' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="newmapping[{{$k}}][title]" type="text" id="title" >
     {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
      </div>
      <button type="button" name="remove" id="{{$k}}" class="btn btn-danger removecoearea bt{{$k}}">Remove</button>
     
 </div><br>
 </div>
 <hr>
 @endif
 <input type="hidden" id="coeareacount" value="{{$k}}">
 
 </div><br>
 <input type="button" class="btn btn-warning" id="addcoearea" value="Add COE Areas">
 <br>
 <br>
 <label for="skill_development_part2" style="font-size:18px;">{{ 'Skill Development Part2' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="skill_development_part2" type="textarea" id="skill_development_part2" >{{ isset($ourimpactpage->skill_development_part2) ? $ourimpactpage->skill_development_part2 : ''}}</textarea>
     {!! $errors->first('skill_development_part2', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <br><br><br>
 <label for="skill_development_part1" style="font-size:20px;">{{ 'Sustainability' }}</label><br>
 @php
 $n=1;
 @endphp
 <div id="sustainabilities">
  @if(isset($ourimpactpage->sustainability) && !empty($ourimpactpage->sustainability))
  @php
  $sustainabilities=json_decode($ourimpactpage->sustainability,true);
  @endphp
  @foreach($sustainabilities as $sustainability)
  <div id="sustainability{{$n}}">
   <label for="featured_image" style="font-size:18px;">{{ 'Featured Image' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="sustain[{{$n}}][featured_image]" type="file" id="featured_image" >
     {!! $errors->first('featured_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($sustainability['featured_image']) && !empty($sustainability['featured_image']))
      <input type="hidden" name="sustain[{{$n}}][featured_image_original]" value="{{$sustainability['featured_image']}}">
      <img src="{{asset($sustainability['featured_image'])}}" height="100" width="100">
      @endif
 </div><br>
 <label for="title" style="font-size:18px;">{{ 'Title' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="sustain[{{$n}}][title]" type="text" id="title" value="{{ isset($sustainability['title']) ? $sustainability['title'] : ''}}" >
     {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
      </div>
      <button type="button" name="remove" id="{{$n}}" class="btn btn-danger removesustainability bt{{$n}}">Remove</button>
     
 </div><br>
 </div>
 <hr>
 @php
 $n++;
 @endphp
 @endforeach
 @else
   <div id="sustainability{{$n}}">
   <label for="featured_image" style="font-size:18px;">{{ 'Featured Image' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="sustain[{{$n}}][featured_image]" type="file" id="featured_image" >
     {!! $errors->first('featured_image', '<p class="help-block">:message</p>') !!}
      </div>
      
 </div><br>
 <label for="title" style="font-size:18px;">{{ 'Title' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="sustain[{{$n}}][title]" type="text" id="title" >
     {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
      </div>
      <button type="button" name="remove" id="{{$n}}" class="btn btn-danger removesustainability bt{{$n}}">Remove</button>
     
 </div><br>
 </div>
 <hr>
 @endif
 <input type="hidden" id="sustainabilitycount" value="{{$n}}">
 
 </div><br>
 <input type="button" class="btn btn-warning" id="addsustainability" value="Add Sustainability">
 <br>
 <br>
 <label for="meta_title" style="font-size:18px;">{{ 'Meta Title' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_title" type="text" id="meta_title" value="{{ isset($ourimpactpage->meta_title) ? $ourimpactpage->meta_title : ''}}" >
     {!! $errors->first('meta_title', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="meta_keyword" style="font-size:18px;">{{ 'Meta Keyword' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_keyword" type="text" id="meta_keyword" value="{{ isset($ourimpactpage->meta_keyword) ? $ourimpactpage->meta_keyword : ''}}" >
     {!! $errors->first('meta_keyword', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="meta_description" style="font-size:18px;">{{ 'Meta Description' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_description" type="text" id="meta_description" value="{{ isset($ourimpactpage->meta_description) ? $ourimpactpage->meta_description : ''}}" >
     {!! $errors->first('meta_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="meta_image" style="font-size:18px;">{{ 'Meta Image' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_image" type="file" id="meta_image" value="{{ isset($ourimpactpage->meta_image) ? $ourimpactpage->meta_image : ''}}" >
     {!! $errors->first('meta_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($ourimpactpage->meta_image) && !empty($ourimpactpage->meta_image))
      <img src="{{asset($ourimpactpage->meta_image)}}" height="100" width="100">
      @endif
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
@section('scripts')
<script>
 $(document).ready(function(){
  var j=$('#cummunitycount').val();
    $('#addcummunity').click(function(){
        // alert('test')
            j++;
            $('#cummunities').append('<div id="cummunity'+j+'"><label for="featured_image" style="font-size:18px;">Featured Image</label><div class=""><div class="form-line"><input class="form-control" name="mapping['+j+'][featured_image]" type="file" id="featured_image" ></div></div><br><label for="title" style="font-size:18px;">Title</label><div class=""><div class="form-line"><input class="form-control" name="mapping['+j+'][title]" type="text" id="title"  ></div><button type="button" name="remove" id="'+j+'" class="btn btn-danger removecummunity bt'+j+'">Remove</button></div><br></div>');
        
        });

        $(document).on('click', '.removecummunity', function(){
            var button1_id = $(this).attr("id");
            $('#cummunity'+button1_id+'').remove();
            $('.bt'+button1_id+'').remove();

        });

        var x=$('#coeareacount').val();
    $('#addcoearea').click(function(){
        // alert('test')
            x++;
            $('#coeareas').append('<div id="coearea'+x+'"><label for="featured_image" style="font-size:18px;">Featured Image</label><div class=""><div class="form-line"><input class="form-control" name="newmapping['+x+'][featured_image]" type="file" id="featured_image" ></div></div><br><label for="title" style="font-size:18px;">Title</label><div class=""><div class="form-line"><input class="form-control" name="newmapping['+x+'][title]" type="text" id="title"  ></div><button type="button" name="remove" id="'+j+'" class="btn btn-danger removecoearea bt'+x+'">Remove</button></div><br></div>');
        
        });

        $(document).on('click', '.removecoearea', function(){
            var button1_id = $(this).attr("id");
            $('#coearea'+button1_id+'').remove();
            $('.bt'+button1_id+'').remove();

        });

        var y=$('#sustainabilitycount').val();
    $('#addsustainability').click(function(){
        // alert('test')
            y++;
            $('#sustainabilities').append('<div id="sustainability'+y+'"><label for="featured_image" style="font-size:18py;">Featured Image</label><div class=""><div class="form-line"><input class="form-control" name="sustain['+y+'][featured_image]" type="file" id="featured_image" ></div></div><br><label for="title" style="font-size:18py;">Title</label><div class=""><div class="form-line"><input class="form-control" name="sustain['+y+'][title]" type="teyt" id="title"  ></div><button type="button" name="remove" id="'+j+'" class="btn btn-danger removesustainability bt'+y+'">Remove</button></div><br></div>');
        
        });

        $(document).on('click', '.removesustainability', function(){
            var button1_id = $(this).attr("id");
            $('#sustainability'+button1_id+'').remove();
            $('.bt'+button1_id+'').remove();

        });
       
        
  });
</script>
@endsection    
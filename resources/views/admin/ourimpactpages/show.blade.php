@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Ourimpactpage  {{ $ourimpactpage->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/ourimpactpages') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_ourimpactpages', 'delete_ourimpactpages')
                         <a href="{{ url('/admin/ourimpactpages/' . $ourimpactpage->id . '/edit') }}" title="Edit Ourimpactpage"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/ourimpactpages' . '/' . $ourimpactpage->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Ourimpactpage" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $ourimpactpage->id }}</td>
                                    </tr>
                                    <tr><th> Desktop Banner </th><td> {{ $ourimpactpage->desktop_banner }} </td></tr><tr><th> Mobile Banner </th><td> {{ $ourimpactpage->mobile_banner }} </td></tr><tr><th> Description </th><td> {{ $ourimpactpage->description }} </td></tr><tr><th> The Cummunity Description </th><td> {{ $ourimpactpage->the_cummunity_description }} </td></tr><tr><th> Csr Initiatives Description </th><td> {{ $ourimpactpage->csr_initiatives_description }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Aboutuspage  {{ $aboutuspage->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/aboutuspages') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_aboutuspages', 'delete_aboutuspages')
                         <a href="{{ url('/admin/aboutuspages/' . $aboutuspage->id . '/edit') }}" title="Edit Aboutuspage"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/aboutuspages' . '/' . $aboutuspage->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Aboutuspage" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $aboutuspage->id }}</td>
                                    </tr>
                                    <tr><th> Desktop Banner </th><td> {{ $aboutuspage->desktop_banner }} </td></tr><tr><th> Mobile Banner </th><td> {{ $aboutuspage->mobile_banner }} </td></tr><tr><th> Overview Part 1 </th><td> {{ $aboutuspage->overview_part_1 }} </td></tr><tr><th> Overview Part 2 </th><td> {{ $aboutuspage->overview_part_2 }} </td></tr><tr><th> Our Legacy Description </th><td> {{ $aboutuspage->our_legacy_description }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

 <label for="desktop_banner" style="font-size:18px;">{{ 'Desktop Banner' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="desktop_banner" type="file" id="desktop_banner" value="{{ isset($aboutuspage->desktop_banner) ? $aboutuspage->desktop_banner : ''}}" >
     {!! $errors->first('desktop_banner', '<p class="help-block">:message</p>') !!}
      </div>
       @if(isset($aboutuspage->desktop_banner))
      <img src="{{asset($aboutuspage->desktop_banner)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="mobile_banner" style="font-size:18px;">{{ 'Mobile Banner' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="mobile_banner" type="file" id="mobile_banner" value="{{ isset($aboutuspage->mobile_banner) ? $aboutuspage->mobile_banner : ''}}" >
     {!! $errors->first('mobile_banner', '<p class="help-block">:message</p>') !!}
      </div>
       @if(isset($aboutuspage->mobile_banner))
      <img src="{{asset($aboutuspage->mobile_banner)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="overview_part_1" style="font-size:18px;">{{ 'Overview Part 1' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="overview_part_1" type="textarea" id="overview_part_1" >{{ isset($aboutuspage->overview_part_1) ? $aboutuspage->overview_part_1 : ''}}</textarea>
     {!! $errors->first('overview_part_1', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="overview_part_2" style="font-size:18px;">{{ 'Overview Part 2' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="overview_part_2" type="textarea" id="overview_part_2" >{{ isset($aboutuspage->overview_part_2) ? $aboutuspage->overview_part_2 : ''}}</textarea>
     {!! $errors->first('overview_part_2', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="our_legacy_description" style="font-size:18px;">{{ 'Our Legacy Description' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="our_legacy_description" type="textarea" id="our_legacy_description" >{{ isset($aboutuspage->our_legacy_description) ? $aboutuspage->our_legacy_description : ''}}</textarea>
     {!! $errors->first('our_legacy_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="our_legacy_backgorund_image" style="font-size:18px;">{{ 'Our Legacy Backgorund Image' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="our_legacy_backgorund_image" type="file" id="our_legacy_backgorund_image" value="{{ isset($aboutuspage->our_legacy_backgorund_image) ? $aboutuspage->our_legacy_backgorund_image : ''}}" >
     {!! $errors->first('our_legacy_backgorund_image', '<p class="help-block">:message</p>') !!}
      </div>
       @if(isset($aboutuspage->our_legacy_backgorund_image))
      <img src="{{asset($aboutuspage->our_legacy_backgorund_image)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="key_strength_and_differentiators_main_description" style="font-size:18px;">{{ 'Key Strength And Differentiators Main Description' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="key_strength_and_differentiators_main_description" type="textarea" id="key_strength_and_differentiators_main_description" >{{ isset($aboutuspage->key_strength_and_differentiators_main_description) ? $aboutuspage->key_strength_and_differentiators_main_description : ''}}</textarea>
     {!! $errors->first('key_strength_and_differentiators_main_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="key_strength_and_differentiators_image1" style="font-size:18px;">{{ 'Key Strength And Differentiators Image1' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="key_strength_and_differentiators_image1" type="file" id="key_strength_and_differentiators_image1" value="{{ isset($aboutuspage->key_strength_and_differentiators_image1) ? $aboutuspage->key_strength_and_differentiators_image1 : ''}}" >
     {!! $errors->first('key_strength_and_differentiators_image1', '<p class="help-block">:message</p>') !!}
      </div>
       @if(isset($aboutuspage->key_strength_and_differentiators_image1))
      <img src="{{asset($aboutuspage->key_strength_and_differentiators_image1)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="key_strength_and_differentiators_title1" style="font-size:18px;">{{ 'Key Strength And Differentiators Title1' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="key_strength_and_differentiators_title1" type="text" id="key_strength_and_differentiators_title1" value="{{ isset($aboutuspage->key_strength_and_differentiators_title1) ? $aboutuspage->key_strength_and_differentiators_title1 : ''}}" >
     {!! $errors->first('key_strength_and_differentiators_title1', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="key_strength_and_differentiators_description1" style="font-size:18px;">{{ 'Key Strength And Differentiators Description1' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="key_strength_and_differentiators_description1" type="textarea" id="key_strength_and_differentiators_description1" >{{ isset($aboutuspage->key_strength_and_differentiators_description1) ? $aboutuspage->key_strength_and_differentiators_description1 : ''}}</textarea>
     {!! $errors->first('key_strength_and_differentiators_description1', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="key_strength_and_differentiators_image2" style="font-size:18px;">{{ 'Key Strength And Differentiators Image2' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="key_strength_and_differentiators_image2" type="file" id="key_strength_and_differentiators_image2" value="{{ isset($aboutuspage->key_strength_and_differentiators_image2) ? $aboutuspage->key_strength_and_differentiators_image2 : ''}}" >
     {!! $errors->first('key_strength_and_differentiators_image2', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($aboutuspage->key_strength_and_differentiators_image2))
      <img src="{{asset($aboutuspage->key_strength_and_differentiators_image2)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="key_strength_and_differentiators_title2" style="font-size:18px;">{{ 'Key Strength And Differentiators Title2' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="key_strength_and_differentiators_title2" type="text" id="key_strength_and_differentiators_title2" value="{{ isset($aboutuspage->key_strength_and_differentiators_title2) ? $aboutuspage->key_strength_and_differentiators_title2 : ''}}" >
     {!! $errors->first('key_strength_and_differentiators_title2', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="key_strength_and_differentiators_description2" style="font-size:18px;">{{ 'Key Strength And Differentiators Description2' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="key_strength_and_differentiators_description2" type="textarea" id="key_strength_and_differentiators_description2" >{{ isset($aboutuspage->key_strength_and_differentiators_description2) ? $aboutuspage->key_strength_and_differentiators_description2 : ''}}</textarea>
     {!! $errors->first('key_strength_and_differentiators_description2', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="key_strength_and_differentiators_image3" style="font-size:18px;">{{ 'Key Strength And Differentiators Image3' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="key_strength_and_differentiators_image3" type="file" id="key_strength_and_differentiators_image3" value="{{ isset($aboutuspage->key_strength_and_differentiators_image3) ? $aboutuspage->key_strength_and_differentiators_image3 : ''}}" >
     {!! $errors->first('key_strength_and_differentiators_image3', '<p class="help-block">:message</p>') !!}
      </div>
       @if(isset($aboutuspage->key_strength_and_differentiators_image3))
      <img src="{{asset($aboutuspage->key_strength_and_differentiators_image3)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="key_strength_and_differentiators_title3" style="font-size:18px;">{{ 'Key Strength And Differentiators Title3' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="key_strength_and_differentiators_title3" type="text" id="key_strength_and_differentiators_title3" value="{{ isset($aboutuspage->key_strength_and_differentiators_title3) ? $aboutuspage->key_strength_and_differentiators_title3 : ''}}" >
     {!! $errors->first('key_strength_and_differentiators_title3', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="key_strength_and_differentiators_description3" style="font-size:18px;">{{ 'Key Strength And Differentiators Description3' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="key_strength_and_differentiators_description3" type="textarea" id="key_strength_and_differentiators_description3" >{{ isset($aboutuspage->key_strength_and_differentiators_description3) ? $aboutuspage->key_strength_and_differentiators_description3 : ''}}</textarea>
     {!! $errors->first('key_strength_and_differentiators_description3', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="key_strength_and_differentiators_image4" style="font-size:18px;">{{ 'Key Strength And Differentiators Image4' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="key_strength_and_differentiators_image4" type="file" id="key_strength_and_differentiators_image4" value="{{ isset($aboutuspage->key_strength_and_differentiators_image4) ? $aboutuspage->key_strength_and_differentiators_image4 : ''}}" >
     {!! $errors->first('key_strength_and_differentiators_image4', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($aboutuspage->key_strength_and_differentiators_image4))
      <img src="{{asset($aboutuspage->key_strength_and_differentiators_image4)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="key_strength_and_differentiators_title4" style="font-size:18px;">{{ 'Key Strength And Differentiators Title4' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="key_strength_and_differentiators_title4" type="text" id="key_strength_and_differentiators_title4" value="{{ isset($aboutuspage->key_strength_and_differentiators_title4) ? $aboutuspage->key_strength_and_differentiators_title4 : ''}}" >
     {!! $errors->first('key_strength_and_differentiators_title4', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="key_strength_and_differentiators_description4" style="font-size:18px;">{{ 'Key Strength And Differentiators Description4' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="key_strength_and_differentiators_description4" type="textarea" id="key_strength_and_differentiators_description4" >{{ isset($aboutuspage->key_strength_and_differentiators_description4) ? $aboutuspage->key_strength_and_differentiators_description4 : ''}}</textarea>
     {!! $errors->first('key_strength_and_differentiators_description4', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="meta_title" style="font-size:18px;">{{ 'Meta Title' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_title" type="text" id="meta_title" value="{{ isset($aboutuspage->meta_title) ? $aboutuspage->meta_title : ''}}" >
     {!! $errors->first('meta_title', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="meta_keyword" style="font-size:18px;">{{ 'Meta Keyword' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_keyword" type="text" id="meta_keyword" value="{{ isset($aboutuspage->meta_keyword) ? $aboutuspage->meta_keyword : ''}}" >
     {!! $errors->first('meta_keyword', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="meta_description" style="font-size:18px;">{{ 'Meta Description' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_description" type="text" id="meta_description" value="{{ isset($aboutuspage->meta_description) ? $aboutuspage->meta_description : ''}}" >
     {!! $errors->first('meta_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="meta_image" style="font-size:18px;">{{ 'Meta Image' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_image" type="file" id="meta_image" value="{{ isset($aboutuspage->meta_image) ? $aboutuspage->meta_image : ''}}" >
     {!! $errors->first('meta_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($aboutuspage->meta_image))
      <img src="{{asset($aboutuspage->meta_image)}}" height="100" width="100">
      @endif
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>

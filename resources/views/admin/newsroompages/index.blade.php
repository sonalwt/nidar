@extends('admin.layouts.master')
<style>
    .searchBar{
        margin-right:22px !important;
    }
</style>
@section('content')
    <div class="container-fluid">
        <div class="row">
                     <div class="col-md-12">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Newsroompages
                                        </h2>
                                    </div>
                     <br>
                     <a href="{{ url('/admin/newsroompages/create') }}" class="btn btn-success btn-sm waves-effect" title="Add New Newsroompage" style="margin-left: 22px;">
                        <i class="material-icons">add_circle</i> Add New
                     </a>
                     {!! Form::open(['method' => 'GET', 'url' => '/admin/newsroompages', 'class' => 'navbar-form navbar-right searchBar', 'role' => 'search'])  !!}
                             <div class="input-group">
                             <input type="text" class="form-control" name="search" placeholder="Search..." style="border: ridge">
                             <span class="input-group-btn">
                             <button class="" type="submit">
                             <i class="material-icons" style="height: 27px !important;">search</i>
                              </button>
                              </span>
                              </div>
                     {!! Form::close() !!}
                     <div class="body">
                     <br>
                      <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Desktop Banner</th><th>Mobile Banner</th><th>Meta Title</th><th>Meta Keyword</th><th>Meta Description</th>
                                        @can('view_newsroompages','edit_newsroompages', 'delete_newsroompages')
                                        <th class="text-center">Actions</th>
                                        @endcan
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($newsroompages as $key => $item)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $item->desktop_banner }}</td><td>{{ $item->mobile_banner }}</td><td>{{ $item->meta_title }}</td><td>{{ $item->meta_keyword }}</td><td>{{ $item->meta_description }}</td>
                                        @can('view_newsroompages')
                                        <td class="text-center">
                                        <a href="{{ url('/admin/newsroompages/' . $item->id) }}" title="View Newsroompage"><button class="btn btn-info btn-xs"><i class="material-icons">remove_red_eye</i> View</button></a>
                                        @include('shared._actions', ['entity' => 'newsroompages',
                                        'id' => $item->id
                                        ])
                                         </td>
                                        @endcan
                                      </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $newsroompages->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

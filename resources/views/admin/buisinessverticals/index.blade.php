@extends('admin.layouts.master')
<style>
    .searchBar{
        margin-right:22px !important;
    }
</style>
@section('content')
    <div class="container-fluid">
        <div class="row">
                     <div class="col-md-12">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Buisiness verticals
                                        </h2>
                                    </div>
                     <br>
                     <a href="{{ url('/admin/buisinessverticals/create') }}" class="btn btn-success btn-sm waves-effect" title="Add New Buisinessvertical" style="margin-left: 22px;">
                        <i class="material-icons">add_circle</i> Add New
                     </a>
                     {!! Form::open(['method' => 'GET', 'url' => '/admin/buisinessverticals', 'class' => 'navbar-form navbar-right searchBar', 'role' => 'search'])  !!}
                             <div class="input-group">
                             <input type="text" class="form-control" name="search" placeholder="Search..." style="border: ridge">
                             <span class="input-group-btn">
                             <button class="" type="submit">
                             <i class="material-icons" style="height: 27px !important;">search</i>
                              </button>
                              </span>
                              </div>
                     {!! Form::close() !!}
                     <div class="body">
                     <br>
                      <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Featured Image</th><th>Title</th><th>Brands</th><th>Status</th>
                                        @can('view_buisinessverticals','edit_buisinessverticals', 'delete_buisinessverticals')
                                        <th class="text-center">Actions</th>
                                        @endcan
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($buisinessverticals as $key => $item)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>@if(isset($item->featured_image))<img src="{{ asset($item->featured_image )}}" height="100" width="100"> @endif</td>
                                        <td>{{ $item->title }}</td>
                                        <td>
                                            @if(isset($item->brands) && !empty($item->brands))
                                            <ul>
                                            @foreach($item->brands as $brand)
                                            <a href="{{ url('/admin/brands/' . $brand->id) }}"><li>{{$brand->title}} </li></a>
                                            @endforeach
                                            </ul>
                                            @endif
                                        </td>
                                        
                                        <td>{{ $item->status }}</td>
                                        @can('view_buisinessverticals')
                                        <td class="text-center">
                                        <a href="{{ url('/admin/buisinessverticals/' . $item->id) }}" title="View Buisinessvertical"><button class="btn btn-info btn-xs"><i class="material-icons">remove_red_eye</i> View</button></a>
                                        @include('shared._actions', ['entity' => 'buisinessverticals',
                                        'id' => $item->id
                                        ])
                                         </td>
                                        @endcan
                                      </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $buisinessverticals->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

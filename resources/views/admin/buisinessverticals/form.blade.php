  <label for="title" style="font-size:18px;">{{ 'Title' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="title" type="text" id="title" value="{{ isset($buisinessvertical->title) ? $buisinessvertical->title : ''}}" required>
     {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>

  <label for="icon" style="font-size:18px;">{{ 'Icon For About Us Page' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="icon" type="file" id="icon" value="{{ isset($buisinessvertical->icon) ? $buisinessvertical->icon : ''}}" >
     {!! $errors->first('icon', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($buisinessvertical->icon))
      <img src="{{asset($buisinessvertical->icon)}}" height="100" width="100">
      @endif
 </div><br>

 <label for="status_about_us" style="font-size:18px;">{{ 'Status For About Us Page' }}</label>
 <div class="">
      <div class="form-line">
        <select name="status_about_us" class="form-control" id="status_about_us" >
    @foreach (json_decode('{"Enabled": "Enabled", "Disabled": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($buisinessvertical->status_about_us) && $buisinessvertical->status_about_us == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <hr>
 <label for="status_buisiness_vertical" style="font-size:18px;">{{ 'Status For Buisiness Vertical Page' }}</label>
 <div class="">
      <div class="form-line">
        <select name="status_buisiness_vertical" class="form-control" id="status_buisiness_vertical" >
    @foreach (json_decode('{"Enabled": "Enabled", "Disabled": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($buisinessvertical->status_buisiness_vertical) && $buisinessvertical->status_buisiness_vertical == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="image" style="font-size:18px;">{{ 'Featured Image For Business Verical Page' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="image" type="file" id="image" value="{{ isset($buisinessvertical->image) ? $buisinessvertical->image : ''}}" >
     {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($buisinessvertical->image))
      <img src="{{asset($buisinessvertical->image)}}" height="100" width="100">
      @endif
 </div><br>
  <label for="image2" style="font-size:18px;">{{ 'Featured Image For Business Verical Page' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="image2" type="file" id="image2" value="{{ isset($buisinessvertical->image2) ? $buisinessvertical->image2 : ''}}" >
     {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($buisinessvertical->image2))
      <img src="{{asset($buisinessvertical->image2)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="description" style="font-size:18px;">{{ 'Description For Business Verical Page' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="description" type="textarea" id="description" >{{ isset($buisinessvertical->description) ? $buisinessvertical->description : ''}}</textarea>
     {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="description_industry_sub_sector" style="font-size:18px;">{{ 'Industry Sub-Sector Description For Business Verical Page' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="description_industry_sub_sector" type="textarea" id="description_industry_sub_sector" >{{ isset($buisinessvertical->description_industry_sub_sector) ? $buisinessvertical->description_industry_sub_sector : ''}}</textarea>
     {!! $errors->first('description_industry_sub_sector', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <br>
 <label for="skill_development_part1" style="font-size:20px;">{{ 'Sectors' }}</label><br>
 @php
 $i=1;
 @endphp
 <div id="milestones">
  @if(isset($buisinessvertical->subsectors) && !empty($buisinessvertical->subsectors))
  @php
  $milestones=json_decode($buisinessvertical->subsectors,true);
  @endphp
  @foreach($milestones as $milestone)
  <div id="milestone{{$i}}">
  <label for="featured_image" style="font-size:18px;">{{ 'Featured Image' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="mapping[{{$i}}][featured_image]" type="file" id="featured_image" >
     {!! $errors->first('featured_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($milestone['featured_image']) && !empty($milestone['featured_image']))
      <input type="hidden" name="mapping[{{$i}}][featured_image_original]" value="{{$milestone['featured_image']}}">
      <img src="{{asset($milestone['featured_image'])}}" height="100" width="100">
      @endif
 </div><br>

 <label for="title" style="font-size:18px;">{{ 'Title' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="mapping[{{$i}}][title]" type="text" id="title" value="{{ isset($milestone['title']) ? $milestone['title'] : ''}}" >
     {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
      </div></div><br>

  <label for="sectors" style="font-size:18px;">{{ 'Sectors' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="mapping[{{$i}}][sectors]" type="text" id="sectors" value="{{ isset($milestone['sectors']) ? $milestone['sectors'] : ''}}" >
     {!! $errors->first('sectors', '<p class="help-block">:message</p>') !!}
      </div>
      <button type="button" name="remove" id="{{$i}}" class="btn btn-danger removemilestone bt{{$i}}">Remove</button>
     
 </div><br>
 </div>
 
 @php
 $i++;
 @endphp
 @endforeach


 @endif
 <input type="hidden" id="milestonecount" value="{{$i}}">
 
 </div><br>
 <input type="button" class="btn btn-warning" id="addmilestone" value="Add More Sub-Sector">
 <br>
 <br>
 <label for="fast_facts_description" style="font-size:18px;">{{ 'Description For Fast Facts Business Vertical Page' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="fast_facts_description" type="textarea" id="fast_facts_description" >{{ isset($buisinessvertical->fast_facts_description) ? $buisinessvertical->fast_facts_description : ''}}</textarea>
     {!! $errors->first('fast_facts_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <br><br><br>
 <label for="skill_development_part1" style="font-size:20px;">{{ 'Vertical Fast facts' }}</label><br>
@php
 $i=1;
 @endphp
 <div id="cummunities">
  @if(isset($buisinessvertical->fast_facts) && !empty($buisinessvertical->fast_facts))
  @php
  $cummunities=json_decode($buisinessvertical->fast_facts,true);
  @endphp
  @foreach($cummunities as $cummunity)
  <div id="cummunity{{$i}}">
   <label for="featured_image" style="font-size:18px;">{{ 'Featured Image' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="newmapping[{{$i}}][featured_image]" type="file" id="featured_image" >
     {!! $errors->first('featured_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($cummunity['featured_image']) && !empty($cummunity['featured_image']))
      <input type="hidden" name="newmapping[{{$i}}][featured_image_original]" value="{{$cummunity['featured_image']}}">
      <img src="{{asset($cummunity['featured_image'])}}" height="100" width="100">
      @endif
 </div><br>
 <label for="title" style="font-size:18px;">{{ 'Title' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="newmapping[{{$i}}][title]" type="text" id="title" value="{{ isset($cummunity['title']) ? $cummunity['title'] : ''}}" >
     {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
      </div>
      
     
 </div><br>
 <label for="title" style="font-size:18px;">{{ 'Count' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="newmapping[{{$i}}][count]" type="text" id="count" value="{{ isset($cummunity['count']) ? $cummunity['count'] : ''}}" >
     {!! $errors->first('count', '<p class="help-block">:message</p>') !!}
      </div>
      
     
 </div><br>
 <label for="unit" style="font-size:18px;">{{ 'Unit' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="newmapping[{{$i}}][unit]" type="text" id="unit" value="{{ isset($cummunity['unit']) ? $cummunity['unit'] : ''}}" >
     {!! $errors->first('unit', '<p class="help-block">:message</p>') !!}
      </div>
      <button type="button" name="remove" id="{{$i}}" class="btn btn-danger removecummunity bt{{$i}}">Remove</button>
     
 </div><br>
 </div>
 <hr>
 @php
 $i++;
 @endphp
 @endforeach

 @endif
 <input type="hidden" id="cummunitycount" value="{{$i}}">
 
 </div><br>
 <input type="button" class="btn btn-warning" id="addcummunity" value="Add Vertical Fast Facts">
 <br>
 <br>
 <hr>
  <label for="featured_image" style="font-size:18px;">{{ 'Featured Image For Our Brand page' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="featured_image" type="file" id="featured_image" value="{{ isset($buisinessvertical->featured_image) ? $buisinessvertical->featured_image : ''}}" >
     {!! $errors->first('featured_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($buisinessvertical->featured_image))
      <img src="{{asset($buisinessvertical->featured_image)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="status" style="font-size:18px;">{{ 'Status For Our Brand Page' }}</label>
 <div class="">
      <div class="form-line">
        <select name="status" class="form-control" id="status" >
    @foreach (json_decode('{"Enabled": "Enabled", "Disabled": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($buisinessvertical->status) && $buisinessvertical->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <hr>
  <label for="status_newsroom_press_page" style="font-size:18px;">{{ 'Status For Newsroom Press Page' }}</label>
 <div class="">
      <div class="form-line">
        <select name="status_newsroom_press_page" class="form-control" id="status_newsroom_press_page" >
    @foreach (json_decode('{"Enabled": "Enabled", "Disabled": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($buisinessvertical->status_newsroom_press_page) && $buisinessvertical->status_newsroom_press_page == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
  <label for="status_newsroom_video_page" style="font-size:18px;">{{ 'Status For Newsroom Video Page' }}</label>
 <div class="">
      <div class="form-line">
        <select name="status_newsroom_video_page" class="form-control" id="status_newsroom_video_page" >
    @foreach (json_decode('{"Enabled": "Enabled", "Disabled": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($buisinessvertical->status_newsroom_video_page) && $buisinessvertical->status_newsroom_video_page == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
@section('scripts')
<script>
 $(document).ready(function(){
var j=$('#milestonecount').val();
    $('#addmilestone').click(function(){
        // alert('test')
            j++;
            $('#milestones').append('<div id="milestone'+j+'"><label for="featured_image" style="font-size:18px;">Featured Image</label><div class=""><div class="form-line"><input class="form-control" name="mapping['+j+'][featured_image]" type="file" id="featured_image" ></div></div><br><label for="title" style="font-size:18px;">Title</label><div class=""><div class="form-line"><input class="form-control" name="mapping['+j+'][title]" type="text" id="title"  ></div></div><br><label for="title" style="font-size:18px;">Sectors</label><div class=""><div class="form-line"><input class="form-control" name="mapping['+j+'][sectors]" type="text" id="sectors"  ></div><button type="button" name="remove" id="'+j+'" class="btn btn-danger removemilestone bt'+j+'">Remove</button></div><br></div>');
        
        });

        $(document).on('click', '.removemilestone', function(){
            var button1_id = $(this).attr("id");
            $('#milestone'+button1_id+'').remove();
            $('.bt'+button1_id+'').remove();

        });

          var m=$('#cummunitycount').val();
    $('#addcummunity').click(function(){
        // alert('test')
            m++;
            $('#cummunities').append('<div id="cummunity'+m+'"><label for="featured_image" style="font-size:18px;">Featured Image</label><div class=""><div class="form-line"><input class="form-control" name="newmapping['+m+'][featured_image]" type="file" id="featured_image" ></div></div><br><label for="title" style="font-size:18px;">Title</label><div class=""><div class="form-line"><input class="form-control" name="newmapping['+m+'][title]" type="text" id="title"  ></div></div><br><label for="count" style="font-size:18px;">Count</label><div class=""><div class="form-line"><input class="form-control" name="newmapping['+m+'][count]" type="text" id="count"  ></div></div><br><label for="title" style="font-size:18px;">Unit</label><div class=""><div class="form-line"><input class="form-control" name="newmapping['+m+'][unit]" type="text" id="unit"  ></div><button type="button" name="remove" id="'+m+'" class="btn btn-danger removecummunity bt'+m+'">Remove</button></div><br></div>');
        
        });

        $(document).on('click', '.removecummunity', function(){
            var button1_id = $(this).attr("id");
            $('#cummunity'+button1_id+'').remove();
            $('.bt'+button1_id+'').remove();

        });
          });
        </script>
        @endsection
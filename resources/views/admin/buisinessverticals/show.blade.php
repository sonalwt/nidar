@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Buisiness vertical  {{ $buisinessvertical->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/buisinessverticals') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_buisinessverticals', 'delete_buisinessverticals')
                         <a href="{{ url('/admin/buisinessverticals/' . $buisinessvertical->id . '/edit') }}" title="Edit Buisinessvertical"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/buisinessverticals' . '/' . $buisinessvertical->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Buisinessvertical" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $buisinessvertical->id }}</td>
                                    </tr>
                                    <tr><th> Featured Image </th><td> <img src="{{ asset($buisinessvertical->featured_image) }}" height="100" width="100"> </td></tr><tr><th> Title </th><td> {{ $buisinessvertical->title }} </td></tr><tr><th> Status </th><td> {{ $buisinessvertical->status }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

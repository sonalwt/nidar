<!--  <label for="featured_image" style="font-size:18px;">{{ 'Featured Image' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="featured_image" type="file" id="featured_image" value="{{ isset($milestone->featured_image) ? $milestone->featured_image : ''}}" required>
     {!! $errors->first('featured_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($milestone->featured_image))
      <img src="{{asset($milestone->featured_image)}}" height="100" width="100">
      @endif
 </div><br>
 <br> -->
 <br>
 <label for="year" style="font-size:18px;">{{ 'Year' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="year" type="text" id="year" value="{{ isset($milestone->year) ? $milestone->year : ''}}" required>
     {!! $errors->first('year', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="skill_development_part1" style="font-size:20px;">{{ 'milestones' }}</label><br>
 @php
 $i=1;
 @endphp
 <div id="milestones">
  @if(isset($milestone->title) && !empty($milestone->title))
  @php
  $milestones=json_decode($milestone->title,true);
  @endphp
  @foreach($milestones as $milestone)
  <div id="milestone{{$i}}">
  <label for="featured_image" style="font-size:18px;">{{ 'Featured Image' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="mapping[{{$i}}][featured_image]" type="file" id="featured_image" >
     {!! $errors->first('featured_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($milestone['featured_image']) && !empty($milestone['featured_image']))
      <input type="hidden" name="mapping[{{$i}}][featured_image_original]" value="{{$milestone['featured_image']}}">
      <img src="{{asset($milestone['featured_image'])}}" height="100" width="100">
      @endif
 </div><br>

 <label for="title" style="font-size:18px;">{{ 'Title' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="mapping[{{$i}}][title]" type="text" id="title" value="{{ isset($milestone['title']) ? $milestone['title'] : ''}}" >
     {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
      </div>
      <button type="button" name="remove" id="{{$i}}" class="btn btn-danger removemilestone bt{{$i}}">Remove</button>
     
 </div><br>
 </div>
 
 @php
 $i++;
 @endphp
 @endforeach
 @else
   <div id="milestone{{$i}}">
    <div class="">
      <div class="form-line">
        <input class="form-control" name="mapping[{{$i}}][featured_image]" type="file" id="featured_image" >
     {!! $errors->first('featured_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($milestone['featured_image']) && !empty($milestone['featured_image']))
      <input type="hidden" name="mapping[{{$i}}][featured_image_original]" value="{{$milestone['featured_image']}}">
      <img src="{{asset($milestone['featured_image'])}}" height="100" width="100">
      @endif
 </div><br>
 <label for="title" style="font-size:18px;">{{ 'Title' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="mapping[{{$i}}][title]" type="text" id="title" required>
     {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
      </div>
      <button type="button" name="remove" id="{{$i}}" class="btn btn-danger removemilestone bt{{$i}}">Remove</button>
     
 </div><br>
 </div>

 @endif
 <input type="hidden" id="milestonecount" value="{{$i}}">
 
 </div><br>
 <input type="button" class="btn btn-warning" id="addmilestone" value="Add More Milestone">
 <br>
 <br>
 <!-- <label for="year" style="font-size:18px;">{{ 'Year' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="year" type="text" id="year" value="{{ isset($milestone->year) ? $milestone->year : ''}}" required>
     {!! $errors->first('year', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br> -->
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="">
      <div class="form-line">
        <select name="status" class="form-control" id="status" required>
    @foreach (json_decode('{"Enabled": "Enabled", "Disabled": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($milestone->status) && $milestone->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
@section('scripts')
<script>
 $(document).ready(function(){
var j=$('#milestonecount').val();
    $('#addmilestone').click(function(){
        // alert('test')
            j++;
            $('#milestones').append('<div id="milestone'+j+'"><label for="featured_image" style="font-size:18px;">Featured Image</label><div class=""><div class="form-line"><input class="form-control" name="mapping['+j+'][featured_image]" type="file" id="featured_image" ></div></div><br><label for="title" style="font-size:18px;">Title</label><div class=""><div class="form-line"><input class="form-control" name="mapping['+j+'][title]" type="text" id="title"  ></div><button type="button" name="remove" id="'+j+'" class="btn btn-danger removemilestone bt'+j+'">Remove</button></div><br></div>');
        
        });

        $(document).on('click', '.removemilestone', function(){
            var button1_id = $(this).attr("id");
            $('#milestone'+button1_id+'').remove();
            $('.bt'+button1_id+'').remove();

        });
          });
        </script>
        @endsection
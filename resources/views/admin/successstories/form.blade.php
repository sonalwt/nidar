 <label for="featured_image" style="font-size:18px;">{{ 'Featured Image' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="featured_image" type="file" id="featured_image" value="{{ isset($successstory->featured_image) ? $successstory->featured_image : ''}}">
     {!! $errors->first('featured_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($successstory->featured_image) && !empty($successstory->featured_image))
      <img src="{{asset($successstory->featured_image)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="name" style="font-size:18px;">{{ 'Name' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="name" type="text" id="name" value="{{ isset($successstory->name) ? $successstory->name : ''}}" required>
     {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="course" style="font-size:18px;">{{ 'Course' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="course" type="text" id="course" value="{{ isset($successstory->course) ? $successstory->course : ''}}" required>
     {!! $errors->first('course', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="total_earnings" style="font-size:18px;">{{ 'Total Earnings' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="total_earnings" type="text" id="total_earnings" value="{{ isset($successstory->total_earnings) ? $successstory->total_earnings : ''}}" required>
     {!! $errors->first('total_earnings', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="description" style="font-size:18px;">{{ 'Description' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="description" type="textarea" id="description" required>{{ isset($successstory->description) ? $successstory->description : ''}}</textarea>
     {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="sort_order" style="font-size:18px;">{{ 'Sort Order' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="sort_order" type="text" id="sort_order" value="{{ isset($successstory->sort_order) ? $successstory->sort_order : ''}}" required>
     {!! $errors->first('sort_order', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="">
      <div class="form-line">
        <select name="status" class="form-control" id="status" required>
    @foreach (json_decode('{"Enabled": "Enabled", "Disabled": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($successstory->status) && $successstory->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>

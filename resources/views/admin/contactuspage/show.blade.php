@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Contactuspage  {{ $contactuspage->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/contactuspage') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_contactuspage', 'delete_contactuspage')
                         <a href="{{ url('/admin/contactuspage/' . $contactuspage->id . '/edit') }}" title="Edit Contactuspage"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/contactuspage' . '/' . $contactuspage->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Contactuspage" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $contactuspage->id }}</td>
                                    </tr>
                                    <tr><th> Desktop Banner </th><td> {{ $contactuspage->desktop_banner }} </td></tr><tr><th> Mobile Banner </th><td> {{ $contactuspage->mobile_banner }} </td></tr><tr><th> India Image </th><td> {{ $contactuspage->india_image }} </td></tr><tr><th> India Address </th><td> {{ $contactuspage->india_address }} </td></tr><tr><th> India Mobile </th><td> {{ $contactuspage->india_mobile }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

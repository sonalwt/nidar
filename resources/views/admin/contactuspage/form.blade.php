 <label for="desktop_banner" style="font-size:18px;">{{ 'Desktop Banner' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="desktop_banner" type="file" id="desktop_banner" value="{{ isset($contactuspage->desktop_banner) ? $contactuspage->desktop_banner : ''}}" >
     {!! $errors->first('desktop_banner', '<p class="help-block">:message</p>') !!}
      </div>
       @if(isset($contactuspage->desktop_banner))
      <img src="{{asset($contactuspage->desktop_banner)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="mobile_banner" style="font-size:18px;">{{ 'Mobile Banner' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="mobile_banner" type="file" id="mobile_banner" value="{{ isset($contactuspage->mobile_banner) ? $contactuspage->mobile_banner : ''}}" >
     {!! $errors->first('mobile_banner', '<p class="help-block">:message</p>') !!}
      </div>
       @if(isset($contactuspage->mobile_banner))
      <img src="{{asset($contactuspage->mobile_banner)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="india_image" style="font-size:18px;">{{ 'India Image' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="india_image" type="file" id="india_image" value="{{ isset($contactuspage->india_image) ? $contactuspage->india_image : ''}}" >
     {!! $errors->first('india_image', '<p class="help-block">:message</p>') !!}
      </div>
       @if(isset($contactuspage->india_image))
      <img src="{{asset($contactuspage->india_image)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="india_address" style="font-size:18px;">{{ 'India Address' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="india_address" type="textarea" id="india_address" >{{ isset($contactuspage->india_address) ? $contactuspage->india_address : ''}}</textarea>
     {!! $errors->first('india_address', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="india_mobile" style="font-size:18px;">{{ 'India Mobile' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="india_mobile" type="text" id="india_mobile" value="{{ isset($contactuspage->india_mobile) ? $contactuspage->india_mobile : ''}}" >
     {!! $errors->first('india_mobile', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="uae_image" style="font-size:18px;">{{ 'Uae Image' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="uae_image" type="file" id="uae_image" value="{{ isset($contactuspage->uae_image) ? $contactuspage->uae_image : ''}}" >
     {!! $errors->first('uae_image', '<p class="help-block">:message</p>') !!}
      </div>
       @if(isset($contactuspage->uae_image))
      <img src="{{asset($contactuspage->uae_image)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="uae_address" style="font-size:18px;">{{ 'Uae Address' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="uae_address" type="textarea" id="uae_address" >{{ isset($contactuspage->uae_address) ? $contactuspage->uae_address : ''}}</textarea>
     {!! $errors->first('uae_address', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="uae_mobile" style="font-size:18px;">{{ 'Uae Mobile' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="uae_mobile" type="text" id="uae_mobile" value="{{ isset($contactuspage->uae_mobile) ? $contactuspage->uae_mobile : ''}}" >
     {!! $errors->first('uae_mobile', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="map" style="font-size:18px;">{{ 'Map' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control" rows="5" name="map" type="textarea" id="map" >{{ isset($contactuspage->map) ? $contactuspage->map : ''}}</textarea>
     {!! $errors->first('map', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
  <label for="meta_title" style="font-size:18px;">{{ 'Meta Title' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_title" type="text" id="meta_title" value="{{ isset($contactuspage->meta_title) ? $contactuspage->meta_title : ''}}" >
     {!! $errors->first('meta_title', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
  <label for="meta_keyword" style="font-size:18px;">{{ 'Meta Keyword' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_keyword" type="text" id="meta_keyword" value="{{ isset($contactuspage->meta_keyword) ? $contactuspage->meta_keyword : ''}}" >
     {!! $errors->first('meta_keyword', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
  <label for="meta_description" style="font-size:18px;">{{ 'Meta Description' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_description" type="text" id="meta_description" value="{{ isset($contactuspage->meta_description) ? $contactuspage->meta_description : ''}}" >
     {!! $errors->first('meta_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="meta_image" style="font-size:18px;">{{ 'Meta Image' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_image" type="file" id="meta_image" value="{{ isset($contactuspage->meta_image) ? $contactuspage->meta_image : ''}}" >
     {!! $errors->first('meta_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($contactuspage->meta_image))
      <img src="{{asset($contactuspage->meta_image)}}" height="100" width="100">
      @endif
 </div><br>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>

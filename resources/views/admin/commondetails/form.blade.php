 <label for="logo" style="font-size:18px;">{{ 'Logo' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="logo" type="file" id="logo" value="{{ isset($commondetail->logo) ? $commondetail->logo : ''}}" >
     {!! $errors->first('logo', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($commondetail->logo) && !empty($commondetail->logo))
      <img src="{{asset($commondetail->logo)}}" height="100" width="100">
      @endif
 </div><br>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>

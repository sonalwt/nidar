@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Brand  {{ $brand->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/brands') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_brands', 'delete_brands')
                         <a href="{{ url('/admin/brands/' . $brand->id . '/edit') }}" title="Edit Brand"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/brands' . '/' . $brand->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Brand" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $brand->id }}</td>
                                    </tr>
                                    <tr><th> Buisiness Vertical </th><td> {{ $brand->vertical->title }} </td></tr>
                                    <tr><th> Featured Image </th><td> <img src="{{ asset($brand->featured_image) }}" height="100" width="100"> </td></tr>
                                    <tr><th> Brand Logo </th><td> <img src="{{ asset($brand->brand_logo) }}" height="100" width="100"> </td></tr>
                                    <tr><th> Title </th><td> {{ $brand->title }} </td>
                                    </tr><tr><th> Short Description </th>
                                        <td> {{ $brand->short_description }} </td></tr>
                                        <tr><th> Description </th><td> {{ $brand->description }} </td></tr>
                                         <tr><th> Sort Order </th><td> {{ $brand->sort_order }} </td></tr>
                                        <tr><th> Status </th><td> {{ $brand->status }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

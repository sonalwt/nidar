 <label for="buisiness_vertical_id" style="font-size:18px;">{{ 'Select Buisiness Vertical' }}</label>
 <div class="">
      <div class="form-line">
        <select name="buisiness_vertical_id" class="form-control" id="buisiness_vertical_id" required>
          <option>Select Buisiness Vertical</option>
    @foreach ($verticals as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($brand->buisiness_vertical_id) && $brand->buisiness_vertical_id == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="featured_image" style="font-size:18px;">{{ 'Featured Image (Desktop)' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="featured_image" type="file" id="featured_image" value="{{ isset($brand->featured_image) ? $brand->featured_image : ''}}" required>
     {!! $errors->first('featured_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($brand->featured_image) && !empty($brand->featured_image))
      <img src="{{asset($brand->featured_image)}}" height="100" width="100">
      @endif
 </div><br>
  <label for="featured_image_mob" style="font-size:18px;">{{ 'Featured Image (Mobile)' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="featured_image_mob" type="file" id="featured_image_mob" value="{{ isset($brand->featured_image_mob) ? $brand->featured_image_mob : ''}}" required>
     {!! $errors->first('featured_image_mob', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($brand->featured_image_mob) && !empty($brand->featured_image_mob))
      <img src="{{asset($brand->featured_image_mob)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="home_page_image" style="font-size:18px;">{{ 'Home page' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="home_page_image" type="file" id="home_page_image" value="{{ isset($brand->home_page_image) ? $brand->home_page_image : ''}}" required>
     {!! $errors->first('home_page_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($brand->home_page_image) && !empty($brand->home_page_image))
      <img src="{{asset($brand->home_page_image)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="brand_logo" style="font-size:18px;">{{ 'Brand Logo' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="brand_logo" type="file" id="brand_logo" value="{{ isset($brand->brand_logo) ? $brand->brand_logo : ''}}" required>
     {!! $errors->first('brand_logo', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($brand->brand_logo) && !empty($brand->brand_logo))
      <img src="{{asset($brand->brand_logo)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="title" style="font-size:18px;">{{ 'Title' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="title" type="text" id="title" value="{{ isset($brand->title) ? $brand->title : ''}}" required>
     {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
      </div>

 </div><br>
 <label for="short_description" style="font-size:18px;">{{ 'Short Description' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="short_description" type="text" id="short_description" value="{{ isset($brand->short_description) ? $brand->short_description : ''}}" required>
     {!! $errors->first('short_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="description" style="font-size:18px;">{{ 'Description' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="description" type="textarea" id="description" required>{{ isset($brand->description) ? $brand->description : ''}}</textarea>
     {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="sort_order" style="font-size:18px;">{{ 'Sort Order' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="sort_order" type="text" id="sort_order" value="{{ isset($brand->sort_order) ? $brand->sort_order : ''}}" required>
     {!! $errors->first('sort_order', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <!-- <label for="slug" style="font-size:18px;">{{ 'Slug' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="slug" type="text" id="slug" value="{{ isset($brand->slug) ? $brand->slug : ''}}" >
     {!! $errors->first('slug', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br> -->
 <label for="meta_title" style="font-size:18px;">{{ 'Meta Title' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_title" type="text" id="meta_title" value="{{ isset($brand->meta_title) ? $brand->meta_title : ''}}" >
     {!! $errors->first('meta_title', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="meta_keyword" style="font-size:18px;">{{ 'Meta Keyword' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_keyword" type="text" id="meta_keyword" value="{{ isset($brand->meta_keyword) ? $brand->meta_keyword : ''}}" >
     {!! $errors->first('meta_keyword', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="meta_description" style="font-size:18px;">{{ 'Meta Description' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_description" type="text" id="meta_description" value="{{ isset($brand->meta_description) ? $brand->meta_description : ''}}" >
     {!! $errors->first('meta_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="meta_image" style="font-size:18px;">{{ 'Meta Image' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_image" type="file" id="meta_image" value="{{ isset($brand->meta_image) ? $brand->meta_image : ''}}" >
     {!! $errors->first('meta_image', '<p class="help-block">:message</p>') !!}
      </div>
       @if(isset($brand->meta_image) && !empty($brand->meta_image))
      <img src="{{asset($brand->meta_image)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="">
      <div class="form-line">
        <select name="status" class="form-control" id="status" required>
    @foreach (json_decode('{"Enabled": "Enabled", "Disabled": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($brand->status) && $brand->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>

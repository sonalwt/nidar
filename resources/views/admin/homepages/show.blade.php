@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Homepage  {{ $homepage->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/homepages') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_homepages', 'delete_homepages')
                         <a href="{{ url('/admin/homepages/' . $homepage->id . '/edit') }}" title="Edit Homepage"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/homepages' . '/' . $homepage->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Homepage" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $homepage->id }}</td>
                                    </tr>
                                    <tr><th> Banner </th><td> {{ $homepage->banner }} </td></tr><tr><th> News1 </th><td> {{ $homepage->news1 }} </td></tr><tr><th> News2 </th><td> {{ $homepage->news2 }} </td></tr><tr><th> News3 </th><td> {{ $homepage->news3 }} </td></tr><tr><th> Equal Opportunity Employer Description </th><td> {{ $homepage->equal_opportunity_employer_description }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


 <label for="skill_development_part1" style="font-size:20px;">{{ 'Banners' }}</label><br>
 @php
 $i=1;
 @endphp
 <div id="milestones">
  @if(isset($homepage->banner) && !empty($homepage->banner))
  @php
  $milestones=json_decode($homepage->banner,true);
  @endphp
  @foreach($milestones as $milestone)
  <div id="milestone{{$i}}">
  <label for="featured_image" style="font-size:18px;">{{ 'Featured Image(.jpg/.png) For Desktop' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="mapping[{{$i}}][featured_image]" type="file" id="featured_image" >
     {!! $errors->first('featured_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($milestone['featured_image']) && !empty($milestone['featured_image']))
      <input type="hidden" name="mapping[{{$i}}][featured_image_original]" value="{{$milestone['featured_image']}}">
      <img src="{{asset($milestone['featured_image'])}}" height="100" width="100">
      @endif
 </div><br>
   <label for="featured_image_webp" style="font-size:18px;">{{ 'Featured Image (.webp) For Desktop' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="mapping[{{$i}}][featured_image_webp]" type="file" id="featured_image_webp" >
     {!! $errors->first('featured_image_webp', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($milestone['featured_image_webp']) && !empty($milestone['featured_image_webp']))
      <input type="hidden" name="mapping[{{$i}}][featured_image_webp_original]" value="{{$milestone['featured_image_webp']}}">
      <img src="{{asset($milestone['featured_image_webp'])}}" height="100" width="100">
      @endif
      
 </div><br>
 <label for="featured_image_mobile" style="font-size:18px;">{{ 'Featured Image(.jpg/.png) For Mobile' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="mapping[{{$i}}][featured_image_mobile]" type="file" id="featured_image_mobile" >
     {!! $errors->first('featured_image_mobile', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($milestone['featured_image_mobile']) && !empty($milestone['featured_image_mobile']))
      <input type="hidden" name="mapping[{{$i}}][featured_image_mobile_original]" value="{{$milestone['featured_image_mobile']}}">
      <img src="{{asset($milestone['featured_image_mobile'])}}" height="100" width="100">
      @endif
 </div><br>
   <label for="featured_image_mobile_webp" style="font-size:18px;">{{ 'Featured Image (.webp) For Mobile' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="mapping[{{$i}}][featured_image_mobile_webp]" type="file" id="featured_image_mobile_webp" >
     {!! $errors->first('featured_image_mobile_webp', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($milestone['featured_image_mobile_webp']) && !empty($milestone['featured_image_mobile_webp']))
      <input type="hidden" name="mapping[{{$i}}][featured_image_mobile_webp_original]" value="{{$milestone['featured_image_mobile_webp']}}">
      <img src="{{asset($milestone['featured_image_mobile_webp'])}}" height="100" width="100">
      @endif
      <button type="button" name="remove" id="{{$i}}" class="btn btn-danger removemilestone bt{{$i}}">Remove</button>
 </div><br>

 </div>
 
 @php
 $i++;
 @endphp
 @endforeach
 

 @endif
 <input type="hidden" id="milestonecount" value="{{$i}}">
 
 </div><br>
 <input type="button" class="btn btn-warning" id="addmilestone" value="Add More Banner">
 <br>
 <br>
  <label for="news1for" style="font-size:18px;">{{ 'News1 For' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="news1for" type="text" id="news1for" value="{{ isset($homepage->news1for) ? $homepage->news1for : ''}}" required>
     {!! $errors->first('news1for', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
  <label for="news1title" style="font-size:18px;">{{ 'News1 Title' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="news1title" type="text" id="news1title" value="{{ isset($homepage->news1title) ? $homepage->news1title : ''}}" >
     {!! $errors->first('news1title', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="news1" style="font-size:18px;">{{ 'News1' }}</label>
 <div class="">
       <div class="form-line">
        <select name="news1" class="form-control" id="news1" >
    @foreach ($news as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($homepage->news1) && $homepage->news1 == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    
     {!! $errors->first('news1', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
  <label for="news2for" style="font-size:18px;">{{ 'News2 Image' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="news2image" type="file" id="news2image" value="{{ isset($homepage->news2image) ? $homepage->news2image : ''}}" >
     {!! $errors->first('news2image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($homepage->news2image) && !empty($homepage->news2image))
      <img src="{{asset($homepage->news2image)}}" height="100" width="100">
      @endif
 </div><br>
  <label for="news2for" style="font-size:18px;">{{ 'News2 For' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="news2for" type="text" id="news2for" value="{{ isset($homepage->news2for) ? $homepage->news2for : ''}}" required>
     {!! $errors->first('news2for', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="news2" style="font-size:18px;">{{ 'News2' }}</label>
 <div class="">
      <div class="form-line">
         <select name="news2" class="form-control" id="news2" >
    @foreach ($news as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($homepage->news2) && $homepage->news2 == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('news2', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
   <label for="news3for" style="font-size:18px;">{{ 'News3 For' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="news3for" type="text" id="news3for" value="{{ isset($homepage->news3for) ? $homepage->news3for : ''}}" required>
     {!! $errors->first('news3for', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="news3" style="font-size:18px;">{{ 'News3' }}</label>
 <div class="">
      <div class="form-line">
          <select name="news3" class="form-control" id="news3" >
    @foreach ($news as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($homepage->news3) && $homepage->news3 == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('news3', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="equal_opportunity_employer_description" style="font-size:18px;">{{ 'Equal Opportunity Employer Description' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="equal_opportunity_employer_description" type="textarea" id="equal_opportunity_employer_description" >{{ isset($homepage->equal_opportunity_employer_description) ? $homepage->equal_opportunity_employer_description : ''}}</textarea>
     {!! $errors->first('equal_opportunity_employer_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="equal_opportunity_employer_image" style="font-size:18px;">{{ 'Equal Opportunity Employer Image' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="equal_opportunity_employer_image" type="file" id="equal_opportunity_employer_image" value="{{ isset($homepage->equal_opportunity_employer_image) ? $homepage->equal_opportunity_employer_image : ''}}" >
     {!! $errors->first('equal_opportunity_employer_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($homepage->equal_opportunity_employer_image) && !empty($homepage->equal_opportunity_employer_image))
      <img src="{{asset($homepage->equal_opportunity_employer_image)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="careers_descriptione" style="font-size:18px;">{{ 'Careers Description' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="careers_descriptione" type="textarea" id="careers_descriptione" >{{ isset($homepage->careers_descriptione) ? $homepage->careers_descriptione : ''}}</textarea>
     {!! $errors->first('careers_descriptione', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="careers_image" style="font-size:18px;">{{ 'Careers Image' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="careers_image" type="file" id="careers_image" value="{{ isset($homepage->careers_image) ? $homepage->careers_image : ''}}" >
     {!! $errors->first('careers_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($homepage->careers_image) && !empty($homepage->careers_image))
      <img src="{{asset($homepage->careers_image)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="current_openings_image" style="font-size:18px;">{{ 'Current Openings Image' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="current_openings_image" type="file" id="current_openings_image" value="{{ isset($homepage->current_openings_image) ? $homepage->current_openings_image : ''}}" >
     {!! $errors->first('current_openings_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($homepage->current_openings_image) && !empty($homepage->current_openings_image))
      <img src="{{asset($homepage->current_openings_image)}}" height="100" width="100">
      @endif
 </div><br>
  <label for="work_culture_image" style="font-size:18px;">{{ 'Work Culture Image' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="work_culture_image" type="file" id="work_culture_image" value="{{ isset($homepage->work_culture_image) ? $homepage->work_culture_image : ''}}" >
     {!! $errors->first('work_culture_image', '<p class="help-block">:message</p>') !!}
      </div>
       @if(isset($homepage->work_culture_image) && !empty($homepage->work_culture_image))
      <img src="{{asset($homepage->work_culture_image)}}" height="100" width="100">
      @endif
 </div>
 <label for="work_culture_description" style="font-size:18px;">{{ 'Work Culture Description' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="work_culture_description" type="textarea" id="work_culture_description" >{{ isset($homepage->work_culture_description) ? $homepage->work_culture_description : ''}}</textarea>
     {!! $errors->first('work_culture_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="meta_title" style="font-size:18px;">{{ 'Meta Title' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_title" type="text" id="meta_title" value="{{ isset($homepage->meta_title) ? $homepage->meta_title : ''}}" >
     {!! $errors->first('meta_title', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="meta_keyword" style="font-size:18px;">{{ 'Meta Keyword' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_keyword" type="text" id="meta_keyword" value="{{ isset($homepage->meta_keyword) ? $homepage->meta_keyword : ''}}" >
     {!! $errors->first('meta_keyword', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="meta_description" style="font-size:18px;">{{ 'Meta Description' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_description" type="text" id="meta_description" value="{{ isset($homepage->meta_description) ? $homepage->meta_description : ''}}" >
     {!! $errors->first('meta_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="meta_image" style="font-size:18px;">{{ 'Meta Image' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="meta_image" type="file" id="meta_image" value="{{ isset($homepage->meta_image) ? $homepage->meta_image : ''}}" >
     {!! $errors->first('meta_image', '<p class="help-block">:message</p>') !!}
      </div>
       @if(isset($homepage->meta_image) && !empty($homepage->meta_image))
      <img src="{{asset($homepage->meta_image)}}" height="100" width="100">
      @endif
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
@section('scripts')
<script>
 $(document).ready(function(){
var j=$('#milestonecount').val();
    $('#addmilestone').click(function(){
        // alert('test')
            j++;
          $('#milestones').append('<hr><div id="milestone'+j+'"><label for="featured_image" style="font-size:18px;">Featured Image (.jpg/.png)</label><div class=""><div class="form-line"><input class="form-control" name="mapping['+j+'][featured_image]" type="file" id="featured_image" ></div></div><br><label for="title" style="font-size:18px;">Featured Image (.webp)</label><div class=""><div class="form-line"><input class="form-control" name="mapping['+j+'][featured_image_webp]"id="title" type="file"  ></div></div><br><label for="featured_image" style="font-size:18px;">Featured Image (.jpg/.png) For Mobile</label><div class=""><div class="form-line"><input class="form-control" name="mapping['+j+'][featured_image_mobile]" type="file" id="featured_image_mobile" ></div></div><br><label for="title" style="font-size:18px;">Featured Image (.webp) For Mobile</label><div class=""><div class="form-line"><input class="form-control" name="mapping['+j+'][featured_image_mobile_webp]"id="title" type="file"  ></div><button type="button" name="remove" id="'+j+'" class="btn btn-danger removemilestone bt'+j+'">Remove</button></div><br></div>');
        
        
        });

        $(document).on('click', '.removemilestone', function(){
            var button1_id = $(this).attr("id");
            $('#milestone'+button1_id+'').remove();
            $('.bt'+button1_id+'').remove();

        });
          });
        </script>
        @endsection
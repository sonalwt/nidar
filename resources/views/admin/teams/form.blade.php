 <label for="image" style="font-size:18px;">{{ 'Image' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="image" type="file" id="image" value="{{ isset($team->image) ? $team->image : ''}}" >
     {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($team->image) && !empty($team->image))
      <img src="{{asset($team->image)}}" height="100" width="100">
      @endif
 </div><br>
 <label for="name" style="font-size:18px;">{{ 'Name' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="name" type="text" id="name" value="{{ isset($team->name) ? $team->name : ''}}" required>
     {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="designation" style="font-size:18px;">{{ 'Designation' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="designation" type="text" id="designation" value="{{ isset($team->designation) ? $team->designation : ''}}" required>
     {!! $errors->first('designation', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="business_vertical" style="font-size:18px;">{{ 'Business Vertical' }}</label>
 <div class="">
      <div class="form-line">
        <input class="form-control" name="business_vertical" type="text" id="business_vertical" value="{{ isset($team->business_vertical) ? $team->business_vertical : ''}}" >
     {!! $errors->first('business_vertical', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="details" style="font-size:18px;">{{ 'Details' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="details" type="textarea" id="details" >{{ isset($team->details) ? $team->details : ''}}</textarea>
     {!! $errors->first('details', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
 <label for="bio" style="font-size:18px;">{{ 'Bio' }}</label>
 <div class="">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="bio" type="textarea" id="bio" >{{ isset($team->bio) ? $team->bio : ''}}</textarea>
     {!! $errors->first('bio', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br>
  <label for="team_type" style="font-size:18px;">{{ 'Team Type' }}</label>
 <div class="">
      <div class="form-line">
        <select name="team_type" class="form-control" id="team_type" required>
    @foreach (json_decode('{"Advisers": "Advisers", "Key Management": "Key Management"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($team->team_type) && $team->team_type == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="">
      <div class="form-line">
        <select name="status" class="form-control" id="status" required>
    @foreach (json_decode('{"Enabled": "Enabled", "Disabled": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($team->status) && $team->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>

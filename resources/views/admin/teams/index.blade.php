@extends('admin.layouts.master')
<style>
    .searchBar{
        margin-right:22px !important;
    }
</style>
@section('content')
    <div class="container-fluid">
        <div class="row">
                     <div class="col-md-12">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Teams
                                        </h2>
                                    </div>
                     <br>
                     <a href="{{ url('/admin/teams/create') }}" class="btn btn-success btn-sm waves-effect" title="Add New Team" style="margin-left: 22px;">
                        <i class="material-icons">add_circle</i> Add New
                     </a>
                     {!! Form::open(['method' => 'GET', 'url' => '/admin/teams', 'class' => 'navbar-form navbar-right searchBar', 'role' => 'search'])  !!}
                             <div class="input-group">
                             <input type="text" class="form-control" name="search" placeholder="Search..." style="border: ridge">
                             <span class="input-group-btn">
                             <button class="" type="submit">
                             <i class="material-icons" style="height: 27px !important;">search</i>
                              </button>
                              </span>
                              </div>
                     {!! Form::close() !!}
                     <div class="body">
                     <br>
                      <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Image</th><th>Name</th><th>Designation</th><th>Business Vertical</th><th>Status</th>
                                        @can('view_teams','edit_teams', 'delete_teams')
                                        <th class="text-center">Actions</th>
                                        @endcan
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($teams as $key => $item)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td> @if(isset($item->image) && !empty($item->image))
      <img src="{{asset($item->image)}}" height="100" width="100">
      @endif</td><td>{{ $item->name }}</td><td>{{ $item->designation }}</td><td>{{ $item->business_vertical }}</td><td>{{ $item->status }}</td>
                                        @can('view_teams')
                                        <td class="text-center">
                                        <a href="{{ url('/admin/teams/' . $item->id) }}" title="View Team"><button class="btn btn-info btn-xs"><i class="material-icons">remove_red_eye</i> View</button></a>
                                        @include('shared._actions', ['entity' => 'teams',
                                        'id' => $item->id
                                        ])
                                         </td>
                                        @endcan
                                      </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $teams->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

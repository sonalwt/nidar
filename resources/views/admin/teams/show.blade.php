@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Team  {{ $team->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/teams') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_teams', 'delete_teams')
                         <a href="{{ url('/admin/teams/' . $team->id . '/edit') }}" title="Edit Team"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/teams' . '/' . $team->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Team" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $team->id }}</td>
                                    </tr>
                                    <tr><th> Image </th><td>  @if(isset($team->image) && !empty($team->image))
      <img src="{{asset($team->image)}}" height="100" width="100">
      @endif </td></tr><tr><th> Name </th><td> {{ $team->name }} </td></tr><tr><th> Designation </th><td> {{ $team->designation }} </td></tr><tr><th> Business Vertical </th><td> {{ $team->business_vertical }} </td></tr><tr><th> Details </th><td> {{ $team->details }} </td></tr>
      <tr><th> Bio </th><td> {{ $team->bio }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

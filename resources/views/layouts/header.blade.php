<!-- preloader -->
<div id="preloader">
 <div class="loader-block">
  <img src="{{asset('assets/images/logo.png')}}" class="img-responsive logo" alt="logo">
<div class="loading-dots">
  <div class="loading-dots--dot"></div>
  <div class="loading-dots--dot"></div>
  <div class="loading-dots--dot"></div>
</div>
</div> 
</div>
<!-- preloader end -->

     <header id="header">
         <div class="container-fluid nopadding">
            <nav id="main-navbar" class="navbar navbar-fixed-top">
               <div class="container-fluid nopadding">
                  <div class="d-flex flex-justify-center flex-align-center d-block-mob menu-mob">
                     <div class="navbar-header">
                        <a class="logo" href="{{url('/')}}"><img src="{{asset('assets/images/logo.png')}}" class="img-responsive logo" alt="logo"></a>
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>

                        <!-- <a href="javascript:void(0);" class="search-icon search-icon-mob visible-xs"><img src="images/search.png"></a> -->
                     </div>
                     <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav menu-nav">
                           <li class="active"><a href="{{url('/')}}">Home</a></li>                        
                           
                           <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">About Us
                                <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                 <li><a href="{{url('about-us')}}">About Us</a></li>
                                  <li><a href="{{url('business-overview')}}">Business Overview</a></li>
                                  <li><a href="{{url('business-verticals')}}">Business Verticals</a></li>
                                  <!-- <li><a href="our-brands.php">Our Brands</a></li> -->
                                   <li><a href="{{url('leader-team')}}">Our Leadership</a></li>
                                 
                                </ul>
                            </li>                        
                           <!-- <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">Our Beliefs
                                <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                  <li><a href="our-belief.php">The Community</a></li>
                                  <li><a href="our-belief.php">Our Initiatives</a></li>
                                  <li><a href="our-belief.php">CSR</a></li>
                                 
                                </ul>
                            </li> -->

                          <li><a href="{{url('our-brands')}}">Our Brands</a></li>
                           <li><a href="{{url('our-impact')}}">Our Impact</a></li> 
                         <!--   <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">We, the people
                                <span class="caret"></span></a>
                                <ul class="dropdown-menu">                                  
                                  <li><a href="leader-team.php">The Leadership Team</a></li>                               
                                </ul>
                              </li> -->


                            <!-- <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">Newsroom
                                <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                  <li><a href="javascript:void(0);">Press Releases</a></li>
                                  <li><a href="javascript:void(0);">Newsletters</a></li>
                                  <li><a href="javascript:void(0);">Media Resources</a></li>
                                
                                </ul>
                            </li> -->
                           <li><a href="{{url('newsroom')}}">Newsroom</a></li>                        
                           <li><a href="{{url('career')}}">Careers</a></li>                        

                          <!--   <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">Careers
                                <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                  <li><a href="current-opening.php">Current openings</a></li>
                                  <li><a href="">Working from home</a></li>
                                  <li><a href="">Equal opportunity employer</a></li>                                
                                
                                </ul>
                            </li> -->
                             <!-- <li><a href="partner-with-us.php">Partner with us</a></li> -->
                           <!--  <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">Partner with us
                                <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                  <li><a href="partner-with-us.php">Outline</a></li>
                                  <li><a href="partner-with-us.php">Vendor Lead Capture Form</a></li>
                                  <li><a href="partner-with-us.php">Location Map</a></li>                                 
                                
                                </ul>
                            </li> -->
                         
                        </ul>
                        <ul class="nav d-flex navbar-right flex-align-center">                          
                           <li><a href="{{url('contact-us')}}" class="btn btn-all btn-orange btn-contact">Contact us</a></li>
                          <!--   <li><a href="javascript:void(0);" class="search-icon search-icon-desk hidden-xs"><img src="images/search.png"></a></li> -->
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="clearfix"></div>
            </nav>
         </div>
      </header>
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="keywords" content="@yield('keywords')"/>
    <meta name="description" content="@yield('description')"/>
   
     <title>  @yield('title')</title>
      <meta name="description" content="The Nidar Group is an ever-evolving organization. The conglomerate has marked its presence from Real Estate, Energy, Infrastructure, big data to education, healthcare, and Hospitality." />
    <!-- Styles -->
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->
    <!-- Latest compiled and minified CSS -->
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-101876138-1"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-101876138-1');
      </script>

      <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}" type="image/x-icon">
      <link rel="icon" href="{{asset('assets/images/favicon.ico')}}" type="image/x-icon">
     
      <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{asset('assets/css/style.css')}}" type="text/css">
      <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}" type="text/css">
      <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}" type="text/css">
      <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.min.css')}}"/>
      <link rel="stylesheet" href="{{asset('assets/css/owl.theme.default.min.css')}}"/>
      <link rel="stylesheet" href="{{asset('assets/css/slick.min.css')}}"/>
      <link rel="stylesheet" href="{{asset('assets/css/slick-theme.min.css')}}"/>
      
</head>
<body class="home">
    @include('layouts.header')
 @yield('content')
 @include('layouts.footer')
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/main.js')}}"></script>
<script src="{{asset('assets/js/slick.min.js')}}"></script>
<script src="{{asset('assets/js/customGsap.js')}}"></script>
<script src="{{asset('assets/js/gsap.min.js')}}"></script>
<script src="{{asset('assets/js/scrollMagic.js')}}"></script>
<script src="{{asset('assets/js/TweenMax.min.js')}}"></script>
<script src="{{asset('assets/js/animation.gsap.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>
<script src="{{asset('assets/js/waypoints.min.js')}}"></script>
<script src="{{asset('assets/js/owl.carousel.js')}}"></script>
 @yield('scripts')
<script type="text/javascript">
    $(document).ready(function(){
           
          $("#subscribe_form").submit(function(e) {
          //  var subscribe_email = $('#subscribe_email').val();  
                    $("#subbtn").val('Please Wait...');
                    var email_id = $('#subscribe_email').val();
                    var ajax_data = {
                        email_id : email_id
                                        };
                    $.ajax({
                        type: "POST",
                        url: '/ajax_submit_subscribe_page_form',
                        data: ajax_data,
                        success: function(data) {
                            if (data == 1) {
                                $("#subbtn").val('Submit');
                                $('#subscribe_form').trigger('reset');
                                $('#successsub').html('Thank you for Subscribing!')
                                
                                 $('#dangersub').html('');
                                 setTimeout(function () { location.reload(1); }, 3000);
                            } else {
                                $("#contact_submit").val('Submit');
                                $('#dangersub').html('Something went wrong')
                            }
                        }
                    });
                
            });
           
         
         });
</script>

</body>
</html>

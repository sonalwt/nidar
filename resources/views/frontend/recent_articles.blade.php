@extends('layouts.app')
@section('content')
 <section id="" class="grey-background section-space position-relative min-height-100"> 
         <div class="top-space-80 mob-space-30"></div>
         <div class="landing-container container-without-lft-line">   

            <div class="row">
              <div class="col-md-12">  
              <div>
                 <h2 class="main-title green-title mg-bot-title big-line-title">Recent Articles</h2>
              </div> 
                <ul class="list-filter filters filter-button-group">                   
                  <li class="active">
                    <div class="single-filter-item" data-filter="*">
                        <p>All</p>                    
                    </div>
                  </li>
                  @if(isset($pverticals) && !empty($pverticals))
                  @foreach($pverticals as $key=>$pvertical)
                    @if($key <= 2 )
                  <li class="">
                    <div class="single-filter-item" data-filter=".vid{{$pvertical->id}}">  
                        <p>{{$pvertical->title}} </p>                   
                    </div>
                  </li>
                  @else
                  @php
                  $asdf1[]=$vertical;
                  @endphp
                  @endif
                  @endforeach
                  @endif
               
                  
                    @if(isset($asdf1) && !empty($asdf1))
                  <li class="dropdown dropdown-filter">
                      <a data-toggle="dropdown" class="dropdown-toggle" href="#">More<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                           @foreach($asdf1 as $as)
                          <li class="">
                            <div class="single-filter-item" data-filter=".vid{{$pvertical->id}}">  
                                <p>{{$as->title}}</p>                     
                            </div>
                          </li>  
                           @endforeach

                        </ul>
                  </li> 
                  @endif
                </ul>
              </div>
            </div>
            @if(isset($news) && !empty($news))
            <div class="row">
               <div class="col-md-12">
                  <div class="block-articls grid">
                    @foreach($news as $new)
                    <div class="grid-item single-arti-block vid{{$new->business_vertical}}">
                      <div class="left-thumb-img">
                        <img src="{{asset($new->image_url)}}" class="img-responsive">
                      </div>  
                      <div class="list-descri arti-desci">
                        <span class="patch patch-re">{{$new->buisinessvertical->title}}</span>

                        <a href="{{$new->news_link}}" target="_blank">
                        <p>
                        {{$new->title}}
                        </p>
                        </a>
                      </div>
                      <div class="arti-date">
                        <span>{{date('d F, Y',strtotime($new->release_date))}}</span>
                      </div>
                    </div>
                
                    @endforeach




                  </div>
               </div>
             </div>
             @endif
          </div>
       </section>
        <!-- brands  end-->

@endsection
 @section('scripts')
   <script type="text/javascript">
        $(document).ready(function() {
          // filter items on button click
            $('.filter-button-group').on( 'click', '.single-filter-item', function() {

              $(".single-arti-block").hide();
             $('.single-filter-item').parent('li').removeClass('active');
             $(this).parent('li').addClass('active');
              var filterValue = $(this).attr('data-filter');             
               $(".single-arti-block").filter(filterValue).show();
          
               TweenMax.fromTo(
                '.single-arti-block img',
                0.2, {            
                    scale:0.8,
                   
                }, {            
                    scale: 1,
                            
                }
            );
              
            });           
            
        });

    </script>
  @endsection
@extends('layouts.app')
@section('title')
{{$leader->meta_title}}
@endsection
@section('keyword')
{{ $leader->meta_keyword}}
@endsection
@section('description')
{{$leader->meta_description}}
@endsection
@section('content')
@section('content')

      <!-- banner section start -->      
      <div id="" class="inner-banner-section leadership-banner banner-overlay" style="background-image: url({{$leader->desktop_banner}}) !important;">
        <div class="left-line-banner"></div>
        <div class="verticle-page-title">About Us</div>
          
        
        <div class="container-fluid nopadding">
          <div class="banner-title">
            <h1 class="hero-title inner-page-title animated-inter-title">Our <br>Leadership</h1>
          </div>            
        </div>  
      </div>
      <!-- banner section end -->

      <!-- small section start-->
      <section id="" class="grey-background section-space position-relative"> 
          <div class="left-line"></div>
          <div class="container small-container">                  
            <div class="row">
              <div class="col-md-12">
                <div class="center-n-line-block">
                  <div class="line-htl-or"> <img src="{{('assets/images/team/h.svg')}}" class="img-responsive"></div>
                   {!! $leader->team_page_description !!}
                  <div class="line-htl-or"> </div>                    
                </div>
                
              </div>    

                       
            </div>
          </div> 
        </section>
        @if(isset($teams) && !empty($teams))
        <section id="" class="grey-background section-space position-relative"> 
           <div class="left-line"></div>
           @foreach($teams as $team)
           @if( $team->team_type=='Advisers')
          <div class="container landing-container team-container">
            <div class="left-pointer"></div> 
            <div class="row">
              <div class="col-md-12">
                  
                   <div class="col-md-4 col-md-push-4"> 
                   <div class="img-team-member animated-text">
                      <img src="{{asset($team->image)}}" class="img-responsive">
                   </div>  
                                   
                  </div>
                  <div class="col-md-4 col-md-pull-4"> 
                    <h3 class="team-name">{{$team->name}}</h3>
                    <p class="team-desi orange-line-bottom">{{$team->designation}}</p>

                    <h4 class="team-compny-type">{{'('}}{{$team->business_vertical}}{{')'}}</h4>

                    <div class="team-descri">
                      {!! $team->details !!}
                    </div>
                   
                  </div>
                   <div class="col-md-4 ">   
                   <div class="designed-block-team ">
                    <div class="line-htl-or grey"> <img src="{{asset('assets/images/team/qoate.svg')}}" class="img-responsive"></div>
                     <h3>
                        {!! $team->bio !!}
                     </h3>
                    <div class="line-htl-or grey"></div>

                   </div>                 
                  </div>
            
            </div>            
          </div>
        </div>
      <div class="pd-30-t-b"></div>
      @endif
       @endforeach
      </section>
      <!-- small section end--> 

<!-- TEAM START -->
     <section id="" class="grey-background section-space position-relative"> 
          <div class="left-line"></div>
          <div class="container landing-container team-container">
            <div class="left-pointer"></div> 

            <div class="row">
              <div class="col-md-12">
                 <h2 class="main-title  mg-bot-title orange-line-bottom">Key Management Team</h2>
              </div>
              <div class="clearfix"></div>
              <div class="col-md-12">


                <div id="team-carousel" class="owl-carousel owl-theme white-circle-nav team-slider team-slider-copy">
                	@foreach($teams as $team)
                	 @if( $team->team_type=='Key Management')
                  <div class="item">
                    <div class="single-top-team d-flex ">
                      <div class="team-top-detail">
                        <h3 class="team-top-name orange-color">{{$team->name}}</h3>
                        <p>{{$team->designation}}</p>
                      </div>
                      <div class="team-top-image">
                        <img src="{{$team->image}}">
                      </div>
                    </div>
                  </div>
                 
                  @endif
                  @endforeach

                </div>
              </div>
            </div>
          </div>
      </section>   
<!-- TEAM END  -->
@endif
@endsection
  @section('scripts')
   <script type="text/javascript">
         
           $(document).ready(function(){
          $('#team-carousel').owlCarousel({
              loop:true,
              margin:10,
              center: false,
              nav: true,  
              smartSpeed:500,           
              dots:false,             
              navText: ["<img src='assets/images/left.png'>", "<img src='assets/images/right.png'>"],

              responsive:{
                  0:{
                      items:1
                  },
                  600:{
                      items:3
                  },
                  1200:{
                      items:3
                  },
                  1600:{
                      items:3
                  }
              }
          });
           });
      </script>

  @endsection
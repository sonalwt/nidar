@extends('layouts.app')
@section('title')
{{$about->meta_title}}
@endsection
@section('keyword')
{{ $about->meta_keyword}}
@endsection
@section('description')
{{$about->meta_description}}
@endsection
@section('content')
@section('content')
<!-- banner section start -->      
      <div id="" class="inner-banner-section banner-about " style="background-image: url({{$about->desktop_banner}}) !important;">
        <div class="left-line-banner"></div>
        <div class="verticle-page-title">About Us</div>
          
        
        <div class="container-fluid nopadding">
          <div class="banner-title">
            <h1 class="hero-title inner-page-title animated-inter-title">About Us</h1>
          </div>            
        </div>  
      </div>
      <!-- banner section end -->

      <!-- Overview section start-->
      <section id="" class="green-background section-space position-relative "> 
          <div class="left-line"></div>
          <div class="landing-container"> 
          <div class="left-pointer"></div>           
            <div class="row">
              <div class="col-md-12">
                  <h2 class="main-title mg-bot-title orange-line-bottom">Overview</h2>
                 {!! $about->overview_part_1 !!}


                 
              </div>    

              <div class="col-md-12">
              	@foreach($verticals as $verticsl)
                <div class="col-md-3">
                  <div class="blk-w-border d-flex flex-justify-spc-bet flex-align-center">
                  <div class="content-group">
                    <h3>{{$verticsl->title}}</h3>
                  </div>
                  <div class="img-grp">
                    <img src="{{asset($verticsl->icon)}}">
                  </div>
                </div>
              </div>
              @endforeach
                
            
              </div>

              <div class="col-md-12">
                 {!! $about->overview_part_2 !!}
              </div>
            </div>
          </div> 
      </section>
      <!-- Overview section end-->



      <section id="" class="grey-background section-space position-relative legacy-background banner-overlay" style="background-image: url({{$about->our_legacy_backgorund_image}}) !important;"> 
          <div class="left-line"></div>
          <div class="landing-container"> 
          <div class="left-pointer"></div>           
            <div class="row">
              <div class="col-md-6">
                  <h2 class="main-title white-color mg-bot-title orange-line-bottom  animated-text">Our Legacy</h2>
                   {!! $about->our_legacy_description !!}
              </div> 
              <div class="clearfix"> 
            </div>
          </div>
      </section>

      <section id="" class="grey-background section-space position-relative"> 
          <div class="left-line"></div>
          <div class="landing-container"> 
          <div class="left-pointer"></div>           
            <div class="row">
              <div class="col-md-12">
                  <h2 class="main-title green-title mg-bot-title orange-line-bottom">Key Strength & differentiators</h2>
              </div>  
            </div>


            <div class="row">
              <div class="col-lg-3 col-sm-12">
                 
                 {!! $about->key_strength_and_differentiators_main_description !!}
              </div> 
              <div class="col-lg-8 values-block col-sm-12">
                <div class="col-lg-4 col-md-6 col-lg-offset-4 nopadding">
                  <div class="single-box-values animated-text">
                    <div>
                      <div class="logo-val">
                          <img src="{{asset($about->key_strength_and_differentiators_image1)}}">
                        </div>
                        <h3 class="green-text">{{$about->key_strength_and_differentiators_title1}}</h3>
                    </div>
                  
                    <div class="descri-val small-content descri-left">
                    {!! $about->key_strength_and_differentiators_description1 !!}
                    </div>
                  </div>
                </div>
                
                  <div class="col-lg-4 col-md-6 nopadding">
                  <div class="single-box-values animated-text">
                   <div>
                    <div class="logo-val">
                      <img src="{{asset($about->key_strength_and_differentiators_image2)}}">
                    </div>
                    <h3 class="green-text">{{$about->key_strength_and_differentiators_title2}}</h3>
                  </div>
                    <div class="descri-val small-content descri-bot">
                      {!! $about->key_strength_and_differentiators_description2 !!}
                    </div>
                  </div>
                </div>
               

                <div class="col-lg-4 col-md-6 nopadding">
                  <div class="single-box-values animated-text">
                    <div>
                    <div class="logo-val">
                      <img src="{{asset($about->key_strength_and_differentiators_image3)}}">
                    </div>
                    <h3 class="green-text">{{$about->key_strength_and_differentiators_title3}}</h3>
                  </div>
                    <div class="descri-val small-content descri-top">
                      {!! $about->key_strength_and_differentiators_description3 !!}
                    </div>
                </div>
                 </div>
                

                <div class="col-lg-4 col-md-6 nopadding">
                  <div class="single-box-values animated-text">                 
                    <div>
                    <div class="logo-val">
                      <img src="{{asset($about->key_strength_and_differentiators_image4)}}">
                    </div>
                    <h3 class="green-text">{{$about->key_strength_and_differentiators_title4}}</h3>
                  </div>
                    <div class="descri-val small-content descri-right">
                      {!! $about->key_strength_and_differentiators_description4 !!}
                    </div>
                  </div>
                  </div>              
              </div>  
            </div>


          </div>
      </section>

@endsection
@extends('layouts.app')
@section('title')
{{$home->meta_title}}
@endsection
@section('keyword')
{{ $home->meta_keyword}}
@endsection
@section('description')
{{$home->meta_description}}
@endsection
@section('content')

  @if(isset($home->banner) && !empty($home->banner))
 
      <!-- banner section start -->
      <div id="banner-section">
         <div class="container-fluid nopadding">
            <div class="banner">  
               <div id="owl-main" class="owl-carousel height-md owl-inner-nav owl-ui-lg">
                @foreach(json_decode($home->banner,true) as $key=>$banner)
                <div class="item owl-one-item">
                  <div class="container-slide slide1">                   
                    <div class="slide-right ">
                      <div class="op-block">                     
                       <!-- <img src="images/home/Home_1.jpg" class="img-responsive desk-img"> -->
                       <!-- <img src="images/home/m_Home_1.jpg" class="img-responsive mob-img"> -->

                       <picture class="desk-img">
                          <source width="1920" height="966" srcset="{{asset($banner['featured_image_webp'])}}" type="image/webp">
                          <source width="1920" height="966" srcset="{{asset($banner['featured_image'])}}" type="image/jpeg"> 
                          <img width="1920" height="966" src="{{asset($banner['featured_image'])}}" alt="banner">                         
                        </picture>

                         <picture class="mob-img">  
                          <source width="768" height="1144" srcset="{{asset($banner['featured_image_mobile_webp'])}}" type="image/webp">
                          <source width="768" height="1144" srcset="{{asset($banner['featured_image_mobile'])}}" type="image/jpeg"> 
                          <img src="{{asset($banner['featured_image_mobile'])}}" width="768" height="1144">
                        </picture>


                     </div>                     
                    </div>                    
                  </div><!-- /.container -->
               </div><!-- /.item -->

                @endforeach

                         
            </div><!-- /.owl-carousel -->
            </div>
         </div>
        
        
        
      </div>
      <!-- banner section end -->
@endif
      <!-- News section start-->
      <section id="home-news" class="grey-background section-space"> 
          <div class="news-container">
            
              <div class="row">
                <div class="col-md-12">
                <div class="d-flex flex-wrap-mob"> 
                  <div class="left-news w-m-100">
                    <h2 class="main-title green-title mg-bot-title pd-left-5"> In the News</h2>

                    @if(!empty($news1))
                  <div class="news-wrapper"> 
                    <div>
                       <span class="patch patch-static patch-grn">News: For {{$home->news1for}}</span>
                    </div>                   
                    <h4 class="sub-head-orange orange-line-bottom">{{$home->news1title}}</h4>
                  
                    <div class="news-block"> <!-- news block -->
                      <h3 class="news-title news-mid">{{$news1->title}}</h3>
                      <span class="news-date">{{date('d,F Y',strtotime($news1->release_date))}}</span>
                      <div class="news-descirption">
                        <p>{{$news1->description}}</p>
                      </div>
                      <div class="know-more-btn">
                         <a href="{{$news1->news_link}}">
                          <span>Read More</span>
                          <div class="arrow-hidden">
                            <img src="{{asset('assets/images/right.png')}}" class="img-responsive">
                          </div>                    
                        </a>
                      </div>
                    </div><!-- news block  end-->
                  </div>
                  @else
                  <div class="news-wrapper"> 
                    <div>
                       <span class="patch patch-static patch-grn">News: For Yotta</span>
                    </div>                   
                    <h4 class="sub-head-orange orange-line-bottom">Noida Bhoomi Pujan</h4>
                  
                    <div class="news-block"> <!-- news block -->
                      <h3 class="news-title news-mid">Hiranandani group to invest Rs 7k crore to set up data centre in Greater Noida</h3>
                      <span class="news-date">28th october, 2020</span>
                      <div class="news-descirption">
                        <p>Yotta Infrastructure, a Hiranandani group firm, on Thursday said it has received approval from the Uttar Pradesh government to set up a 20-acre data centre park in Greater Noida.</p>
                      </div>
                      <div class="know-more-btn">
                         <a href="https://www.financialexpress.com/industry/hiranandani-group-to-invest-rs-7k-crore-to-set-up-data-centre-in-greater-noida/2115029/" target="_blank" class="btn btn-all btn-white scale-btn flex-btn hover-orange">
                          <span>Read More</span>
                          <div class="arrow-hidden">
                            <img src="{{asset('assets/images/right.png')}}" class="img-responsive">
                          </div>                    
                        </a>
                      </div>
                    </div><!-- news block  end-->
                  </div>
                  @endif
                  </div>
                  <div class="right-news w-m-100">
                      <div class="d-flex flex-wrap-mob">
                        @if(!empty($news2))
                          <div class="right-mews-one w-m-100">
                            <div class="news-wrapper">                           
                            <div class="news-with-TopImage">
                              <div class="top-image border-top-orange position-relative">
                                <!-- <img src="images/home/news-top1.png" class="img-responsive"> -->

                                <picture>
                                  <source srcset="{{asset('assets/images/homewebp/news-top1.webp')}}" type="image/webp">
                                  <source srcset="{{asset($home->news2image)}}" type="image/png"> 
                                  <img src="{{asset($home->news2image)}}" alt="banner">
                                </picture> 
                                <span class="patch patch-absolute patch-1">News: For {{$home->news2for}}</span>
                              </div>                             
                              <div class="news-block"> <!-- news block -->
                              <h3 class="news-title news-mid">{{$news2->title}}</h3>
                              <span class="news-date">{{date('d F, Y',strtotime($news2->release_date))}}</span>
                              <div class="news-descirption">
                                <p>{{$news2->description}}</p>
                              </div>
                              <div class="know-more-btn">
                                 <a href="{{$news2->news_link}}" target="_blank" class="btn btn-all btn-white scale-btn flex-btn hover-orange">
                                  <span>Read More</span>
                                  <div class="arrow-hidden">
                                    <img src="{{asset('assets/images/right.png')}}" class="img-responsive">
                                  </div>                    
                                </a>
                              </div>                            
                            </div><!-- news block  end-->

                            </div>
                          </div>                          
                          </div>
                          @else
                          <div class="right-mews-one w-m-100">
                            <div class="news-wrapper">                           
                            <div class="news-with-TopImage">
                              <div class="top-image border-top-orange position-relative">
                                <!-- <img src="images/home/news-top1.png" class="img-responsive"> -->

                                <picture>
                                  <source srcset="{{asset('assets/images/homewebp/news-top1.webp')}}" type="image/webp">
                                  <source srcset="{{asset('assets/images/home/news-top1.png')}}" type="image/png"> 
                                  <img src="{{asset('assets/images/home/news-top1.png')}}" alt="banner">
                                </picture> 
                                <span class="patch patch-absolute patch-1">News: For Hiranandani Communities</span>
                              </div>                             
                              <div class="news-block"> <!-- news block -->
                              <h3 class="news-title news-mid">MultiLiving join hands with Hiranandani Group as their official leasing partner.</h3>
                              <span class="news-date">10th Sep, 2020</span>
                              <div class="news-descirption">
                                <p>India’s first all-inclusive residential-rental ecosystem, MultiLiving is now the official leasing partner of Hiranandani Communities. The collaboration allows creme de la creme homeowners of...</p>
                              </div>
                              <div class="know-more-btn">
                                 <a href=" https://www.hiranandanicommunities.com/latestnews/multiLiving-join-hands-with-hiranandani-group-as-their-official-leasing-partner" target="_blank" class="btn btn-all btn-white scale-btn flex-btn hover-orange">
                                  <span>Read More</span>
                                  <div class="arrow-hidden">
                                    <img src="{{asset('assets/images/right.png')}}" class="img-responsive">
                                  </div>                    
                                </a>
                              </div>                            
                            </div><!-- news block  end-->

                            </div>
                          </div>                          
                          </div>
                          @endif
                          <div class="right-mews-two w-m-100"> 
                            @if(!empty($news3))
                             <div class="news-wrapper">
                              <div class="news-leftImage-small d-flex">
                                <div class="img-news position-relative ">
                                  <span class="patch patch-absolute patch-light-grn t">News: For {{$home->news3for}}</span>
                                  <img src="{{asset($news3->image_url)}}"  class="img-responsive grayscale-img">
                                </div>
                                 <div class="news-block"> <!-- news block -->
                                <h3 class="news-title news-small">{{$news3->title}}</h3>
                                <span class="news-date">{{date('d F, Y',strtotime($news3->release_date))}}</span>
                                <div class="news-descirption">
                                  <p>{{$news3->description}}</p>
                                </div>
                                <div class="know-more-btn">
                                   <a href="{{$news3->news_link}}/" target="_blank" class="btn btn-all btn-white scale-btn flex-btn hover-orange">
                                    <span>Read More</span>
                                    <div class="arrow-hidden">
                                      <img src="{{asset('assets/images/right.png')}}" class="img-responsive">
                                    </div>                    
                                  </a>
                                </div>
                              </div><!-- news block  end-->
                              </div>
                            </div>
                            @else
                            <div class="news-wrapper">
                              <div class="news-leftImage-small d-flex">
                                <div class="img-news position-relative ">
                                  <span class="patch patch-absolute patch-light-grn t">News: For H-Energy</span>
                                  <img src="{{asset('assets/images/home/news-top2.png')}}"  class="img-responsive grayscale-img">
                                </div>
                                 <div class="news-block"> <!-- news block -->
                                <h3 class="news-title news-small">Höegh LNG finalises 10-year FSRU charter with H-Energy </h3>
                                <span class="news-date">2nd February, 2021</span>
                                <div class="news-descirption">
                                  <p>Höegh LNG Holdings Ltd. has announced that it has completed and signed all documentation for its 10-year...</p>
                                </div>
                                <div class="know-more-btn">
                                   <a href="https://www.lngindustry.com/liquid-natural-gas/02022021/hoegh-lng-finalises-10-year-fsru-charter-with-h-energy/" target="_blank" class="btn btn-all btn-white scale-btn flex-btn hover-orange">
                                    <span>Read More</span>
                                    <div class="arrow-hidden">
                                      <img src="{{asset('assets/images/right.png')}}" class="img-responsive">
                                    </div>                    
                                  </a>
                                </div>
                              </div><!-- news block  end-->
                              </div>
                            </div>
                            @endif
                          <div class="line-orange-20"></div>

                           
                             <!-- green-block-news -->
                          <div class="green-block-news animated-text">

                              <div id="news-slider-home" class="owl-carousel owl-theme transparent-nav">
                                @if(!empty($recents))
                                @foreach($recents as $recent)
                                <div class="item">
                                   <h3 class="">{{$recent->title}}  </h3>

                                     <div class="know-more-btn">
                                     <a href="{{$recent->news_link}}" target="_blank" class="btn btn-all btn-white scale-btn flex-btn hover-orange">
                                      <span>Read More</span>
                                      <div class="arrow-hidden">
                                        <img src="{{asset('assets/images/right.png')}}" class="img-responsive">
                                      </div>                    
                                    </a>
                                  </div>
                                </div>
                                @endforeach
                                @else
                                <div class="item">
                                   <h3 class="">Hiranandani inks JV with Blackstone; to invest Rs 2,500 crore for industrial, warehousing space  </h3>

                                     <div class="know-more-btn">
                                     <a href="https://economictimes.indiatimes.com/news/international/business/hiranandani-inks-jv-with-blackstone-to-invest-rs-2500-crore-for-industrial-warehousing-space/articleshow/72958536.cms?utm_source=contentofinterest&utm_medium=text&utm_campaign=cppst" target="_blank" class="btn btn-all btn-white scale-btn flex-btn hover-orange">
                                      <span>Read More</span>
                                      <div class="arrow-hidden">
                                        <img src="{{asset('assets/images/right.png')}}" class="img-responsive">
                                      </div>                    
                                    </a>
                                  </div>
                                </div>

                                 <div class="item">
                                   <h3 class="">Praxis Media in association with Education Connect announced the prestigious National Education  </h3>

                                     <div class="know-more-btn">
                                     <a href="https://www.hiranandanicommunities.com/latestnews/praxis-media-in-association-with-education-connect-announced-the-prestigious-national-education-excellence-awards-on-june-30-2020" target="_blank" class="btn btn-all btn-white scale-btn flex-btn hover-orange">
                                      <span>Read More</span>
                                      <div class="arrow-hidden">
                                        <img src="{{asset('assets/images/right.png')}}" class="img-responsive">
                                      </div>                    
                                    </a>
                                  </div>
                                </div>
                                @endif
                              </div> 
                              <div class="counter-news"></div>
                             
                          </div>                           
                          </div>
                      </div> 
                  </div>
                </div>
              </div>
            </div><!-- ROW END -->

 <!-- News 2 nd row .................-->
            <br>
            <div class="d-flex">
              <div class="l1 line-news"></div>
              <div class="l2 line-news"></div>
              <div class="l3 line-news"></div>
            </div>
            <div class="row">
               <div class="col-md-12 text-center">
                 <div class="know-more-btn">
                         <a href="newsroom.php" class="btn btn-all btn-white scale-btn flex-btn hover-orange">
                          <span>Read All News</span>
                          <div class="arrow-hidden">
                            <img src="{{asset('assets/images/right.png')}}" class="img-responsive">
                          </div>                    
                        </a>
                      </div> 
                </div> 
            </div>
            
          </div>
          
      </section>
      <!-- News section end-->

<div class="clearfix"></div>
      <!-- Language start -->
      <section id="brand-box" class="position-relative overflow-x-hidden white-light-background">            
        <!-- <div class="square-shape-left"> </div>  -->
         <div class="show-nav-brand"></div>

        <div class="po-ab-brand-title">
           <h2 class="main-title white-color mg-bot-title"> Our Brands</h2>
        </div>
         <div class="container-fluid nopadding">
          <div class="row">
          <div class="col-md-12"> 
           
                <!-- <h2 class="main-title green-title mg-bot-title"> Our Brands</h2> -->
               
                                         
            <div class="outer">

                  <div id="big" class="owl-carousel owl-theme">
                    @foreach($brands as $brand)
                    <div class="item">
                      <div class="big-img-brand-home">
                        <!-- <img src="images/home/brands/HiranandaniParks.jpg" class="desk-img">
                        <img src="images/home/brands/m_HiranandaniParks.jpg" class="mob-img"> -->

                        <picture>
                          <source class="desk-img" srcset="{{asset($brand->home_page_image_webp)}}" width="1920" height="1080" type="image/webp">
                          <source class="desk-img" srcset="{{asset($brand->home_page_image)}}" width="1920" height="1080" type="image/jpeg"> 
                          <img class="img-responsive desk-img" src="{{asset($brand->home_page_image)}}" width="1920" height="1080" alt="banner">

                          <source class="img-responsive mob-img" srcset="{{asset($brand->home_page_image_webp_mobile)}}" width="500" height="500" type="image/webp">
                          <source class="img-responsive mob-img" srcset="{{asset($brand->home_page_image_mobile)}}"  width="500" height="500" type="image/jpeg"> 
                          <img src="{{asset($brand->home_page_image_mobile)}}"  width="500" height="500" class="mob-img">
                        </picture> 

                      </div>
                      <div class="caption-brand caption1">
                         <span class="patch patch-re">{{$brand->vertical->title}}</span>
                        <h3 class="hero-title">{{$brand->title}}</h3>
                       
                        <div class="know-more-btn">
                            <a href="javascript:void(0);" class="btn btn-all btn-white scale-btn flex-btn hover-orange">
                              <span>View in detail</span>
                                <div class="arrow-hidden">
                                  <img src="{{asset('assets/images/right.png')}}" class="img-responsive">
                                </div>                    
                              </a>
                        </div>
                      </div>
                    </div>
                    @endforeach
                    

                  </div>
                  <div id="thumbs" class="owl-carousel owl-theme orange-nav">    
                   @foreach($brands as $brand)               
                    <div class="item">
                      <img src="{{asset($brand->brand_logo)}}"> 
                                       
                    </div>
                    @endforeach
                                    
                  </div>
                  </div>
            
            



             
          </div>
          </div>
         </div>
      </section>
      <!-- language end -->
     
     <section id="career-section" class="section-space">
        <div class="show-nav-career"></div>
       <div class="container landing-container">
          <div class="row">
           <div class="col-md-11 d-flex flex-wrap-mob col-rev-mob">
             <div class="col-md-8">
               <a href="career.php" class="oppr-block d-flex flex-wrap-mob col-rev-mob mg-bot-m-10">
                 <div class="opp-head career-head flex-column w-m-100">
                   <h4 class="title-oppo-block">Equal <br>opportunity <br>employer</h4>
                   {!! $home->equal_opportunity_employer_description!!}
                 </div>
                 <div class="opp-image w-m-100">
                    <!-- <img src="images/home/oppo.png" class="img-responsive"> -->

                    <picture>
                          <source srcset="{{asset('assets/images/homewebp/oppo.webp')}}" type="image/webp">
                          <source srcset="{{asset($home->equal_opportunity_employer_image)}}" type="image/jpeg"> 
                          <img src="{{asset($home->equal_opportunity_employer_image)}}" alt="banner">
                    </picture> 
                 </div>
               </a>

               <div class="opening-block d-flex flex-wrap-mob ">
                <div class="opening-image w-m-100">
                   <!-- <img src="images/home/opening.png" class="img-responsive"> -->
                   <picture>
                          <source srcset="{{asset('assets/images/homewebp/opening.webp')}}" type="image/webp">
                          <source srcset="{{asset($home->current_openings_image)}}" type="image/jpeg"> 
                          <img src="{{asset($home->current_openings_image)}}" alt="banner">
                    </picture> 
                 </div>
                 <a href="{{url('career')}}" class="opening-head w-m-100">
                   <h4 class="title-career-block">Current Openings</h4>
                   <div class="arrow-block">
                     <img src="{{asset('assets/images/right.png')}}">
                   </div>
                 </a>
                 
               </div>

             </div>
             <div class="col-md-4 mg-bot-m-10">              
               <div class="wfh-block d-flex flex-column">
                  <div class="title-block-career ">
                    <h2 class="main-title green-title mg-bot-title">Careers</h2>                    
                    {!! $home->careers_descriptione !!}
                  </div>
                  <div class="wfh-block-bot">
                    <div class="wfh-image">
                   <!-- <img src="images/home/wfh.png" class="img-responsive"> -->
                    <picture>
                          <source srcset="{{asset('assets/images/homewebp/wfh.webp')}}" type="image/webp">
                          <source srcset="{{asset($home->work_culture_image)}}" type="image/jpeg"> 
                          <img src="{{asset($home->work_culture_image)}}" alt="banner">
                    </picture> 
                     </div>
                     <a href="career.php" class="wfh-head career-head flex-column">
                       <h4 class="title-wfh-block">Work Culture at Nidar Group</h4>
                       {!! $home->work_culture_description !!}
                     </a>
                  </div>
                  
                 
               </div>
             </div>
           </div>
         </div>
       </div>      
     </section>
      @endsection
      @section('scripts')
      <script type="text/javascript">
         $(document).ready(function(){
                
           // header hide and show js
              var lastScrollTop = 0;
              var controller = new ScrollMagic.Controller();

              var rio = TweenMax.to(".home .navbar", 0.2, {className: "navbar navbar-fixed-top trans-header",ease: Linear.easeNone});

              var rioInverse = TweenMax.to(".home .navbar", 0.5, {className: "navbar navbar-fixed-top",ease: Linear.easeNone});


              var scene = new ScrollMagic.Scene({triggerElement:".show-nav-brand", offset: 0, triggerHook: 0.15})
                .setTween(rio) 
                .addTo(controller);      

              var scene = new ScrollMagic.Scene({triggerElement:"#career-section", offset: 0, triggerHook: 0.5})
                          .setTween(rioInverse) 
                          .addTo(controller);
                       });

                $(window).scroll(function() {
                  if ($(document).scrollTop() > 100) {
                  $(".home .navbar").css('opacity','0');

                  clearTimeout($.data(this, 'scrollTimer'));
                    $.data(this, 'scrollTimer', setTimeout(function() {           
                      TweenMax.fromTo(
                          '.home .navbar',
                          0.1, {
                              opacity: 0,
                              y:-20,
                          }, {
                              opacity: 1,
                              y: 0,
                              ease: Expo.easeOut,
                              transformOrigin:"-50% 0%",
                              delay: 0
                          }
                      );              
                    }, 1500));
                  }
                  else{
                  $(".home .navbar").css('opacity','1');
                  }     
                });
                // header hide and show js end

      </script>
      @endsection
@extends('layouts.app')
@section('title')
{{$contact->meta_title}}
@endsection
@section('keyword')
{{ $contact->meta_keyword}}
@endsection
@section('description')
{{$contact->meta_description}}
@endsection
@section('content')
@section('content')
    <!-- banner section start -->
    <div id="" class="inner-banner-section contact-banner banner-overlay" style="background-image: url({{$contact->desktop_banner}}) !important;">

        <div class="container-fluid nopadding">
            <div class="banner-title">
                <h1 class="hero-title inner-page-title animated-inter-title">CONTACT US</h1>
            </div>
        </div>
    </div>
    <!-- banner section end -->

    <!-- Outline section start-->
    <section id="location-data" class=" position-relative">
        <div class="container-fluid">
             <div class="row">
                <div class="col-md-12 nopadding"> 
                    <div class="con-wrapper">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#collapseOne" data-toggle="tab">India</a></li>
                            <li><a href="#collapseTwo" data-toggle="tab">UAE</a></li>
                        </ul>                
                     </div>

                    <div class="tab-content">
                    <div id="collapseOne" class="tab-pane fade in active">
                        <div class="col-md-12 col-sm-12 col-xs-12 nopadding d-flex flex-wrap-mob">
                            <div class="col-md-6 col-sm-6 col-xs-12 nopadding">
                                <div class="img">
                                    <img src="{{asset($contact->india_image)}}">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="single-about d-sm-flex mt-30 wow fadeInUp">
                                    <div class="about-icon">
                                        <img src="{{asset('assets/images/contact-us/office-block.png')}}" alt="Icon">
                                       {!! $contact->india_address !!}
                                    </div>
                                    <div class="about-icon">
                                       <img src="{{asset('assets/images/contact-us/call-i.png')}}" alt="Icon">
                                        <p class="text">{{$contact->india_mobile}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="collapseTwo" class="tab-pane fade">
                        <div class="col-md-12 col-xs-12 nopadding d-flex flex-wrap-mob">
                            <div class="col-md-6 col-sm-6 col-xs-12 nopadding">
                                <div class="img">
                                 <img src="{{asset($contact->uae_image)}}">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="single-about d-sm-flex mt-30 wow fadeInUp">
                                <div class="about-icon">
                                    <img src="{{asset('assets/images/contact-us/office-block.png')}}" alt="Icon">
                                   {!! $contact->uae_address !!}
                                </div>
                                <div class="about-icon">
                                  <img src="{{asset('assets/images/contact-us/call-i.png')}}" alt="Icon">
                                    <p class="text">{{$contact->uae_mobile}}</p>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>


                    </div>

                </div>
             </div>
        </div>
    </section>
    <!-- Outline end-->

    <div class="form-map">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-sm-12 flex-map-section">             
                <div class="col-md-6 col-sm-6 get-form form-block-partner">
                    <h2 class="main-title green-title mg-bot-title orange-line-bottom">Get In Touch</h2>
                    <form action="javascript:void(0);" id="contact_form">
                        <div class="form-group">
                            <div class="controls">
                                <input type="text" id="contact_first_name" class="floatLabel form-control" name="contact_first_name" required>
                                <label for="name">First Name*</label>
                            </div>
                            <div class="controls">
                                <input type="text" id="contact_last_name" class="floatLabel form-control" name="contact_last_name" required>
                                <label for="name">Last Name*</label>
                            </div>
                            <div class="controls">
                                <input type="email" id="contact_email" class="floatLabel form-control" name="contact_email" required>
                                <label for="email">E-mail ID*</label>
                            </div>
                            <div class="controls">
                                <input type="tel" id="contact_phone" class="floatLabel form-control" name="contact_phone" required>
                                <label for="email">Phone Number*</label>
                            </div>
                            <div class="controls">
                                <textarea type="textarea" id="contact_msg" class="floatLabel form-control" name="contact_msg" rows="4" cols="50"></textarea>
                                <label for="email">Message</label>
                            </div>

                            <div class="clearfix"> </div>


                            <div class="col-md-12 text-center">
                                <input type="submit" name="contactsubmit" id="contactsubmit" class="btn btn-all submit-btn" value="Submit"><br>
                                <span id="success" class="success"></span>
                            <span id="danger" class="danger"></span>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="col-md-6 col-sm-6 map">
                    <div class="map-block">
                       <iframe src="{{$contact->map}}" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @section('scripts')

<script type="text/javascript">
        $(document).ready(function() {
            function floatLabel(inputType) {
                $(inputType).each(function() {
                    var $this = $(this);
                    // on focus add cladd active to label
                    $this.focus(function() {
                        $this.next().addClass("active");
                    });
                    //on blur check field and remove class if needed
                    $this.blur(function() {
                        if ($this.val() === '' || $this.val() === 'blank') {
                            $this.next().removeClass();
                        }
                    });
                });
            }
            // just add a class of "floatLabel to the input field!"
            floatLabel(".floatLabel");

          $("#contact_form").submit(function(e) {

            var contact_first_name = $('#contact_first_name').val();
            var contact_last_name= $('#contact_last_name').val();
           var contact_email= $('#contact_email').val();
           var contact_phone= $('#contact_phone').val();
           var contact_msg= $('#contact_msg').val();
        
                var flag = 0;

                if (contact_first_name == null || contact_first_name == '') {
                    $('#contact_first_name').addClass('has-error');
                    flag++;
                } else {
                    $("#contact_first_name").removeClass('has-error');
                }
                if (contact_last_name == null || contact_last_name == '') {
                    $('#contact_last_name').addClass('has-error');
                    flag++;
                } else {
                    $("#contact_last_name").removeClass('has-error');
                }
                if (contact_email == '' || contact_email == '') {
                    $("#contact_email").addClass('has-error');

                    flag++;
                } else {
                    $("#contact_email").addClass('has-error');
                }
                if (contact_phone == null || contact_phone == '') {
                    $("#contact_phone").addClass('has-error');
                    flag++;
                } else {
                    $("#contact_phone").removeClass('error');

                }


                // if(mobile.length<10||mobile.length>10) {
                //     $("#mobile").addClass('error');
                //     $('#mobile_error').html('mobile is not valid');
                //     flag++;
                // }
                // else{
                //     $("#mobile").removeClass('error');
                //     $('#mobile_error').html('');
                //
                // }


                if (flag > 0) {
                    e.preventDefault();
                    return false;
                } else {
                    $("#contactsubmit").val('Please Wait...');
                    var form_data = new FormData();
                    form_data.append('contact_first_name', contact_first_name);
                    form_data.append('contact_last_name', contact_last_name);
                    form_data.append('contact_email', contact_email);
                    form_data.append('contact_phone', contact_phone);
                    form_data.append('contact_msg', contact_msg);
                    $.ajax({
                        type: "POST",
                        url: 'ajax_submit_contact_page_form',
                        dataType: 'text', // what to expect back from the PHP script, if anything
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        success: function(data) {
                            if (data == 1) {
                                $("#contactsubmit").val('Submit');
                                $('#contact_form').trigger('reset');
                                $('#success').html('Thank you for Enquiry!');
                                 $('#danger').html('');
                                 setTimeout(function () { location.reload(1); }, 3000);
                            } else {
                                $("#contactsubmit").val('Submit');
                                $('#danger').html('Something went wrong');
                            }
                        }
                    });
                }

            });
        });

    </script>
@endsection
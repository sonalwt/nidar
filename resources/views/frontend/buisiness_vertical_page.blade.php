 @extends('layouts.app')
 @section('title')
{{$bv->meta_title}}
@endsection
@section('keyword')
{{ $bv->meta_keyword}}
@endsection
@section('description')
{{$bv->meta_description}}
@endsection
@section('content')
@section('content')
 <!-- banner section start -->      
      <div id="" class="inner-banner-section banner-bv" style="background-image: url({{$bv->desktop_banner}}) !important;">
         <div class="left-line-banner"></div>
         <div class="verticle-page-title">About Us</div>
         <div class="container-fluid nopadding">
            <div class="banner-title">
               <h1 class="hero-title inner-page-title animated-inter-title">Business <br> Verticals</h1>
            </div>
         </div>
      </div>
      <!-- banner section end -->
      <!-- Overview section start-->
      <section id="" class="green-background section-space position-relative ">
         <div class="landing-container">
            <div class="row">
               <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                  {!! $bv->description !!}
               </div>
            </div>
         </div>
      </section>
      @if(isset($verticals) && !empty($verticals))
      <section id="" class=" position-relative ">
         <!-- <div class="left-line"></div> -->
         <div class="container">
            <!-- <div class="left-pointer"></div>            -->
            <div class="row">
               <div class="col-md-12 nopadding">
                  <ul class="nav nav-pills sbu-tabs d-flex">
                     @foreach($verticals as $key=>$vertical)
                     <li class="@if($key==0) {{'active'}} @endif">
                        <a data-toggle="pill" href="#vertical{{$vertical->id}}" class="scrollTo">
                           <div class="d-flex sbu-item flex-column">
                              <div class="img-sbu">
                                 <img src="{{asset( $vertical->image)}}" class="img-responsive">
                              </div>
                              <div class="sbu-name">
                                 <h4>{{$vertical->title}} </h4>
                              </div>
                              <!--  <span class="btn btn-all btn-white">Know more</span> -->
                           </div>
                        </a>
                     </li>
                     @endforeach
                     
                  </ul>
               </div>
            </div>
         </div>
      </section>
      @endif
      <!-- Overview section end-->
      <section id="tab-content-bv" class="position-relative bg-gray">
         <div class="tab-content">
            @foreach($verticals as $key=>$vertical)
            <div id="vertical{{$vertical->id}}" class="section-space-top tab-pane fade in @if($key==0) {{'active'}} @endif">
               <div class="landing-container">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="col-md-6">
                           <!-- <h1 class="hero-title mg-bot-title orange-color sbu-title">Real Estate</h1> -->
                           <div class="sbu-bold-content">
                              {!! $vertical->description !!}

                           </div>
                           @if(isset($vertical->description_industry_sub_sector) && !empty($vertical->description_industry_sub_sector))
                           <div class="sub-div-block">
                              <!-- <div class="down-arrow-orange po-abso">
                                 <img src="images/right-arrow.png">
                                 </div> -->
                              <h2 class="main-title orange-line-bottom green-title">Industry Sub-sectors </h2>
                              <div class="sbu-bold-content">
                                  {!! $vertical->description_industry_sub_sector !!}
                              </div>
                           </div>
                           @else
                            <div class="sub-div-block">
                              <!-- <div class="down-arrow-orange po-abso">
                                 <img src="images/right-arrow.png">
                                 </div> -->
                              <h2 class="main-title orange-line-bottom green-title">Vertical Fast Facts </h2>
                              <div class="sbu-bold-content">
                                  {!! $vertical->fast_facts_description !!}
                              </div>
                           </div>
                           @endif
                        </div>
                        <div class="col-md-6">
                           <div> 
                              <img src="{{asset($vertical->image2)}}">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- <div class="orange-brod-line"></div> -->
               @if(isset($vertical->subsectors) && !empty($vertical->subsectors))
               <div class="landing-container">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="sub-divisions-points-block d-flex flex-wrap-mob">
                           @php
                           $subsectors=json_decode($vertical->subsectors,true);
                           @endphp
                           @foreach($subsectors as $subsector)
                           <div class="point1 w-m-100">
                              <div class="points-top">@if(isset($subsector['featured_image']))<img src="{{asset($subsector['featured_image'])}}">@endif{{$subsector['title']}}</div>
                              @php
                              $sectors=explode(",",$subsector['sectors']);
                              @endphp
                              @if(!empty($sectors))
                              <div class="points-list">
                                 <ul>
                                    @foreach( $sectors as  $sector)
                                    <li>{{$sector}}</li>
                                   @endforeach
                                 </ul>
                              </div>
                                @endif
                           </div>
                         
                           @endforeach
                          
                        </div>
                     </div>
                  </div>
               </div>
               @endif
                  @if(isset($vertical->fast_facts) && !empty($vertical->fast_facts))
               <div class="landing-container sbu-number-blocks">
                  <div class="row section-space-bottom">
                     <div class="col-md-12">
                        <h2 class="main-title green-title mg-bot-title orange-line-bottom">Vertical Fast Facts</h2>
                     </div>
                     <div class="col-md-12 col-xs-12 nopadding">
                        @php
                           $fast_facts=json_decode($vertical->fast_facts,true);
                           @endphp
                           @foreach($fast_facts as $fast_fact)
                        <div class="col-md-4 col-xs-6">
                           <div class="single-number-block">
                              <div class="img-number">
                                 @if(isset($fast_fact['featured_image']))<img src="{{asset($fast_fact['featured_image'])}}">@endif
                              </div>
                              <div class="num-block">
                                 <h3 class="big-num counter green-text">@if(!empty($fast_fact['count'])){!! $fast_fact['count'] !!}@endif</h3>
                                 @if(!empty($fast_fact['unit']))<span class="num-unit">{!! $fast_fact['unit'] !!}</span>@endif
                              </div>
                              @if(!empty($fast_fact['title'])) <p class="num-descri"> {{$fast_fact['title']}} </p>@endif
                           </div>
                        </div>
                        @endforeach
                     </div>
                  </div>
                   <div class="note-line-ab">
                       As per annual report 2019-20
                     </div>
               </div>
               @endif
               <!--bg-darkgray-starts-->
               <!-- <div class="bg-darkgray section-ptb-60">
                  <div class="space-small">
                     <div class="landing-container sbu-number-blocks">
                        <div class="row">
                           <div class="col-md-12">
                              <h2 class="main-title white-title mg-bot-title orange-line-bottom">Real Estate Brand</h2>
                           </div>
                           <div class="col-md-12">
                              <div class="d-flex flex-wrap block-logos b-or-block b-5-block grid">
                                 <div class="grid-item single-logo-block">
                                    <div class="brand-logo">
                                       <img src="images/brands/logo/8.png" class="img-responsive">
                                    </div>
                                 </div>
                                 <div class="grid-item single-logo-block">
                                    <div class="brand-logo">
                                       <img src="images/brands/logo/hiranandani_group.png" class="img-responsive">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="space-small">
                     <div class="landing-container sbu-number-blocks">
                        <div class="row">
                           <div class="col-md-12">
                              <h2 class="main-title white-title mg-bot-title orange-line-bottom">Township Brand </h2>
                           </div>
                           <div class="col-md-12">
                              <div class="d-flex flex-wrap block-logos b-or-block b-5-block grid">
                                 <div class="grid-item single-logo-block">
                                    <div class="brand-logo">
                                       <img src="images/brands/logo/real-estate/6.png" class="img-responsive">
                                    </div>
                                 </div>
                                 <div class="grid-item single-logo-block">
                                    <div class="brand-logo">
                                       <img src="images/brands/logo/5.png" class="img-responsive">
                                    </div>
                                 </div>
                                 <div class="grid-item single-logo-block">
                                    <div class="brand-logo">
                                       <img src="images/brands/logo/real-estate/2.png" class="img-responsive">
                                    </div>
                                 </div>
                                 <div class="grid-item single-logo-block">
                                    <div class="brand-logo">
                                       <img src="images/brands/logo/real-estate/3.png" class="img-responsive">
                                    </div>
                                 </div>
                                 <div class="grid-item single-logo-block custom-logo-text">
                                    <div class="brand-logo">
                                       <img src="images/brands/logo/hiranandani_group.png" class="img-responsive">
                                       <p class="">Hiranandani<br>Gardens</p>
                                    </div>
                                 </div>
                                 <div class="grid-item single-logo-block custom-logo-text">
                                    <div class="brand-logo">
                                       <img src="images/brands/logo/hiranandani_group.png" class="img-responsive">
                                       <p class="">Hiranandani<br>Estate</p>
                                    </div>
                                 </div>
                                 <div class="grid-item single-logo-block custom-logo-text">
                                    <div class="brand-logo">
                                       <img src="images/brands/logo/hiranandani_group.png" class="img-responsive">
                                       <p class="">Hiranandani<br>Meadows</p>
                                    </div>
                                 </div>
                                 <div class="grid-item single-logo-block custom-logo-text">
                                    <div class="brand-logo">
                                       <img src="images/brands/logo/hiranandani_group.png" class="img-responsive">
                                       <p class="">One<br>Hiranandani<br>Park</p>
                                    </div>
                                 </div>
                                 <div class="grid-item single-logo-block">
                                    <div class="brand-logo">
                                       <img src="images/brands/logo/real-estate/4.png" class="img-responsive">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                
                  <div class="space-small">
                     <div class="landing-container sbu-number-blocks">
                        <div class="row">
                           <div class="col-md-12">
                              <h2 class="main-title white-title mg-bot-title orange-line-bottom">Healthcare Brand</h2>
                           </div>
                           <div class="col-md-12">
                              <div class="d-flex flex-wrap block-logos b-or-block b-5-block grid">
                                 <div class="grid-item single-logo-block">
                                    <div class="brand-logo">
                                       <img src="images/brands/logo/5.png" class="img-responsive">
                                       <a href="javascript:void(0);" class="btn btn-all btn-orange btn-contact sub-brand-btn">Pow­­ai</a>
                                    </div>
                                 </div>
                                 <div class="grid-item single-logo-block">
                                    <div class="brand-logo">
                                       <img src="images/brands/logo/5.png" class="img-responsive">
                                       <a href="javascript:void(0);" class="btn btn-all btn-orange btn-contact sub-brand-btn">Thane</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  
                  <div class="space-small">
                     <div class="landing-container sbu-number-blocks">
                        <div class="row">
                           <div class="col-md-12">
                              <h2 class="main-title white-title mg-bot-title orange-line-bottom">Education Brand</h2>
                           </div>
                           <div class="col-md-12">
                              <div class="d-flex flex-wrap block-logos b-or-block b-5-block grid">
                                 <div class="grid-item single-logo-block">
                                    <div class="brand-logo">
                                       <img src="images/brands/logo/education/9.png" class="img-responsive">
                                       <a href="javascript:void(0);" class="btn btn-all btn-orange btn-contact sub-brand-btn">Pow­­ai</a>
                                    </div>
                                 </div>
                                 <div class="grid-item single-logo-block">
                                    <div class="brand-logo">
                                       <img src="images/brands/logo/education/9.png" class="img-responsive">
                                       <a href="javascript:void(0);" class="btn btn-all btn-orange btn-contact sub-brand-btn">Thane</a>
                                    </div>
                                 </div>
                                 <div class="grid-item single-logo-block">
                                    <div class="brand-logo">
                                       <img src="images/brands/logo/education/8.png" class="img-responsive">
                                    </div>
                                 </div>                                
                                 <div class="grid-item single-logo-block">
                                    <div class="brand-logo">
                                       <img src="images/brands/logo/education/2.png" class="img-responsive">
                                    </div>
                                 </div>
                                 <div class="grid-item single-logo-block">
                                    <div class="brand-logo">
                                       <img src="images/brands/logo/education/3.png" class="img-responsive">
                                    </div>
                                 </div>
                                 <div class="grid-item single-logo-block">
                                    <div class="brand-logo">
                                       <img src="images/brands/logo/education/4.png" class="img-responsive">
                                    </div>
                                 </div>
                                 <div class="grid-item single-logo-block custom-logo-text">
                                    <div class="brand-logo">
                                     
                                       <p class="">Hiradale</p>
                                    </div>
                                 </div>
                                 <div class="grid-item single-logo-block custom-logo-text">
                                    <div class="brand-logo">
                                    
                                       <p class="">Beaumount</p>
                                    </div>
                                 </div>
                                 <div class="grid-item single-logo-block">
                                    <div class="brand-logo">
                                       <img src="images/brands/logo/education/10.png" class="img-responsive">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="space-small">
                     <div class="landing-container sbu-number-blocks">
                        <div class="row">
                           <div class="col-md-12">
                              <h2 class="main-title white-title mg-bot-title orange-line-bottom">Hospitality Brand</h2>
                           </div>
                           <div class="col-md-12">
                              <div class="d-flex flex-wrap block-logos b-or-block b-5-block grid">
                                 <div class="grid-item single-logo-block">
                                    <div class="brand-logo">
                                       <img src="images/brands/logo/hospitality/1.png" class="img-responsive">
                                    </div>
                                 </div>
                                 <div class="grid-item single-logo-block">
                                    <div class="brand-logo">
                                       <img src="images/brands/logo/hospitality/2.png" class="img-responsive">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  
               </div> -->
               <!--bg-darkgray-ends-->
            </div>
            @endforeach
            
         </div>
      </section>
      @endsection
       @section('scripts')
       @endsection
@extends('layouts.app')
@section('title')
{{$bo->meta_title}}
@endsection
@section('keyword')
{{ $bo->meta_keyword}}
@endsection
@section('description')
{{$bo->meta_description}}
@endsection
@section('content')
@section('content')
<!-- banner section start -->
        <div id="" class="inner-banner-section overview-banner" style="background-image: url({{$bo->desktop_banner}}) !important;">
            <div class="left-line-banner"></div>
            <div class="verticle-page-title">About Us</div>


            <div class="container-fluid nopadding">
                <div class="banner-title">
                    <h1 class="hero-title inner-page-title animated-inter-title">Business <br> overview</h1>
                </div>
            </div>
        </div>
        <!-- banner section end -->

        <!-- Overview section start-->
        <section id="" class="grey-background section-space position-relative">
            <div class="left-line"></div>
            <!-- <div class="light-orange-patch"></div> -->
            <div class="landing-container">
                <div class="left-pointer"></div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <h2 class="main-title green-title mg-bot-title orange-line-bottom">Overview</h2>
                        @if(isset($bo->overview_part) && !empty($bo->overview_part))
                        {!!  $bo->overview_part !!}
                        @else
                        <p class="green-text animated-text">As a group, we are inspired by the aspirations of our country. A country that is home to over a billion people, with dreams galore. Powered by innovation and strengthened by insight, all our projects endeavor to offer every citizen an enhanced life. The Nidar Group is committed to positively impact society, not just today, but for years to come</p>

                         <p class="green-text animated-text">Our real estate offerings are designed to match the lifestyle of aspiring Indians, while our energy and infrastructure projects are setting new highs for trade, commerce, logistics and urbanization. </p>

                          <p class="green-text animated-text">As we're poised to take a new leap, we are aligning ourselves with projects that are not only in sync with customer priorities, but also focus on present and future exigencies.  </p>
                          @endif
                    </div>

                    <!-- <div class="col-md-12">
                        <div class="col-md-offset-2 col-md-5">
                            <div class="overview-content-small green-text animated-text">
                                <p>Our real estate offerings continue to transform the lifestyle of the 21st century aspiring Indian, and our energy and infrastructure ventures are revolutionising trade and commerce, logistics, and urbanisation.</p>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="overview-content-small green-text animated-text">
                                <p>
                                Staying closer to our customers, we are aligning ourselves with their evolving priorities. As we accelerate towards the future, we stay focussed on the horizon of opportunity. We are finding newer ways to create value, entering into spaces that have great potential, and launching projects that address the current and future imperatives.
                                </p>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </section>
        <!-- Overview section end-->

@if(isset($numbers) && !empty($numbers))
        <section class="section-space green-background number-section position-relative">
            <div class="left-line"></div>
            <div class="landing-container">
                <div class="left-pointer"></div>
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="main-title mg-bot-title white-color orange-line-bottom">Group in Numbers</h2>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-xs-12 nopadding">
                    	@foreach($numbers as $number)
                        <div class="col-md-3 col-xs-6">
                            <div class="single-number-block">
                                <div class="img-number">
                                    <img src="{{asset($number->icon)}}">
                                </div>
                                <div class="num-block">
                                    <h3 class="big-num counter">{{$number->count}}</h3>
                                    <span class="num-unit">{{$number->unit}}</span>
                                </div>
                                <p class="num-descri">{{$number->title}}</p>

                            </div>
                        </div>
                        @endforeach

                    </div>

                  

                </div>

            </div>
            <div class="note-line-ab">
              @if(isset($bo) && !empty($bo))
              {{$bo->Group_in_numbers_remark}}
              @endif
            </div>
        </section>
@endif
@if(isset($milestones) && !empty($milestones))
        <section id="" class="grey-background section-space logo-back position-relative">
            <div class="left-line"></div>
            <div class="landing-container">
                <div class="left-pointer"></div>
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="main-title mg-bot-title green-title orange-line-bottom">Milestones</h2>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <div class="row milestone-row">
                    <div class="col-md-12">
                        <div class="listing_year col-md-2 nopadding">
                            <div class="milestone nav nav-pills" id="milestone">
                            	@foreach($milestones as $key=>$milestone)
                                <div class="item-yr"><a href="#{{$milestone->year}}" data-toggle="tab" class="@if($key==0) {{'active'}} @endif">{{$milestone->year}}</a></div>
                                @endforeach
                                <!-- <div class="item-yr"><a href="#1987" data-toggle="tab">1987</a></div>
                                <div class="item-yr"><a href="#1990" data-toggle="tab">1990</a></div>
                                <div class="item-yr"><a href="#1996" data-toggle="tab">1996</a></div>
                                <div class="item-yr"><a href="#1999" data-toggle="tab">1999</a></div>
                                <div class="item-yr"><a href="#2003" data-toggle="tab">2003</a></div>
                                <div class="item-yr"><a href="#2004" data-toggle="tab">2004</a></div>
                                <div class="item-yr"><a href="#2008" data-toggle="tab">2008</a></div>
                                <div class="item-yr"><a href="#2012" data-toggle="tab">2012</a></div>
                                <div class="item-yr"><a href="#2014" data-toggle="tab">2014</a></div>
                                <div class="item-yr"><a href="#2015" data-toggle="tab">2015</a></div>
                                <div class="item-yr"><a href="#2017" data-toggle="tab">2017</a></div>
                                <div class="item-yr"><a href="#2018" data-toggle="tab">2018</a></div>
                                <div class="item-yr"><a href="#2019" data-toggle="tab">2019</a></div>
                                <div class="item-yr"><a href="#2020" data-toggle="tab">2020</a></div>
                                <div class="item-yr"><a href="#2021" data-toggle="tab">2021</a></div> -->
                            </div>

                        </div>


                        <!-- newly added -->

                        <div class=" col-md-10">
                            <div class="journey_detail_new">
                                <div class="tab-content clearfix">
                                	@foreach($milestones as $key=>$milestone)
                                    <div class="tab-pane @if($key==0) {{'active'}} @endif" id="{{$milestone->year}}">
                                        <div class="journey_content">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-2 nopadding"><span class="year-value">{{$milestone->year}}</span></div>
                                                    <div class="col-md-4">
                                                        <!-- Pills navs -->
                                                        @php
                                                        	$details=json_decode($milestone->title,true);
                                                        	$i=1;
                                                        	@endphp
                                                        <ul class="nav nav-pills">
                                                        	@foreach($details as $key=>$detail)
                                                            <li class="@if($i==1) {{'active'}} @endif">
                                                                <a data-toggle="pill" href="#{{($milestone->year)}}{{$key}}">
                                                                {!! $detail['title'] !!}
                                                            </a>
                                                            </li>
                                                            @php
                                                            $i++;
                                                            @endphp
                                                            @endforeach
                                                            <!--
                                                            <li>
                                                                <a data-toggle="pill" href="#industrial_shed">Completed construction of the first industrial shed at Oragadam, Chennai for Vestas, by Greenbase</a>
                                                            </li>
-->
                                                        </ul>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="tab-content">
                                                        	 @php
                                                        	$details=json_decode($milestone->title,true);
                                                        	$i=1;
                                                        	
                                                        	@endphp

                                                        	@foreach($details as $key=>$detail)
                                                            <div id="{{($milestone->year)}}{{$key}}" class="tab-pane fade  @if($i==1) {{'in active'}} @endif">
                                                                <div class="milestone-img">
                                                                    <img src="{{asset($detail['featured_image'])}}">
                                                                </div>
                                                            </div>
                                                             @php
                                                            $i++;
                                                            @endphp
                                                            @endforeach
                                                            <!--
                                                            <div id="industrial_shed" class="tab-pane fade">
                                                                <div class="milestone-img">
                                                                    <img src="images/milestone/81-86.png">
                                                                </div>
                                                            </div>
-->

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach

                                  
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        @endif
@endsection
  @section('scripts')
 <script type="text/javascript">
                    $(document).ready(function() {
                        $('.counter').counterUp({
                            delay: 10,
                            time: 1000
                        });

                        $('.milestone').slick({
                            dots: false,
                            vertical: true,
                            slidesToShow: 6,
                            slidesToScroll: 5,
                            centerMode: true,
                            verticalSwiping: true,
                            focusOnSelect: true,

                            responsive: [{
                                breakpoint: 1024,
                                settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 3,
                                    adaptiveHeight: true,
                                },
                            }, {
                                breakpoint: 767,

                                settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 3,
                                    vertical: false,
                                },
                            }, ],
                        });
                    });

                </script>

       
@endsection
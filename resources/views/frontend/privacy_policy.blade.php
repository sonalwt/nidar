 @extends('layouts.app')
@section('content')
     
     

      <!-- small section start-->
      <section id="" class="grey-background section-space position-relative min-height-100"> 
          <div class="top-space-80 mob-space-30"></div>
          <div class="left-line"></div>
          <div class="container small-container">                  
            <div class="row">
              <div class="col-md-12">
                <div class="">
                  <h2 class="main-title mg-bot-title orange-line-bottom">Privacy policy</h2>
               
                 <div class="animated-text">
                    <p class="green-text">
                    Thank you for visiting our web site. This privacy policy tells you how we use personal information collected at this site. Please read this privacy policy carefully before using this website or submitting any personal information. By using the site, you are accepting the practices described in this privacy policy. These practices may be changed, but any changes will be posted and changes will only apply to activities and information on a going forward, not retroactive basis. You are encouraged to review the privacy policy whenever you visit the site to make sure that you understand how any personal information you provide will be used.</p>

                    <p> <strong>Note:</strong> The privacy practices set forth in this privacy policy are for this web site only. If you link to other web sites, please review the privacy policies posted at those sites.</p>

                    <h3 class="title-static">Collection of Information</h3>

                    <p>We collect personally identifiable information, like names, postal addresses, email addresses, etc., when voluntarily submitted by our visitors. The information you provide is used to fulfil you specific request. This information is only used to fulfil your specific request, unless you give us permission to use it in another manner, for example to add you to one of our mailing lists.</p>

                    <h3 class="title-static">Cookie/Tracking Technology</h3>

                    <p>The Site may use cookie and tracking technology depending on the features offered. Cookie and tracking technology are useful for gathering information such as browser type and operating system, tracking the number of visitors to the Site, and understanding how visitors use the Site. Cookies can also help customize the Site for visitors. Personal information cannot be collected via cookies and other tracking technology, however, if you previously provided personally identifiable information, cookies may be tied to such information. Aggregate cookie and tracking information may be shared with third parties.</p>


                     <h3 class="title-static">Distribution of Information</h3>

                     <p>We may share information with governmental agencies or other companies assisting us in fraud prevention or investigation. We may do so when: (1) permitted or required by law; or, (2) trying to protect against or prevent actual or potential fraud or unauthorized transactions; or, (3) investigating fraud which has already taken place. The information is not provided to these companies for marketing purposes.</p>

                      <h3 class="title-static">Commitment to Data Security</h3>

                      <p>Your personally identifiable information is kept secure. Only authorized employees, agents and contractors (who have agreed to keep information secure and confidential) have access to this information. All emails and newsletters from this site allow you to opt out of further mailings.</p>

                      <!-- <h3 class="title-static">Privacy Contact Information</h3>

                      <p>If you have any questions, concerns, or comments about our privacy policy you may contact us using the information below:Info@xyz.com (email id)</p> -->


                  </div>            
                </div>
                
              </div>    

                       
            </div>
          </div> 
        </section>
       

@endsection
 

  
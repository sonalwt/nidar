.0632+@extends('layouts.app')
@section('title')
{{$brandpage->meta_title}}
@endsection
@section('keyword')
{{ $brandpage->meta_keyword}}
@endsection
@section('description')
{{$brandpage->meta_description}}
@endsection
@section('content')
@section('content')
      <!-- banner section start -->      
      <div id="" class="inner-banner-section brands-banner banner-overlay" style="background-image: url({{$brandpage->desktop_banner}}) !important;">
        <div class="left-line-banner"></div>
        <!-- <div class="verticle-page-title">The conglomerate</div> -->
          
        
        <div class="container-fluid nopadding">
          <div class="banner-title">
            <h1 class="hero-title inner-page-title animated-inter-title">Our <br> Brands</h1>
          </div>            
        </div>  
      </div>
      <!-- banner section end -->

      <!-- All brands listing section start-->
      <section id="" class="green-background section-space position-relative"> 
          <div class="left-line"></div>
          <div class="landing-container"> 
          <div class="left-pointer"></div>           
            <div class="row">
              <div class="col-md-12">
                <h2 class="main-title white-color mg-bot-title orange-line-bottom">Overview</h2>
              </div>
            </div>
          </div>



          
            <div class="container small-container">
            <div class="col-md-12">         

               <div class="">
                      {!! $brandpage->overview !!}
                     
                     
                      </div>
                    
              </div>
               
            </div>
        <br><br>
          </div>
                  @if(isset($verticals) && !empty($verticals))
          @foreach($verticals as $key=>$vertical) 
           @if($key==0) 
                <input type="hidden" id="vid" value="{{$vertical->id}}">
            
                    @endif
                    @endforeach
                    @endif
          @if(isset($verticals) && !empty($verticals))
          <div class="landing-container"> 
          <div class="left-pointer"></div>           
            <div class="row">
              <div class="col-md-12">
                <h2 class="main-title white-color mg-bot-title orange-line-bottom">Our Business Verticals</h2>
              </div>
              <div class="col-md-12">  
                <div id="brand-carousel" class="owl-carousel owl-theme white-circle-nav filters filter-button-group">                   
                  @foreach($verticals as $key=>$vertical) 
                  
                  <div class="item">
                    
                    <div class="d-flex single-brand-item flex-column  scrollTo_b  @if($key==0) {{'active'}}  @endif" data-filter=".brand{{$vertical->id}}" data-brand="{{$vertical->title}} Brands">
                      <div class="img-brand">
                        <img src="{{asset($vertical->featured_image)}}" class="img-responsive">
                      </div>
                      <div class="brand-name">
                        <p>{{$vertical->title}} </p>
                      </div>
                    </div>
                  </div>
                  @endforeach
                  

                 
                 
                  
                </div>
              </div>  

                       
            </div>
          </div> 
          @endif
      </section>
      <!-- All brands listing section end-->


    
   <!-- brands  start-->
       <section id="tab-content-brands" class="white-light-background section-space position-relative"> 
         <div class="left-line"></div>
         <div class="landing-container">                     
            <div class="row">
              <div class="col-md-12 text-center">                
                <h2 class="main-title mg-bot-title text-center brand-type">Our Real Estate Brands</h2>
              </div>
            </div>

            <div class="row">
               <div class="col-md-10 col-md-offset-1 col-sm-12">
                  <div class="d-flex flex-wrap block-logos grid brand-page-logo">
                    @foreach($brands as $brand)
                    <div class="grid-item single-logo-block brand{{$brand->buisiness_vertical_id}} flex-column">
                      <div>
                        <img src="{{asset($brand->featured_image)}}" class="desk-img">
                        <img src="{{asset($brand->featured_image_mob)}}" class="mob-img">
                      </div>
                      <div class="d-flex flex-align-center">                       
                      
                      <div class="brand-logo">
                        <img src="{{asset($brand->brand_logo)}}" class="img-responsive">
                      </div>  
                      <div>
                        <p>
                        {{$brand->short_description}}
                        </p>
                      </div>
                      <div class="know-more-btn">
                              <a href="javascript:void(0);" class="btn btn-all btn-orange btn-brnd scale-btn flex-btn">
                              <span>Read More</span>
                                  <div class="show-arrow">
                                   <img src="{{asset('assets/images/right.png')}}" class="img-responsive">
                                  </div>                    
                              </a>
                      </div>
                      </div>
                    </div>
                    @endforeach
                    
                    
                                                            
                  </div>
               </div>
             </div>
          </div>
       </section>
        <!-- brands  end-->

    @endsection
    @section('scripts')
 <script type="text/javascript">
         $(document).ready(function(){
          $('#brand-carousel').owlCarousel({
              loop:true,
              margin:10,
              nav: true,  
              smartSpeed:500,           
              dots:false,
              autoplay:true,
              smartSpeed:2000,
             autoplayHoverPause:true,
              mouseDrag:false,
              touchDrag:false,
              navText: ["<img src='assets/images/left.png'>", "<img src='assets/images/right.png'>"],

              responsive:{
                  0:{
                      items:2
                  },
                  600:{
                      items:4
                  },
                  1200:{
                      items:7
                  },
                  1600:{
                      items:7
                  }
              }
          });

        
          // filter items on button click
          var vid=$('#vid').val();
          $(".single-logo-block").hide();
           $(".single-logo-block").filter('.brand'+vid).show();
            $('.filter-button-group').on( 'click', '.single-brand-item', function() {

              $(".single-logo-block").hide();
             $('.single-brand-item').removeClass('active');
             $(this).addClass('active');
              var filterValue = $(this).attr('data-filter');             
               $(".single-logo-block").filter(filterValue).show();

               var filtername = $(this).attr('data-brand');  
               $('.brand-type').text();

               $('.brand-type').text(filtername);

               TweenMax.fromTo(
                '.single-logo-block img',
                0.2, {            
                    scale:0.8,
                   
                }, {            
                    scale: 1,
                            
                }
            );
              
            });

             
            $('.scrollTo_b').click(function(e){
              e.preventDefault();
              var target = $('#tab-content-brands');
              if(target.length){
                var scrollTo = target.offset().top-100;
                $('body, html').animate({scrollTop: scrollTo+'px'}, 500);
              }
            });
        

        

         });
          
      </script>
       
@endsection
@extends('layouts.app')
@section('title')
{{$newsroom->meta_title}}
@endsection
@section('keyword')
{{ $newsroom->meta_keyword}}
@endsection
@section('description')
{{$newsroom->meta_description}}
@endsection
@section('content')
<!-- banner section start -->
    <div id="" class="inner-banner-section newsroom-banner banner-overlay" style="background-image: url({{$newsroom->desktop_banner}}) !important;">

        <div class="container-fluid nopadding">
            <div class="banner-title">
                <h1 class="hero-title inner-page-title animated-inter-title">Newsroom</h1>
            </div>
        </div>
    </div>
    <!-- banner section end -->


    <section id="" class="grey-background position-relative">
        <!-- <div class="left-line"></div> -->
        <div class="container-fluid ">
            <div class="row">
                <div class="col-md-12 nopadding">
                	@if(isset($recents) && !empty($recents))
                    <div class="col-md-5">
                        <div class="article-block">
                            <p>Recent Articles</p>

                            <div id="article-slider" class="article-slider owl-carousel owl-theme orange-nav ">
                            	@foreach($recents as $recent)
                                <div class="item">
                                    <div class="single-article-content">
                                       <a href="{{$recent->news_link}}" target="_blank"><h3 class="aticle-title">{{$recent->title}}</h3></a>
                                        <div class="article-descri">
                                       {!! $recent->description !!}
                                        </div>

                                    </div>
                                </div>
                                @endforeach
                             


                            </div>
                        </div>
                        <div class="recent-btn">
                            <a href="recentArticles.php">View All Recent Articles</a>
                        </div>
                    </div>
                    @endif
                   @if(isset($mosts) && !empty($mosts))
                    <div class="col-md-7 nopadding most-vw-wrap">
                    	 
                        <div class="most-vw-block ">


                            <div id="most-article-slider" class="article-slider owl-carousel owl-theme orange-nav ab-nav">
                            	@foreach($mosts as $most)
                                <div class="item">
                                    <div class="single-article flex-wrap-mob">

                                        <div class="img-article w-m-100">
                                            <img src="{{asset($most->image_url)}}">
                                        </div>

                                        <div class="single-article-content w-m-100">
                                            <p>Most Viewed</p>
                                            <a href="{{$most->news_link}}" target="_blank"><h3 class="aticle-title" >{{$most->title}}</h3></a>
                                            <div class="article-descri">
                                                {!! $most->description !!}
                                            </div>

                                        </div>

                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="heading-most-viewed">
                            <a href="{{url('/articles')}}" class="white-color">View all Popular Article</a>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </section>



    <section id="video_corner" class="section-space position-relative">
        <div class="container">
            <div class="row">
                <div id="exTab1" class="title-wrap container">
                    <div class="col-md-12 nopadding-mob">  
              <div class="m-hide-line">
                 <h2 class="main-title green-title mg-bot-title big-line-title">Video Corner</h2>
              </div> 
                <ul class="list-filter filters filter-button-group-v">                   
                  <li class="active">
                    <div class="single-filter-item vf" data-filter="*">
                        <p>All</p>                    
                    </div>
                  </li>
                  @foreach($vverticals as $key=>$vertical)
                  @if($key <= 1)
                  <li class="">
                    <div class="single-filter-item vf" data-filter=".v-{{$vertical->id}}">  
                        <p>{{$vertical->title}} </p>                   
                    </div>
                  </li>
                  @else
                  @php
                  $asdf[]=$vertical;
                  @endphp
                  @endif
                  @endforeach
                
                <!--    <li class="">
                    <div class="single-filter-item vf" data-filter=".v-infrastructure">  
                        <p>Infrastructure</p>                     
                    </div>
                  </li>  -->
                  
                   @if(isset($asdf) && !empty($asdf))
                  <li class="dropdown dropdown-filter">
                      <a data-toggle="dropdown" class="dropdown-toggle" href="#">More<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                        @foreach($asdf as $as)
                           <li class="">
                            <div class="single-filter-item vf" data-filter=".v-{{$as->id}}">  
                                <p>{{$as->title}}</p>                     
                            </div>
                          </li>      
                     @endforeach

                        </ul>
                  </li> 
                  @endif
                </ul>
              </div>
                    <div class="">
                        <div class="">
                            <div class="vehicle-detail-banner clearfix">
                                <div class="banner-slider">
                                	@foreach($videos as $key=>$video)
                                	@if($key==0)
                                    <div class="slider slider-for-one  slider-for">
                                        <div class="slider-banner-image video-iframe">
                                            <iframe src="https://www.youtube.com/embed/{{$video->video_url}}?controls=0&amp;showinfo=0&amp;loop=1&playlist=hj1jHh2MOgE&amp;enablejsapi=1&amp;iv_load_policy=3&amp;rel=0&amp;modestbranding=1" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>


                                            <div class="video-content">
                                                <span class="patch patch-dc bui-{{$video->buisinessvertical->id}}">{{$video->buisinessvertical->title}}</span>
                                                <h2 class="vid-head">{!! $video->title !!}</h2>
                                                <p class="vid-date">{{date('d F, Y',strtotime($video->release_date))}}</p>                                            
                                            </div>
                                        </div>                                                                                                                  
                                                                             
                                    </div>
                                    @endif
                                   @endforeach
                                    <div id="style-3" class="slider slider-nav-one thumb-image slider-nav scrollbar video-thumbs">
                                    	@foreach($videos as $key=>$video)
                                      @if(!empty($video))
                                        <div data-video="{{$video->video_url}}" class="thumbnail-image single-v-news v-{{$video->buisinessvertical->id}}">
                                            <div class="thumbImg">
                                                <!-- https://youtu.be/hj1jHh2MOgE -->
                                                <img src="{{asset($video->image_url)}}">
                                            </div>
                                            <div class="thumb-data">
                                                <span class="patch patch-dc bui-{{$video->buisinessvertical->id}}">{{$video->buisinessvertical->title}}</span>
                                                <p class="thumb-desc">{{$video->title}} </p>
                                                <p class="thumb-date">{{date('d F, Y',strtotime($video->release_date))}}</p>
                                            </div>
                                        </div>
                                        @endif
                                   @endforeach
                                         
                                    </div>
                                    
                                </div>
                            </div>
                        </div>                       
                    </div>
                </div>
            </div>
        </div>
    </section>


    <div id="press_release" class="section-space position-relative">
        <div class="container">
            <div class="row">
            <div id="exTab1" class="title-wrap container">
            <div class="col-md-12 nopadding-mob">  
              <div class="m-hide-line">
                 <h2 class="main-title green-title mg-bot-title big-line-title">Press Release</h2>
              </div> 
                <ul class="list-filter filters filter-button-group-p">                   
                  <li class="active">
                    <div class="single-filter-item pf" data-filter=".all-p">
                        <p>All</p>                    
                    </div>
                  </li>
                  @foreach($pverticals as $key=>$vertical)

                  @if($key <= 2)
                  <li class="">
                    <div class="single-filter-item pf" data-filter=".p-{{$vertical->id}}">  
                        <p>{{$vertical->title}} </p>                   
                    </div>
                  </li>
                  @else
                  @php
                  $asdf1[]=$vertical;
                  @endphp
                  @endif
                  @endforeach
                
                <!--    <li class="">
                    <div class="single-filter-item vf" data-filter=".v-infrastructure">  
                        <p>Infrastructure</p>                     
                    </div>
                  </li>  -->
                  
                   @if(isset($asdf1) && !empty($asdf1))
                  <li class="dropdown dropdown-filter">
                      <a data-toggle="dropdown" class="dropdown-toggle" href="#">More<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                        @foreach($asdf1 as $as)
                           <li class="">
                            <div class="single-filter-item pf" data-filter=".p-{{$as->id}}">  
                                <p>{{$as->title}}</p>                     
                            </div>
                          </li>      
                     @endforeach

                        </ul>
                  </li> 
                  @endif
                </ul>
              </div>
              <div class="">
                        <div class="press-thumbs" id="1p">
                           @php
                            $preleases=App\Models\Pressrelease::where('status','Enabled')->orderBy('id','DESC')->take(5)->get();
                           @endphp
                            <div class="press-parts all-p">
                              @foreach($preleases as $key=>$prelease)
                              @if($key==0)
                                <div class="wrapper press-tab-1">
                                    <div id="press-tab-one">
                                        <div class="">
                                            <div class="press-card">
                                                <span class="patch patch-re bui-{{$prelease->buisinessvertical->id}}">{{$prelease->buisinessvertical->title}}</span>
                                                <div class="press-img">
                                                    <img src="{{asset($prelease->image_url)}}">
                                                </div>
                                                <div class="press-data">
                                                    <a href="{{$prelease->news_link}}" target="_blank"><h2 class="press-desc">{{$prelease->title}}</h2></a>
                                                    <p class="press-date">{{date('d F, Y',strtotime($prelease->release_date))}}</p>
                                                    <p class="news-data">{{$prelease->description}}</p>
                                                </div>
                                            </div>
                                        </div>  
                                    </div>

                                </div>
                                 @endif 
                                 @if($key==1)
                                <div class="wrapper press-tab-2">
                                    <div class="tab-2-data">
                                        <span class="patch patch-re bui-{{$prelease->buisinessvertical->id}}">{{$prelease->buisinessvertical->title}}</span>

                                        <div class="press-data">
                                            <a href="{{$prelease->news_link}}" target="_blank"><h2 class="press-desc">{{$prelease->title}}</h2></a>
                                            <p class="press-date">{{date('d F, Y',strtotime($prelease->release_date))}}</p>
                                            <p class="news-data">{{$prelease->description}}</p>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @if( $key > 1)
                                @if($key==2) 
                                @php
                                $j=1;
                                @endphp

                                <div class="wrapper press-tab-3">
                                    <div class="tab-3-data">
                                      @endif
                                        <div class="release-{{$j}}">
                                            <div class="thumbnail-image">
                                                <div class="thumbImg">
                                                    <img src="{{asset($prelease->image_url)}}" alt="slider-img">
                                                </div>
                                                <div class="thumb-data">
                                                    <span class="patch patch-re bui-{{$prelease->buisinessvertical->id}}">{{$prelease->buisinessvertical->title}}</span>
                                                    <a href="{{$prelease->news_link}}" target="_blank"><p class="thumb-desc">{{$prelease->title}}</p></a>
                                                    <p class="thumb-date">{{date('d F, Y',strtotime($prelease->release_date))}} </p>
                                                </div>
                                            </div>
                                        </div>
                                        @php
                                        $j++;
                                        @endphp

                                         @if($key==4)
                                    </div>
                                    <div class="press-release-btn">
                                        <a href="javascript:void(0);">View All Press Releae</a>
                                    </div>
                                </div>
                                @endif
                                @endif
                                @endforeach
                            </div>
                           @foreach($pverticals as $key1=>$vertical)

                           @php
                            $preleases=App\Models\Pressrelease::where('status','Enabled')->where('business_vertical',$vertical->id)->take(5)->get();
                           
                           @endphp
                            <div class="press-parts p-{{$vertical->id}}">
                              @foreach($preleases as $key=>$prelease)
                              @if($key==0)
                                <div class="wrapper press-tab-1">
                                    <div id="press-tab-one">
                                        <div class="">
                                            <div class="press-card">
                                                <span class="patch patch-re bui-{{$prelease->buisinessvertical->id}}">{{$prelease->buisinessvertical->title}}</span>
                                                <div class="press-img">
                                                    <img src="{{asset($prelease->image_url)}}">
                                                </div>
                                                <div class="press-data">
                                                    <a href="{{$prelease->news_link}}" target="_blank"><h2 class="press-desc">{{$prelease->buisinessvertical->title}}</h2></a>
                                                    <p class="press-date">{{date('d F, Y',strtotime($prelease->release_date))}}</p>
                                                    <p class="news-data">{{$prelease->description}}</p>
                                                </div>
                                            </div>
                                        </div>  
                                    </div>

                                </div>
                                 @endif 
                                 @if($key==1)
                                <div class="wrapper press-tab-2">
                                    <div class="tab-2-data">
                                        <span class="patch patch-re bui-{{$prelease->buisinessvertical->id}}">{{$prelease->buisinessvertical->title}}</span>

                                        <div class="press-data">
                                            <a href="{{$prelease->news_link}}" target="_blank"><h2 class="press-desc">{{$prelease->buisinessvertical->title}}</h2></a>
                                            <p class="press-date">{{date('d F, Y',strtotime($prelease->release_date))}}</p>
                                            <p class="news-data">{{$prelease->description}}</p>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @if( $key > 1)
                                @if($key==2) 
                                <div class="wrapper press-tab-3">
                                    <div class="tab-3-data">
                                      @endif
                                        <div class="release-1">
                                            <div class="thumbnail-image">
                                                <div class="thumbImg">
                                                    <img src="{{asset($prelease->image_url)}}" alt="slider-img">
                                                </div>
                                                <div class="thumb-data">
                                                    <span class="patch patch-re bui-{{$prelease->buisinessvertical->id}}">{{$prelease->buisinessvertical->title}}</span>
                                                    <a href="{{$prelease->news_link}}" target="_blank"><p class="thumb-desc">{{$prelease->title}}</p></a>
                                                    <p class="thumb-date">{{date('d F, Y',strtotime($prelease->release_date))}}</p>
                                                </div>
                                            </div>
                                        </div>
                                         @if($key==4)
                                    </div>
                                    <div class="press-release-btn">
                                        <a href="{{url('/articles')}}">View All Press Releae</a>
                                    </div>
                                </div>
                                @endif
                                @endif
                                @endforeach
                            </div>
                           @endforeach
                            
                        </div>




                                                
                       
                        
                    </div>
            </div>
          </div>
</div>
</div>


    <section id="media-resource" class="section-space position-relative hidden">
        <div class="container">
            <div class="row">
                <div id="exTab1" class="col-lg-8 col-md-8 title-wrap container">
                    <div class="">  
                          <div class="m-hide-line">
                             <h2 class="main-title green-title mg-bot-title big-line-title">Media Resources</h2>
                          </div> 
                            <ul class="list-filter filters filter-button-group">                   
                              <li class="active">
                                <div class="single-filter-item" data-filter="*">
                                    <p>All</p>                    
                                </div>
                              </li>
                              <li class="">
                                <div class="single-filter-item" data-filter=".m-realEstate">  
                                    <p>Real Estate </p>                   
                                </div>
                              </li>
                              <li class="">
                                <div class="single-filter-item" data-filter=".m-oilnGas">  
                                    <p>Oil and Gas</p>                     
                                </div>
                              </li> 
                              <li class="dropdown dropdown-filter">
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#">More<b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                      <li class="">
                                        <div class="single-filter-item" data-filter=".m-infrastructure">  
                                            <p>Industrial and Logistics Parks</p>                     
                                        </div>
                                      </li>  
                                       <li class="">
                                        <div class="single-filter-item" data-filter=".m-dataCenter">  
                                            <p>Data Center</p>                     
                                        </div>
                                      </li>   

                                    </ul>
                              </li> 
                            </ul>
                        </div>
            


                    <div class="tab-content clearfix">
                        <div class="tab-pane active" id="1E">
                            <div>
                                <div class="item">
                                <div class="w-m-100">
                                    <div class="d-flex flex-wrap-mob">
                                        <div class="right-mews-one w-m-100 light-back">
                                            <div class="press-wrapper">
                                                <div class="press-block-light">
                                                    <div class="position-relative">
                                                        <img src="images/newsroom/media-resource/mr1.jpg" class="img-responsive">
                                                    </div>
                                                    <div class="news-block pd-20">
                                                        <!-- news block -->
                                                        <span class="patch patch-dc">Data Center</span>
                                                       <a href=" https://www.financialexpress.com/industry/hiranandani-group-to-invest-rs-7k-crore-to-set-up-data-centre-in-greater-noida/2115029/" target="_blank"> <h3 class="media-title press-title news-mid">Hiranandani group to invest Rs 7k crore to set up data centre in Greater Noida</h3></a>
                                                        <p class="press-date">28th October, 2020</p>
                                                        <div class="media-descirption">
                                                            <p>Yotta Infrastructure, a Hiranandani group firm, on Thursday said it has received approval from the Uttar Pradesh government to set up a 20-acre data centre park in Greater Noida.</p>
                                                        </div>


                                                    </div>
                                                    <!-- news block  end-->

                                                </div>
                                            </div>
                                        </div>
                                        <div class="right-mews-two mg-left-10 w-m-100 light-back">
                                            <div class="press-wrapper">
                                                <div class="press-block-light">
                                                    <div class="wrapper">
                                                        <div class="media-tab-2-data"> 
                                                            <div class="press-data">
                                                                <img class="event-news-img" src="images/newsroom/media-resource/mr2.jpg">
                                                                <span class="patch patch-re">Real Estate</span>
                                                                <a href="https://www.hiranandanicommunities.com/latestnews/integrated-township-living-Offers-ease-of-living" target="_blank"><h2 class="press-desc">Hiranandani Parks at Oragadam Offers Holistic Living</h2></a>
                                                                <p class="press-date">1st October, 2020</p>
                                                                <p class="news-data">For an integrated township which already offers a bouquet of amenities and facilities, the announcement of setting up a data centre park adds to the positives of </p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>




                                        </div>
                                    </div>
                                </div>

                                <div class="w-m-100">
                                    <div class="d-flex flex-wrap-mob">
                                        <div class="right-mews-one w-m-100 light-back">
                                            <div class="">                                              
                                                    <div class="press-wrapper">
                                                        <div class="press-block-light media-tab-2-data">
                                                            <div class="position-relative">
                                                                <img src="images/newsroom/media-resource/mr3.jpg" class="img-responsive">

                                                            </div>
                                                            <div class="news-block pd-20">
                                                                <!-- news block -->
                                                                <a href="https://www.hiranandanicommunities.com/latestnews/praxis-media-in-association-with-education-connect-announced-the-prestigious-national-education-excellence-awards-on-june-30-2020" target="_blank"><h3 class="news-title press-title news-mid">Praxis Media in association with Education Connect announced the prestigious National Education Excellence Awards on June 30, 2020</h3></a>
                                                                <div class="news-descirption">
                                                                    <p>The National Education Excellence Awards, 2020 were announced by India's leading media and marketing group, Praxis Media to identify, celebrate and encourage excellence in education and learning....</p>
                                                                </div>

                                                            </div>
                                                            <!-- news block  end-->

                                                        </div>
                                                    </div>
                                                </div>                                             
                                            </div>

                                            <div class="right-mews-one w-m-100 light-back">
                                            <div class="press-wrapper">
                                                <div class="press-block-light ">
                                                    <div class="position-relative">
                                                        <img src="images/newsroom/media-resource/mr4.jpg" class="img-responsive">
                                                    </div>
                                                    <div class="news-block pd-20">
                                                        <!-- news block -->
                                                        <span class="patch patch-og">Oil & Gas</span>
                                                        <h3 class="media-title press-title news-mid">Flagship India LNG project commissioning in April</h3>
                                                        <p class="press-date">6th April, 2021</p>
                                                        <div class="media-descirption">
                                                            <p>Yotta Infrastructure, a Hiranandani group firm, on Thursday said it has received approval from the Uttar Pradesh government to set up a 20-acre data centre park in Greater Noida.</p>
                                                        </div>


                                                    </div>
                                                    <!-- news block  end-->

                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        
                                    </div>
                                    </div>
                                </div>



                            </div>

                           
                        </div>
                         <div class="media-resource-btn">
                                <a href="#" class="media-view">View All</a>
                            </div>
                    </div>

               

                <div class="col-lg-4 col-md-4 mg-mb-20 newsletter-content">
                    <div class="d-flex flex-align-center flex-justify-spc-bet newsletter-head">
                        <h2 class="main-title green-title">Newsletters</h2>
                        <div class="press-release-btn">
                            <a href="#footer" class="scrollTo_footer">Subscribe Now</a>
                        </div>
                    </div>
                    

                    <div class="newsletter-block">
                        <div class="single-newsletter">
                            <div class="thumbnail-image">
                                <div class="thumbImg">
                                    <img src="images/newsroom/news/news1.jpg" alt="slider-img">
                                </div>
                                <div class="thumb-data">                                    
                                    <p class="thumb-desc">Quarterly Newsletter</p>
                                    <p class="thumb-date">July, 2018</p>
                                    <a href="newsletters/hfc/2018/nl_jul.pdf" class="btn btn-small" target="_blank">view</a>
                                </div>

                            </div>
                        </div>

                        <div class="single-newsletter">
                            <div class="thumbnail-image">
                                <div class="thumbImg">
                                    <img src="images/newsroom/news/news2.jpg" alt="slider-img">
                                </div>
                                <div class="thumb-data">                                    
                                    <p class="thumb-desc">The Township</p>
                                    <p class="thumb-date">January, 2019</p>
                                    <a href="newsletters/hfc/2019/nl_jan.pdf" class="btn btn-small" target="_blank">view</a>
                                </div>
                            </div>
                        </div>
                        <div class="single-newsletter">
                            <div class="thumbnail-image">
                                <div class="thumbImg">
                                    <img src="images/newsroom/news/news3.jpg" alt="slider-img">
                                </div>
                                <div class="thumb-data">                                   
                                    <p class="thumb-desc">The introduction of MAHARERA has paved the way for a regulated and process-driven ‘Real Estate Sector’</p>
                                    <p class="thumb-date">June, 2017</p>
                                    <a href="newsletters/hp/2017/nl_jun.pdf" class="btn btn-small" target="_blank">view</a>
                                </div>
                            </div>
                        </div>

                         <div class="single-newsletter">
                            <div class="thumbnail-image">
                                <div class="thumbImg">
                                    <img src="images/newsroom/news/hfc_nl_oct_19.jpg" alt="slider-img">
                                </div>
                                <div class="thumb-data">                                   
                                    <p class="thumb-desc">The Festivities Begin</p>
                                    <p class="thumb-date">1st October, 2020</p>
                                    <a href="newsletters/hfc/2019/nl_oct.pdf" class="btn btn-small" target="_blank">view</a>
                                </div>
                            </div>
                        </div>


                        <div class="single-newsletter">
                            <div class="thumbnail-image">
                                <div class="thumbImg">
                                    <img src="images/newsroom/news/news4.jpg" alt="slider-img">
                                </div>
                                <div class="thumb-data">                                    
                                    <p class="thumb-desc">Mount Alterra Khandala </p>
                                    <p class="thumb-date">May, 2019</p>
                                    <a href="newsletters/ma/2019/nl_may.pdf" class="btn btn-small" target="_blank">view</a>
                                </div>
                            </div>
                        </div>

                        <div class="single-newsletter">
                            <div class="thumbnail-image">
                                <div class="thumbImg">
                                    <img src="images/newsroom/news/hp_nl_may_19.jpg" alt="slider-img">
                                </div>
                                <div class="thumb-data">                                   
                                    <p class="thumb-desc">Hiranadani Parks Oragaadam</p>
                                    <p class="thumb-date">May, 2019</p>
                                    <a href="newsletters/hp/2019/nl_may.pdf" class="btn btn-small" target="_blank">view</a>
                                </div>
                            </div>
                        </div>

                       

                      

                        


                    </div>
                </div>

            </div>
        </div>
    </section>

    <div id="events" class="section-space position-relative hidden">
        <div class="container">
            <div class="row">
                <div id="exTab1" class="title-wrap container">
                    <div class="col-md-12">  
              <div class="m-hide-line">
                 <h2 class="main-title green-title mg-bot-title big-line-title">Events</h2>
              </div> 
                <ul class="list-filter filters filter-button-group">                   
                  <li class="active">
                    <div class="single-filter-item" data-filter="*">
                        <p>All</p>                    
                    </div>
                  </li>
                  <li class="">
                    <div class="single-filter-item" data-filter=".p-realEstate">  
                        <p>Real Estate </p>                   
                    </div>
                  </li>
                  <li class="">
                    <div class="single-filter-item" data-filter=".p-oilnGas">  
                        <p>Oil and Gas</p>                     
                    </div>
                  </li>
                   <li class="hidden-xs">
                    <div class="single-filter-item" data-filter=".p-infrastructure">  
                        <p>Industrial and Logistics Parks</p>                     
                    </div>
                  </li> 
                  
                   
                  <li class="dropdown dropdown-filter">
                      <a data-toggle="dropdown" class="dropdown-toggle" href="#">More<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                          <li class="visible-xs">
                            <div class="single-filter-item" data-filter=".p-infrastructure">  
                                <p>Industrial and Logistics Parks</p>                     
                            </div>
                          </li>  
                           <li class="">
                            <div class="single-filter-item" data-filter=".p-dataCenter">  
                                <p>Data Center</p>                     
                            </div>
                          </li>   

                        </ul>
                  </li> 
                </ul>
              </div>


                    <div class="tab-content clearfix">
                        <div class="tab-pane active" id="1E">
                            <div>
                                <div class="wrapper press-tab-1">


                                    <div id="event-tab-one" class="">
                                        <div class="">
                                            <div class="press-card">                                                
                                                <div class="press-img">
                                                    <img src="images/newsroom/event/e1.jpg">
                                                </div>
                                                <div class="press-data">
                                                    <h2 class="press-desc">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</h2>
                                                    <p class="press-date">1st October, 2020</p>
                                                    <p class="news-data">Lorem ipsum dolor sit amet,consectetuer adipiscing elit</p>
                                                </div>
                                            </div>
                                        </div>                                        
                                    </div>





                                </div>
                                <div class="wrapper press-tab-2">
                                    <div class="tab-2-data">                                       
                                        <div class="press-data">
                                            <img class="event-news-img" src="images/newsroom/event/e2.jpg">
                                            <h2 class="press-desc">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean</h2>
                                            <p class="press-date">1st October, 2020</p>
                                            <p class="news-data">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="wrapper press-tab-3">
                                    <div class="tab-3-data">
                                        <div class="release-1">
                                            <div class="thumbnail-image">
                                                <div class="thumbImg">
                                                    <img src="images/newsroom/event/e3.jpg" alt="slider-img">
                                                </div>
                                                <div class="thumb-data">
                                                  
                                                    <p class="thumb-desc">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. </p>
                                                    <p class="thumb-date">1st October, 2020</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="release-2">
                                            <div class="thumbnail-image">
                                                <div class="thumbImg">
                                                    <img src="images/newsroom/event/e4.jpg" alt="slider-img">
                                                </div>
                                                <div class="thumb-data">
                                                   
                                                    <p class="thumb-desc">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. </p>
                                                    <p class="thumb-date">1st October, 2020</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="release-3">
                                            <div class="thumbnail-image">
                                                <div class="thumbImg">
                                                    <img src="images/newsroom/event/e5.jpg" alt="slider-img">
                                                </div>
                                                <div class="thumb-data">
                                                   
                                                    <p class="thumb-desc">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. </p>
                                                    <p class="thumb-date">1st October, 2020</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="press-release-btn">
                                        <a href="#">View All Events</a>
                                    </div>
                                </div>
                            </div>
                        </div>                       
                    </div>

                </div>
            </div>
        </div>
    </div>


@endsection
 @section('scripts')
   <script type="text/javascript">
        $(document).ready(function() {
            $('#article-slider').owlCarousel({
                loop: false,
                margin: 10,
                nav: true,
                dots: false,
                items: 1,
                smartSpeed: 300,
                navText: ["<img src='assets/images/left-arrow.png'>", "<img src='assets/images/right-arrow.png'>"],

            });

            $('#most-article-slider').owlCarousel({
                loop: false,
                margin: 10,
                nav: true,
                dots: false,
                items: 1,
                smartSpeed: 300,
                navText: ["<img src='assets/images/left-arrow.png'>", "<img src='assets/images/right-arrow.png'>"],

            });



        // filters
        $('.filter-button-group-v').on( 'click', '.single-filter-item.vf', function() {
              $(".video-thumbs .single-v-news").css('display', 'none!important');
             $(".video-thumbs .single-v-news").hide();
             $('.single-filter-item.vf').parent('li').removeClass('active');
             $(this).parent('li').addClass('active');
             var filterValue = $(this).attr('data-filter');             
             $(".video-thumbs .single-v-news").filter(filterValue).show();
               TweenMax.fromTo(
                '.single-v-news img',
                0.2, {            
                    scale:0.8,
                   
                }, {            
                    scale: 1,
                            
                }
            );
              
        }); 

        $(".press-thumbs .press-parts").hide();
        $(".press-thumbs .press-parts").filter('.all-p').show();
        $('.filter-button-group-p').on( 'click', '.single-filter-item.pf', function() {             
              $(".press-thumbs .press-parts").hide();
             $('.single-filter-item.pf').parent('li').removeClass('active');
             $(this).parent('li').addClass('active');
             var filterValue = $(this).attr('data-filter');             
             $(".press-parts").filter(filterValue).show();
              
              
        }); 


         $('.scrollTo_footer').click(function(e){
              e.preventDefault();
              var target = $('#footer');
              if(target.length){
                var scrollTo = target.offset().top-100;
                $('body, html').animate({scrollTop: scrollTo+'px'}, 500);
              }
        });


         

        $('div.single-v-news').click(function() {
            
            let dataVideo = $(this).attr('data-video');                  

            let customSrc =  "https://www.youtube.com/embed/" + dataVideo + "?controls=0&amp;showinfo=0&amp;loop=1&playlist=" + dataVideo + "&amp;enablejsapi=1&amp;iv_load_policy=3&amp;rel=0&amp;modestbranding=1";

            let SelectedPatch =  $(this).children().find('span').text(); 
           
            let PatchClassName =  $(this).children().find('span').attr('class'); 
          
            let SelectedVTitle =  $(this).children().find('.thumb-desc').text();
          
            let SelectedVDate =  $(this).children().find('.thumb-date').text();
                      

            $('.video-iframe iframe').attr('src', customSrc);            
            $('.video-iframe .video-content span').text(SelectedPatch);
            $('.video-iframe .video-content span').removeClass();
            $('.video-iframe .video-content span').addClass(PatchClassName);
            $('.video-iframe .vid-head').text(SelectedVTitle);
            $('.video-iframe .vid-date').text(SelectedVDate);
        });
        });

    </script>
  @endsection
@extends('layouts.app')
@section('title')
{{$impact->meta_title}}
@endsection
@section('keyword')
{{ $impact->meta_keyword}}
@endsection
@section('description')
{{$impact->meta_description}}
@endsection
@section('content')
@section('content')
    <!-- banner section start -->
        <div id="" class="inner-banner-section belief-banner banner-overlay"  style="background-image: url({{$impact->desktop_banner}}) !important;">



            <div class="container-fluid nopadding">
                <div class="banner-title">
                    <h1 class="hero-title inner-page-title animated-inter-title">Our <br> Impact</h1>
                </div>
            </div>
        </div>
        <!-- banner section end -->

        <!-- Overview section start-->
        <section id="" class="grey-background section-space position-relative">
            <div class="left-line"></div>
            <div class="landing-container">
                <div class="left-pointer"></div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                        <div class="">
                            {!! $impact->description !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Overview section end-->


        <section id="The_community" class="section-space green-background position-relative">
            <div class="left-line"></div>
            <div class="landing-container">
                <div class="left-pointer"></div>
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="main-title mg-bot-title white-color orange-line-bottom">The Community</h2>

                        {!! $impact->the_cummunity_description !!}

                      
                    </div>

                    <div class="col-md-12">
                        <div class="grid-container-gallery">
                            @php
                            $j=1
                            @endphp
                            @foreach(json_decode($impact->cummunity_img,true) as $com)

                            <div class="grid-item-gal g-gal{{$j}} gal-anim">
                                <div class="gal-thumb">
                                    <img src="{{asset($com['featured_image'])}}">
                                </div>
                                <p class="b-gal-caption">{{$com['title']}}</p>
                            </div>

                            @php
                            if($j==1){
                            $j=21;
                        }else{
                        $j++;
                    }
                           
                           @endphp
                            @endforeach

                 <!--            <div class="grid-item-gal g-gal21 gal-anim">
                                <div class="gal-thumb">
                                    <img src="images/belief/gal/2.png">
                                </div>
                                <p class="b-gal-caption">Women's Day</p>
                            </div>
                            <div class="grid-item-gal g-gal22 gal-anim">
                                <div class="gal-thumb">
                                    <img src="images/belief/gal/3.png">
                                </div>
                                <p class="b-gal-caption">Valentine's Day</p>
                            </div>
                            <div class="grid-item-gal g-gal23 gal-anim">
                                <div class="gal-thumb">
                                    <img src="images/belief/gal/4.png">
                                </div>
                                <p class="b-gal-caption">Sports</p>
                            </div>
                            <div class="grid-item-gal g-gal24 gal-anim">                              

                                <div class="gal-thumb">
                                    <img src="images/belief/gal/13.png">
                                </div>
                                <p class="b-gal-caption">Diwali celebrations</p>


                            </div> -->

                          
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="" class="section-space grey-background position-relative">
            <div class="left-line"></div>
            <div class="landing-container">
                <div class="left-pointer"></div>
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="main-title mg-bot-title green-title orange-line-bottom">Our CSR Initiatives </h2>
                        {!! $impact->csr_initiatives_description !!}
                    </div>
                </div>

                <br>
                <br>

                <div class="row">
                    <div class="col-md-12">

                        <h3 class="sub-title green-title orange-line-bottom">Skill Development</h3>
                        {!! $impact->skill_development_part1 !!}
                        <div id="" class="service-blocks">
                            @foreach(json_decode($impact->coeareas,true) as $coe)
                            <div class="single-service">

                                <div class="service image">
                                    <img src="{{asset($coe['featured_image'])}}">
                                </div>
                                <div class="service-title">
                                    <h4 class="">{{$coe['title']}}</h4></div>
                            </div>
                            @endforeach
<!-- 


                            <div class="single-service">

                                <div class="service image">
                                    <img src="images/belief/c2.png">
                                </div>
                                <div class="service-title">
                                    <h4 class="">Retail</h4></div>
                            </div>

                            <div class="single-service">

                                <div class="service image">
                                    <img src="images/belief/c3.png">
                                </div>
                                <div class="service-title">
                                    <h4 class="">Healthcare</h4></div>
                            </div>

                            <div class="single-service">

                                <div class="service image">
                                    <img src="images/belief/c4.png">
                                </div>
                                <div class="service-title">
                                    <h4 class="">Security</h4></div>
                            </div>

                            <div class="single-service">

                                <div class="service image">
                                    <img src="images/belief/c5.png">
                                </div>
                                <div class="service-title">
                                    <h4 class="">Hospitality</h4></div>
                            </div>

                            <div class="single-service">

                                <div class="service image">
                                    <img src="images/belief/c6.png">
                                </div>
                                <div class="service-title">
                                    <h4 class="">Financial Services</h4></div>
                            </div>
                            <div class="single-service">

                                <div class="service image">
                                    <img src="images/belief/c7.png">
                                </div>
                                <div class="service-title">
                                    <h4 class="">Information Technology</h4></div>
                            </div> -->

                        </div>
                    </div>

                    <div class="col-md-12">

                        {!! $impact->skill_development_part2 !!}
                    </div>

                    <div class="col-md-12">
                        <div class="subsection-space">
                            <h3 class="sub-title green-title orange-line-bottom">Success Stories </h3>

                            <div class="grid-container-communities">
                                @php
                                $i=1;
                                @endphp
                                @foreach($stories as $story)
                                @if($i==1)
                                <div class="grid-item-comm story1">
                                    <div class="d-flex flex-wrap-mob">
                                        <div class="img-person w-50 w-m-100">
                                            <img src="{{asset($story->featured_image)}}">
                                        </div>
                                        <div class="course-detail w-50 w-m-100">
                                            <h3 class="sub2-title">Course</h3>
                                            <p>
                                               {{$story->course }}{{' Total Earnings:'}} {{$story->total_earnings}}
                                            </p>

                                            <h3 class="sub2-title">{{$story->name}}</h3>
                                           {!! $story->description !!}
                                        </div>
                                    </div>
                                </div>
                                @endif
                                 @if($i==2)
                                <div class="grid-item-comm story2 course-overlay">
                                    <div class="img-person"> <img src="{{asset($story->featured_image)}}"></div>
                                    <div class="course-detail po-ab">
                                        <h3 class="sub2-title">{{$story->name}}</h3>
                                        {!! $story->description !!}

                                        <h3 class="sub2-title">Course</h3>
                                        <p>{{$story->course }}{{' Total Earnings:'}} {{$story->total_earnings}} </p>
                                    </div>
                                </div>
                                 @endif
                                 @if($i==3)
                                <div class="grid-item-comm story3">
                                    <div class="d-flex flex-column h-100">
                                        <div class="img-person h-50">
                                            <img src="{{asset($story->featured_image)}}">
                                        </div>
                                        <div class="course-detail h-50">
                                            <h3 class="sub2-title">Course</h3>
                                            <p>
                                               {{$story->course }}{{' Total Earnings:'}} {{$story->total_earnings}}
                                            </p>

                                            <h3 class="sub2-title">{{$story->name}} </h3>
                                            {!! $story->description !!}
                                        </div>
                                    </div>
                                </div>
                                 @endif
                                 @if($i==4)
                                <div class="grid-item-comm story4 course-overlay">
                                    <div class="img-person"> <img src="{{asset($story->featured_image)}}"></div>
                                    <div class="course-detail po-ab">
                                        <h3 class="sub2-title">{{$story->name}}</h3>
                                         {!! $story->description !!}

                                        <h3 class="sub2-title">Course</h3>
                                        <p>{{$story->course }}{{' Total Earnings:'}} {{$story->total_earnings}}  </p>
                                    </div>
                                </div>
                                 @endif
                                 @if($i==5)
                                <div class="grid-item-comm story5">
                                    <div class="d-flex flex-wrap-mob">
                                        <div class="img-person w-50 w-m-100">
                                            <img src="{{asset($story->featured_image)}}">
                                        </div>
                                        <div class="course-detail w-50 w-m-100">
                                            <h3 class="sub2-title">Course</h3>
                                            <p>
                                              {{$story->course }}{{' Total Earnings:'}} {{$story->total_earnings}}
                                            </p>

                                            <h3 class="sub2-title">{{$story->name}}</h3>
                                            {!! $story->description !!}
                                        </div>
                                    </div>
                                </div>
                                 @php
                                $i=0;
                                @endphp
                                @endif
                                @php
                                $i++;
                                @endphp
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="Our_Initiatives" class="grey-background position-relative">
            <div class="left-line"></div>
            <div class="landing-container">
                <div class="left-pointer"></div>
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="main-title mg-bot-title green-title orange-line-bottom">Sustainability </h2>

                    </div>
                </div>


               <div class="row ">
                   <div class="col-md-12">
                       <div class="grid-container-sustain">
                        @php
                            $k=1
                            @endphp
                        @foreach(json_decode($impact->sustainability,true) as $ss)
                           <div class="single-sustain-block si{{$k}} anim-fr-bot">
                               <div class="sustain-content">
                                   <span>{{$ss['title']}}</span>
                               </div>
                               <div class="sustain-icon">
                                   <img src="{{asset($ss['featured_image'])}}">
                               </div>
                           </div>
                           @php
                           $k++;
                           @endphp
                           @endforeach
                          <!--  <div class="single-sustain-block si2 anim-fr-bot">
                               <div class="sustain-content">
                                   <span>Barrier-free design for the comfort of all the occupants</span>
                               </div>
                               <div class="sustain-icon">
                                   <img src="images/sustainability_icons/i2.png">
                               </div>
                           </div>
                           <div class="single-sustain-block si3 anim-fr-bot">
                               <div class="sustain-content">
                                   <span>On-site sewage treatment plant (STP) catering to 100% requirement of water for flushing, landscaping and cooling tower</span>
                               </div>
                               <div class="sustain-icon">
                                   <img src="images/sustainability_icons/i3.png">
                               </div>
                           </div>
                           <div class="single-sustain-block si4 anim-fr-bot">
                               <div class="sustain-content">
                                   <span>Building constructed with 100% STP water</span>
                               </div>
                               <div class="sustain-icon">
                                   <img src="images/sustainability_icons/i4.png">
                               </div>
                           </div>
                           <div class="single-sustain-block si5 anim-fr-bot">
                               <div class="sustain-content">
                                   <span>Charging facilities for 5% of the total parking spaces</span>
                               </div>
                               <div class="sustain-icon">
                                   <img src="images/sustainability_icons/i5.png">
                               </div>
                           </div>
                           <div class="single-sustain-block si6 anim-fr-bot">
                               <div class="sustain-content">
                                   <span>The entire exposed roof (after deducting services and vegetation) covered with high SRI (solar reflectance index) materials, helping the building to stay relatively cool even under a burning sun</span>
                               </div>
                               <div class="sustain-icon">
                                   <img src="images/sustainability_icons/i6.png">
                               </div>
                           </div>
                           <div class="single-sustain-block si7 anim-fr-bot">
                               <div class="sustain-content">
                                   <span>Outdoor light pollution reduced by 70%</span>
                               </div>
                               <div class="sustain-icon">
                                   <img src="images/sustainability_icons/i7.png">
                               </div>
                           </div>
                           <div class="single-sustain-block si8 anim-fr-bot">
                               <div class="sustain-content">
                                   <span>Annual energy saving of 30.19% against ASHRAE 90.1-2010</span>
                               </div>
                               <div class="sustain-icon">
                                   <img src="images/sustainability_icons/i8.png">
                               </div>
                           </div>

                           <div class="single-sustain-block si9 anim-fr-bot">
                               <div class="sustain-content">
                                   <span>Energy-efficient LED lighting used for common interior areas</span>
                               </div>
                               <div class="sustain-icon">
                                   <img src="images/sustainability_icons/i9.png">
                               </div>
                           </div>

                           <div class="single-sustain-block si10 anim-fr-bot">
                               <div class="sustain-content">
                                   <span>Efficient construction materials used in the project, with a combined recycled content such as reinforcement, concrete, reinforced concrete, block work, glazing, insulation, tiles, etc.</span>
                               </div>
                               <div class="sustain-icon">
                                   <img src="images/sustainability_icons/i10.png">
                               </div>
                           </div>

                           <div class="single-sustain-block si11 anim-fr-bot">
                               <div class="sustain-content">
                                   <span>Construction material sourced within a radius of 400 km, including reinforcement, concrete, block work, gypsum board, insulation, etc.</span>
                               </div>
                               <div class="sustain-icon">
                                   <img src="images/sustainability_icons/i11.png">
                               </div>
                           </div> -->

                       </div>

                       <div class="top-space-80 mob-space-30"></div>
                   </div>
                </div> 
            </div>
        </section>

    @endsection
    @section('scripts')

       
@endsection
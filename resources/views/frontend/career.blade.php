
@extends('layouts.app')
@section('title')
{{$career->meta_title}}
@endsection
@section('keyword')
{{ $career->meta_keyword}}
@endsection
@section('description')
{{$career->meta_description}}
@endsection
@section('content')
@section('content')
   <!-- banner section start -->      
      <div id="" class="inner-banner-section career-banner" style="background-image: url({{$career->desktop_banner}}) !important;">               
        
        <div class="container-fluid nopadding">
          <div class="banner-title">
            <h1 class="hero-title inner-page-title animated-inter-title">Careers</h1>
          </div>            
        </div>  
      </div>
      <!-- banner section end -->

     


      <section class="section-space grey-background position-relative">
        <div class="left-line"></div>
        <div class="landing-container">
         <div class="left-pointer"></div>              
            <div class="row">
              <div class="col-md-12">                
                <h2 class="main-title mg-bot-title  orange-line-bottom">Join the Nidar Group and fast-track your professional graph</h2>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
               <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12 nopadding form-block-partner">
              
                <form action="javascript:void(0);" id="careerform" enctype="multipart/form-data">
                      
                      <!--  Contact Person Details -->
                      <div class="form-group two-feild">                       

                        <div class="col-md-12 nopadding "> 
                           <div class="col-md-6 nopadding">
                            <div class="controls">
                            <input type="text" id="career_first_name" class="floatLabel form-control" name=" " required>
                            <label for="name">First Name*</label>
                          </div>
                          </div>
                          <div class="col-md-6 nopadding f-pd-left">
                             <div class="controls">
                            <input type="text" id="career_last_name" class="floatLabel form-control" name="career_last_name" required>
                            <label for="name">Last Name*</label>
                          </div>
                          </div>
                        </div>
                      </div>

                      <div class="form-group two-feild">
                          <div class="col-md-12 nopadding"> 
                            <div class="col-md-6 nopadding ">
                             <div class="controls">
                            <input type="email" id="career_email" class="floatLabel form-control" name="career_email" required>
                            <label for="email">E-mail ID*</label>
                          </div>
                          </div>
                           <div class="col-md-6 nopadding f-pd-left">
                             <div class="controls">
                              <input type="tel" id="career_phone" class="floatLabel form-control" name="career_phone" required>
                              <label for="email">Phone Number*</label>
                            </div>
                          </div>
                          
                        </div>
                      </div>


                      <div class="clearfix"> </div>

                       <!--  Corporate Address -->
                      <div class="form-group ">                        
                        <div class="controls">
                          <textarea name="career_message" class="floatLabel form-control" id="career_message"></textarea>
                          <label for="add1">Message</label>                        
                        </div>

                      </div>

                        

                     

                    <div class="clearfix"> </div>


                      <div class="col-md-12 nopadding">  

                        <div class="col-md-6 nopadding">
                          <div class="upload-btn-wrapper">
                            <button class="btn btn-all green-upload-btn"><img src="{{asset('assets/images/upload.png')}}"> Upload  your CV</button>
                            <input type="file" name="myfile" id="myfile" onchange="validate_fileupload(this);" required class="form-control" / >
                             <div id="file-upload-filename"></div>
                                <div id="resume_error"></div>
                          </div>
                        </div>
                        <div class="col-md-4">
                           <input type="submit" name="" id="careersubmit" class="btn btn-all submit-btn" value="Submit"><br>
                            <span id="success" class="success"></span>
                            <span id="danger" class="danger"></span>
                        </div>                     
                      
                      </div>
                     
                    </form>

                
                  

               </div>

               </div>

            </div>

        </div>
      </section>


       <!-- Overview section start-->
      <section id="" class="green-background section-space position-relative"> 
          <div class="left-line"></div>
          <div class="landing-container"> 
          <div class="left-pointer"></div>           
            <div class="row">
               

              <div class="col-md-12">
                <div class="d-flex wfh-big flex-wrap-mob">
                    <div class="wfh-big-left d-flex flex-column w-50 w-m-100">
                      <!-- left top -->
                      <div>
                      <h2 class="main-title white-color mg-bot-title orange-line-bottom">Work Culture</h2>
                      <!-- left-bot -->
                      {!! $career->work_culture_description !!}
                      </div>
                      <div class="w-cl-b">
                        <img src="{{asset($career->work_culture_image1)}}">
                      </div>
                    </div>
                    <div class="wfh-big-right d-flex  w-50 w-m-100">
                      <div class="d-flex flex-column w-cl">
                        <div class="w-clt">
                          <img src="{{asset($career->work_culture_image2)}}">
                        </div>
                        <div class="w-clb">
                          <img src="{{asset($career->work_culture_image4)}}">                          
                        </div>
                      </div>

                      <div class="w-cr">
                          <img src="{{asset($career->work_culture_image3)}}">                          
                        
                      </div>

                      <!-- right-bot -->
                      
                    </div>
                </div>
              </div>  

                      
            </div>
          </div> 
      </section>
      <!-- Overview section end-->

      <section id="" class="grey-background opprtunity-background section-space position-relative banner-overlay" style="background-image: url({{$career->equal_opportunity_employer_background_image}}) !important;"> 
          <div class="left-line"></div>
          <div class="landing-container"> 
          <div class="left-pointer"></div>           
            <div class="row">
              <div class="col-md-12">
                  <h2 class="main-title white-color mg-bot-title orange-line-bottom">Equal opportunity employer</h2>               
              </div> 


            </div>
          </div>
            <a href="javascript:void(0);" class="opprtunity-content arrow-anim">
                {!! $career->equal_opportunity_employer_description!!}
              </a>
    </section>
       
    @endsection
    @section('scripts')

<script type="text/javascript">
        $(document).ready(function() {
            function floatLabel(inputType) {
                $(inputType).each(function() {
                    var $this = $(this);
                    // on focus add cladd active to label
                    $this.focus(function() {
                        $this.next().addClass("active");
                    });
                    //on blur check field and remove class if needed
                    $this.blur(function() {
                        if ($this.val() === '' || $this.val() === 'blank') {
                            $this.next().removeClass();
                        }
                    });
                });
            }
            // just add a class of "floatLabel to the input field!"
            floatLabel(".floatLabel");

          $("#contact_form").submit(function(e) {

            var contact_first_name = $('#contact_first_name').val();
            var contact_last_name= $('#contact_last_name').val();
           var contact_email= $('#contact_email').val();
           var contact_phone= $('#contact_phone').val();
           var contact_msg= $('#contact_msg').val();
        
                var flag = 0;

                if (contact_first_name == null || contact_first_name == '') {
                    $('#contact_first_name').addClass('has-error');
                    flag++;
                } else {
                    $("#contact_first_name").removeClass('has-error');
                }
                if (contact_last_name == null || contact_last_name == '') {
                    $('#contact_last_name').addClass('has-error');
                    flag++;
                } else {
                    $("#contact_last_name").removeClass('has-error');
                }
                if (contact_email == '' || contact_email == '') {
                    $("#contact_email").addClass('has-error');

                    flag++;
                } else {
                    $("#contact_email").addClass('has-error');
                }
                if (contact_phone == null || contact_phone == '') {
                    $("#contact_phone").addClass('has-error');
                    flag++;
                } else {
                    $("#contact_phone").removeClass('error');

                }


                // if(mobile.length<10||mobile.length>10) {
                //     $("#mobile").addClass('error');
                //     $('#mobile_error').html('mobile is not valid');
                //     flag++;
                // }
                // else{
                //     $("#mobile").removeClass('error');
                //     $('#mobile_error').html('');
                //
                // }


                if (flag > 0) {
                    e.preventDefault();
                    return false;
                } else {
                    $("#contactsubmit").val('Please Wait...');
                    var form_data = new FormData();
                    form_data.append('contact_first_name', contact_first_name);
                    form_data.append('contact_last_name', contact_last_name);
                    form_data.append('contact_email', contact_email);
                    form_data.append('contact_phone', contact_phone);
                    form_data.append('contact_msg', contact_msg);
                    $.ajax({
                        type: "POST",
                        url: 'ajax_submit_contact_page_form',
                        dataType: 'text', // what to expect back from the PHP script, if anything
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        success: function(data) {
                            if (data == 1) {
                                $("#contactsubmit").val('Submit');
                                $('#contact_form').trigger('reset');
                                $('#success').html('Thank you for Enquiry!');
                                 $('#danger').html('');
                                 setTimeout(function () { location.reload(1); }, 3000);
                            } else {
                                $("#contactsubmit").val('Submit');
                                $('#danger').html('Something went wrong');
                            }
                        }
                    });
                }

            });
        });

    </script>
@endsection
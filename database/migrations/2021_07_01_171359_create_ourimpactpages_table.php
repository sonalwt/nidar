<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOurimpactpagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ourimpactpages', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('desktop_banner')->nullable();
            $table->string('mobile_banner')->nullable();
            $table->text('description')->nullable();
            $table->text('the_cummunity_description')->nullable();
            $table->text('csr_initiatives_description')->nullable();
            $table->text('skill_development_part1')->nullable();
            $table->text('skill_development_part2')->nullable();
            $table->string('meta_title')->nullable();
            $table->string('meta_keyword')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_image')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ourimpactpages');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAboutuspagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aboutuspages', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('desktop_banner')->nullable();
            $table->string('mobile_banner')->nullable();
            $table->text('overview_part_1')->nullable();
            $table->text('overview_part_2')->nullable();
            $table->text('our_legacy_description')->nullable();
            $table->string('our_legacy_backgorund_image')->nullable();
            $table->text('key_strength_and_differentiators_main_description')->nullable();
            $table->string('key_strength_and_differentiators_image1')->nullable();
            $table->string('key_strength_and_differentiators_title1')->nullable();
            $table->text('key_strength_and_differentiators_description1')->nullable();
            $table->string('key_strength_and_differentiators_image2')->nullable();
            $table->string('key_strength_and_differentiators_title2')->nullable();
            $table->text('key_strength_and_differentiators_description2')->nullable();
            $table->string('key_strength_and_differentiators_image3')->nullable();
            $table->string('key_strength_and_differentiators_title3')->nullable();
            $table->text('key_strength_and_differentiators_description3')->nullable();
            $table->string('key_strength_and_differentiators_image4')->nullable();
            $table->string('key_strength_and_differentiators_title4')->nullable();
            $table->text('key_strength_and_differentiators_description4')->nullable();
            $table->string('meta_title')->nullable();
            $table->string('meta_keyword')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_image')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('aboutuspages');
    }
}

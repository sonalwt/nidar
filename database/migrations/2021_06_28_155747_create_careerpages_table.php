<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCareerpagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('careerpages', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('desktop_banner')->nullable();
            $table->string('mobile_banner')->nullable();
            $table->string('work_culture_image1')->nullable();
            $table->string('work_culture_image2')->nullable();
            $table->string('work_culture_image3')->nullable();
            $table->string('work_culture_image4')->nullable();
            $table->text('work_culture_description')->nullable();
            $table->string('equal_opportunity_employer_background_image')->nullable();
            $table->text('equal_opportunity_employer_description')->nullable();
            $table->string('meta_title')->nullable();
            $table->string('meta_keyword')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_image')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('careerpages');
    }
}

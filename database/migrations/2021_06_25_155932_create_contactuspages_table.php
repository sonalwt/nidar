<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContactuspagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contactuspages', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('desktop_banner')->nullable();
            $table->string('mobile_banner')->nullable();
            $table->string('india_image')->nullable();
            $table->text('india_address')->nullable();
            $table->string('india_mobile')->nullable();
            $table->string('uae_image')->nullable();
            $table->text('uae_address')->nullable();
            $table->string('uae_mobile')->nullable();
            $table->text('map')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contactuspages');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHomepagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homepages', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('banner')->nullable();
            $table->string('news1')->nullable();
            $table->string('news2')->nullable();
            $table->string('news3')->nullable();
            $table->text('equal_opportunity_employer_description')->nullable();
            $table->string('equal_opportunity_employer_image')->nullable();
            $table->text('careers_descriptione')->nullable();
            $table->string('careers_image')->nullable();
            $table->string('current_openings_image')->nullable();
            $table->string('meta_title')->nullable();
            $table->string('meta_keyword')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_image')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('homepages');
    }
}

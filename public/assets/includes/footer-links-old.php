<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/main.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/customGsap.js"></script>
<script src="js/gsap.min.js"></script>
<script src="js/scrollMagic.js"></script>
<script src="js/TweenMax.min.js"></script>
<script src="js/animation.gsap.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/owl.carousel.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
           
          $("#subscribe_form").submit(function(e) {
            var subscribe_email = $('#subscribe_email').val();  
                    $("#subbtn").val('Please Wait...');
                    var form_data = new FormData();
                    form_data.append('subscribe_email', subscribe_email);
                    $.ajax({
                        type: "POST",
                        url: 'includes/ajax_submit_subscribe_page_form.php',
                        dataType: 'text', // what to expect back from the PHP script, if anything
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        success: function(data) {
                            if (data == 1) {
                                $("#subbtn").val('Submit');
                                $('#subscribe_form').trigger('reset');
                                $('#successsub').html('Thank you for submitting your resume!')
                                 $('#dangersub').html('');
                            } else {
                                $("#contact_submit").val('Submit');
                                $('#dangersub').html('Something went wrong')
                            }
                        }
                    });
                
            });
           
         
         });
</script>

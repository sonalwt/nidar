<!-- preloader -->
<div id="preloader">
 <div class="loader-block">
  <img src="images/logo.png" class="img-responsive logo" alt="logo">
<div class="loading-dots">
  <div class="loading-dots--dot"></div>
  <div class="loading-dots--dot"></div>
  <div class="loading-dots--dot"></div>
</div>
</div> 
</div>
<!-- preloader end -->

     <header id="header">
         <div class="container-fluid nopadding">
            <nav id="main-navbar" class="navbar navbar-fixed-top">
               <div class="container-fluid nopadding">
                  <div class="d-flex flex-justify-center flex-align-center d-block-mob menu-mob">
                     <div class="navbar-header">
                        <a class="logo" href="index.php"><img src="images/logo.png" class="img-responsive logo" alt="logo"></a>
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>

                        <!-- <a href="javascript:void(0);" class="search-icon search-icon-mob visible-xs"><img src="images/search.png"></a> -->
                     </div>
                     <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav menu-nav">
                           <li class="active"><a href="index.php">Home</a></li>                        
                           
                           <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">About Us
                                <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                 <li><a href="about.php">About Us</a></li>
                                  <li><a href="business-overview.php">Business Overview</a></li>
                                  <li><a href="business-verticals.php">Business Verticals</a></li>
                                  <!-- <li><a href="our-brands.php">Our Brands</a></li> -->
                                   <li><a href="leader-team.php">Our Leadership</a></li>
                                 
                                </ul>
                            </li>                        
                           <!-- <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">Our Beliefs
                                <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                  <li><a href="our-belief.php">The Community</a></li>
                                  <li><a href="our-belief.php">Our Initiatives</a></li>
                                  <li><a href="our-belief.php">CSR</a></li>
                                 
                                </ul>
                            </li> -->

                          <li><a href="our-brands.php">Our Brands</a></li>
                           <li><a href="our-impact.php">Our Impact</a></li> 
                         <!--   <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">We, the people
                                <span class="caret"></span></a>
                                <ul class="dropdown-menu">                                  
                                  <li><a href="leader-team.php">The Leadership Team</a></li>                               
                                </ul>
                              </li> -->


                            <!-- <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">Newsroom
                                <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                  <li><a href="javascript:void(0);">Press Releases</a></li>
                                  <li><a href="javascript:void(0);">Newsletters</a></li>
                                  <li><a href="javascript:void(0);">Media Resources</a></li>
                                
                                </ul>
                            </li> -->
                           <li><a href="newsroom.php">Newsroom</a></li>                        
                           <li><a href="career.php">Careers</a></li>                        

                          <!--   <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">Careers
                                <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                  <li><a href="current-opening.php">Current openings</a></li>
                                  <li><a href="">Working from home</a></li>
                                  <li><a href="">Equal opportunity employer</a></li>                                
                                
                                </ul>
                            </li> -->
                             <!-- <li><a href="partner-with-us.php">Partner with us</a></li> -->
                           <!--  <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">Partner with us
                                <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                  <li><a href="partner-with-us.php">Outline</a></li>
                                  <li><a href="partner-with-us.php">Vendor Lead Capture Form</a></li>
                                  <li><a href="partner-with-us.php">Location Map</a></li>                                 
                                
                                </ul>
                            </li> -->
                         
                        </ul>
                        <ul class="nav d-flex navbar-right flex-align-center">                          
                           <li><a href="contact-us.php" class="btn btn-all btn-orange btn-contact">Contact us</a></li>
                          <!--   <li><a href="javascript:void(0);" class="search-icon search-icon-desk hidden-xs"><img src="images/search.png"></a></li> -->
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="clearfix"></div>
            </nav>
         </div>
      </header>
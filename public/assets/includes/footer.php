 <div id="footer" class="footer"> 

      <div class="d-flex flex-align-center flex-wrap-mob">
         <div class="footer-left">
         <div class="col-md-12 pd-right-0">
            <div class="col-md-2 col-sm-6 col-xs-6">
               <ul class="footer-links">
                  <li><a href="javascript:void(0);">Sitemap</a></li>
                  <li><a href="career.php">Careers</a></li>                  
               </ul>
            </div>

            <div class="col-md-2 col-sm-6 col-xs-6">
               <ul class="footer-links">
                  <li><a href="newsroom.php">Newsroom </a></li>
                  <li><a href="privacy-policy.php">Privacy Policy</a></li>                 
               </ul>
            </div>

            <div class="col-md-2 col-sm-6 col-xs-6">
               <ul class="footer-links">
                  <li><a href="disclaimer.php">Legal Disclaimer</a></li>                
               </ul>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
               <p class="subscribe-text">Subscribe to Nidar Group via Email</p>
               <form action="javascript:void(0);" id="subscribe_form">
               <div class="d-flex">
                  <input type="email" name="subscribe_email" id="subscribe_email" class="form-control" placeholder="Email Address" required><input type="submit" id="subbtn" name="subbtn" class="btn btn-submit" value="SUBSCRIBE">
                  <br>
                  
               </div>   
               <span id="successsub" class="success"></span>
               <span id="dangersub" class="danger"></span>           
            </div>            
            
            <div class="col-md-2 col-sm-6 col-xs-12 text-center nopadding">              
               <ul class="social-links">
                 <li><a href=""><i class="fa fa-instagram"></i></a></li>
                  <li><a href=""><i class="fa fa-twitter"></i></a></li>
                  <li><a href=""><i class="fa fa-facebook"></i></a></li>
                 
                 
               </ul>
            </div>
            <div class="clearfix"></div>          
         </div>
         </div>
         <div class="back-top-block nopadding">
           
              <a class="button back-to-topbtn" href="javascript:void(0);"> <img src="images/right.png" class="img-responsive"></a>
                          
           
         </div>
          </div>



      </div>
$(window).on('load', function () {
	// on scroll animation starts title
    var lastScrollTop = 0;
   	var controller = new ScrollMagic.Controller();
    
    var revealElements1 = document.getElementsByClassName("main-title");
		for (var i=0; i<revealElements1.length; i++) { 
			new ScrollMagic.Scene({
							triggerElement: revealElements1[i], 
							offset: 50,												
							triggerHook: 0.85,
					})
							.setClassToggle(revealElements1[i], "fadeout")
							
							.addTo(controller);

	}
   var revealElementorLine = document.getElementsByClassName("orange-line-bottom");
		for (var i=0; i<revealElementorLine.length; i++) { 
			new ScrollMagic.Scene({
							triggerElement: revealElementorLine[i], 
							offset: 50,												
							triggerHook: 0.85,
					})
							.setClassToggle(revealElementorLine[i], "slide")
							
							.addTo(controller);

	}

	  var revealElementorLine = document.getElementsByClassName("left-pointer");
		for (var i=0; i<revealElementorLine.length; i++) { 
			new ScrollMagic.Scene({
							triggerElement: revealElementorLine[i], 
							offset: 50,												
							triggerHook: 0.85,
					})
							.setClassToggle(revealElementorLine[i], "popup")
							
							.addTo(controller);

	}

	var revealElementorLine = document.getElementsByClassName("line-htl-or");
		for (var i=0; i<revealElementorLine.length; i++) { 
			new ScrollMagic.Scene({
							triggerElement: revealElementorLine[i], 
							offset: 50,												
							triggerHook: 0.85,
					})
							.setClassToggle(revealElementorLine[i], "slide")
							
							.addTo(controller);

	}

	var revealElementorLine = document.getElementsByClassName("slide-to-left");
		for (var i=0; i<revealElementorLine.length; i++) { 
			new ScrollMagic.Scene({
							triggerElement: revealElementorLine[i], 
							offset: 60,												
							triggerHook: 0.85,
					})
							.setClassToggle(revealElementorLine[i], "slide-to")
							
							.addTo(controller);

	}

	   // text-animation
	 var revealElements2 = document.getElementsByClassName("news-wrapper");
		for (var i=0; i<revealElements2.length; i++) { // create a scene for each element
			new ScrollMagic.Scene({
							triggerElement: revealElements2[i], // y value not modified, so we can use element as trigger as well
							offset: 50,												 // start a little later
							triggerHook: 0.85,
					})
							.setClassToggle(revealElements2[i], "move-news") // add class toggle
							// .addIndicators({name: "digit " + (i+1) }) // add indicators (requires plugin)
							.addTo(controller);

						}

	 // text-animation
	 var revealElements3 = document.getElementsByClassName("animated-text");
		for (var i=0; i<revealElements3.length; i++) { // create a scene for each element
			new ScrollMagic.Scene({
							triggerElement: revealElements3[i], // y value not modified, so we can use element as trigger as well
							offset: 50,												 // start a little later
							triggerHook: 0.85,
					})
							.setClassToggle(revealElements3[i], "move-text") // add class toggle
							// .addIndicators({name: "digit " + (i+1) }) // add indicators (requires plugin)
							.addTo(controller);

						}


		// text-animation
	 var revealElements4 = document.getElementsByClassName("verticle-page-title");
		for (var i=0; i<revealElements4.length; i++) { // create a scene for each element
			new ScrollMagic.Scene({
							triggerElement: revealElements4[i], // y value not modified, so we can use element as trigger as well
							offset: 50,												 // start a little later
							triggerHook: 0.85,
					})
							.setClassToggle(revealElements4[i], "move-bot") // add class toggle
							// .addIndicators({name: "digit " + (i+1) }) // add indicators (requires plugin)
							.addTo(controller);

						}

		// text-animation
	 var revealElements5 = document.getElementsByClassName("animated-inter-title");
		for (var i=0; i<revealElements5.length; i++) { // create a scene for each element
			new ScrollMagic.Scene({
							triggerElement: revealElements5[i], // y value not modified, so we can use element as trigger as well
							offset: 50,												 // start a little later
							triggerHook: 0.85,
					})
							.setClassToggle(revealElements5[i], "move-title") // add class toggle
							// .addIndicators({name: "digit " + (i+1) }) // add indicators (requires plugin)
							.addTo(controller);

						}


	 // text-animation
	 var revealGal = document.getElementsByClassName("gal-anim");
		for (var i=0; i<revealGal.length; i++) { // create a scene for each element
			new ScrollMagic.Scene({
							triggerElement: revealGal[i], // y value not modified, so we can use element as trigger as well
							offset: 50,												 // start a little later
							triggerHook: 0.75,
					})
							.setClassToggle(revealGal[i], "gal-move") // add class toggle
							// .addIndicators({name: "digit " + (i+1) }) // add indicators (requires plugin)
							.addTo(controller);

						}
	

	var revealblocksS = document.getElementsByClassName("anim-fr-bot");
		for (var i=0; i<revealblocksS.length; i++) { // create a scene for each element
			new ScrollMagic.Scene({
							triggerElement: revealblocksS[i], // y value not modified, so we can use element as trigger as well
							offset: 50,												 // start a little later
							triggerHook: 0.75,
					})
							.setClassToggle(revealblocksS[i], "anim-fr-bot-move") // add class toggle
							// .addIndicators({name: "digit " + (i+1) }) // add indicators (requires plugin)
							.addTo(controller);

						}





});
$(window).on('load', function () {
    $('#preloader').fadeOut();
});
$(document).ready(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() >= 100) { // If page is scrolled more than 50px
            $('.back-to-topbtn').fadeIn(200); // Fade in the arrow

        } else {
            $('.back-to-topbtn').fadeOut(200); // Else fade out the arrow

        }
    });

    $(".back-to-topbtn").click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 1000);
        setTimeout(function() { $('#main-navbar').removeClass('trans-header'); },1000); 
    }); 



    // Home page js
       // Home slider-anim
             // var dragging = true;
             
             $('#owl-main').owlCarousel({                
                autoplay: true,
                // autoplayTimeout: 2500,
                autoplaySpeed:1500,
                loop:true,
                dots: false,
                items: 1,
                smartSpeed: 500,  
                 animateOut: 'fadeOut',
                responsive:{
                  0:{
                      items:1,
                       nav: false,
                  },
                  600:{   
                      nav: true,                   
                       navText: ["<img src='assets/images/leftAarrow.png'>", "<img src='assets/images/rightAarrow.png'>"],
                  }
                  
              }             
              });                     
          
           // home slider-anim end

           // news slider-anim
            $('#news-slider-home').owlCarousel({
              loop:true,
              margin:10,autoplay: true,
              nav: true,  
              smartSpeed:500,           
              dots:false,
              items:1,
              addClassActive: true,
              mouseDrag:false,
              touchDrag:false,
              onInitialized:counter, //When the plugin has initialized.
              onTranslated:counter,
              navText: ["<img src='assets/images/left.png'>", "<img src='assets/images/right.png'>"],
            });

            function counter(event) {
                var element   = event.target;         // DOM element, in this example .owl-carousel
                var items     = event.item.count;     // Number of items
                var item      = event.item.index - 1;     // Position of the current item

              // it loop is true then reset counter from 1
              if(item > items) {
                item = item - items
              }
              $('.counter-news').html(+item+" / "+items)
            }
            // news slide end

            // Brands new slider
                var bigimage = $("#big");
                var thumbs = $("#thumbs");
                //var totalslides = 10;
                var syncedSecondary = true;

                bigimage
                  .owlCarousel({
                    items: 1,
                    slideSpeed: 2000,
                    smartSpeed: 300,                    
                    nav: false,
                    autoplay: true,
                    dots: false,
                    loop: true,
                    animateOut: 'fadeOut',
                    // responsiveRefreshRate: 200,
                   
                  })
                  .on("changed.owl.carousel", syncPosition);

                thumbs
                  .on("initialized.owl.carousel", function () {
                    thumbs.find(".owl-item").eq(0).addClass("current");
                  })
                  .owlCarousel({
                    items: 6,
                    dots: false,
                    nav: true,                  
                    smartSpeed: 300,
                    // slideSpeed: 500,
                    slideBy: 6,
                    mouseDrag:false,
                    touchDrag:false,
                    // responsiveRefreshRate: 100
                  })
                  .on("changed.owl.carousel", syncPosition2);

                function syncPosition(el) {
                  //if loop is set to false, then you have to uncomment the next line
                  //var current = el.item.index;

                  //to disable loop, comment this block
                  var count = el.item.count - 1;
                  var current = Math.round(el.item.index - el.item.count / 2 - 0.5);

                  if (current < 0) {
                    current = count;
                  }
                  if (current > count) {
                    current = 0;
                  }
                  //to this
                  thumbs
                    .find(".owl-item")
                    .removeClass("current")
                    .eq(current)
                    .addClass("current");
                  var onscreen = thumbs.find(".owl-item.active").length - 1;
                  var start = thumbs.find(".owl-item.active").first().index();
                  var end = thumbs.find(".owl-item.active").last().index();

                  if (current > end) {
                    thumbs.data("owl.carousel").to(current, 100, true);
                  }
                  if (current < start) {
                    thumbs.data("owl.carousel").to(current - onscreen, 100, true);
                  }
                }

                function syncPosition2(el) {
                  if (syncedSecondary) {
                    var number = el.item.index;
                    bigimage.data("owl.carousel").to(number, 100, true);
                  }
                }

                thumbs.on("click", ".owl-item", function (e) {
                  e.preventDefault();
                  var number = $(this).index();
                  bigimage.data("owl.carousel").to(number, 300, true);
                });
                // brand slider end  
});

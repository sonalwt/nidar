<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
class Contactform extends Model
{
        use SoftDeletes;
        /* The database table used by the model.
        *
        * @var string
        */
       
       protected $table = 'contactformleads';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['contact_first_name','contact_last_name','contact_email','contact_phone','contact_msg'];

    
}

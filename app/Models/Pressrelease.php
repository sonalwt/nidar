<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
class Pressrelease extends Model
{
        use SoftDeletes;
        /* The database table used by the model.
        *
        * @var string
        */
        use  HasRoles;
       protected $table = 'pressreleases';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'image_url', 'news_link', 'release_date', 'description', 'status','business_vertical','mostly_viewed_status','recent_article_status'];
     public function buisinessvertical(){
        return $this->belongsTo('App\Models\Buisinessvertical','business_vertical');
    }

    
}

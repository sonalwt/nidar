<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
class Ourimpactpage extends Model
{
        use SoftDeletes;
        /* The database table used by the model.
        *
        * @var string
        */
        use  HasRoles;
       protected $table = 'ourimpactpages';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['desktop_banner', 'mobile_banner', 'description', 'the_cummunity_description', 'csr_initiatives_description', 'skill_development_part1', 'skill_development_part2', 'meta_title', 'meta_keyword', 'meta_description', 'meta_image','cummunity_img','coeareas','sustainability'];

    
}

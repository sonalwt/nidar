<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
class Aboutuspage extends Model
{
        use SoftDeletes;
        /* The database table used by the model.
        *
        * @var string
        */
        use  HasRoles;
       protected $table = 'aboutuspages';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['desktop_banner', 'mobile_banner', 'overview_part_1', 'overview_part_2', 'our_legacy_description', 'our_legacy_backgorund_image', 'key_strength_and_differentiators_main_description', 'key_strength_and_differentiators_image1', 'key_strength_and_differentiators_title1', 'key_strength_and_differentiators_description1', 'key_strength_and_differentiators_image2', 'key_strength_and_differentiators_title2', 'key_strength_and_differentiators_description2', 'key_strength_and_differentiators_image3', 'key_strength_and_differentiators_title3', 'key_strength_and_differentiators_description3', 'key_strength_and_differentiators_image4', 'key_strength_and_differentiators_title4', 'key_strength_and_differentiators_description4', 'meta_title', 'meta_keyword', 'meta_description', 'meta_image'];

    
}

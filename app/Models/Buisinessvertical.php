<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
class Buisinessvertical extends Model
{
        use SoftDeletes;
        /* The database table used by the model.
        *
        * @var string
        */
        use  HasRoles;
       protected $table = 'buisinessverticals';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['featured_image', 'title', 'status','icon','status_about_us','status_newsroom_press_page','status_newsroom_video_page','subsectors','description','image2','image','status_buisiness_vertical','description_industry_sub_sector','fast_facts','fast_facts_description'];
     public function brands(){
        return $this->hasMany('App\Models\Brand','buisiness_vertical_id');
    }
     public function videocorners(){
        return $this->hasMany('App\Models\Videocorner','business_vertical');
    }
    public function pressreleases(){
        return $this->hasMany('App\Models\Pressrelease','business_vertical');
    }
    
}

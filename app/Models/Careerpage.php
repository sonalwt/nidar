<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
class Careerpage extends Model
{
        use SoftDeletes;
        /* The database table used by the model.
        *
        * @var string
        */
        use  HasRoles;
       protected $table = 'careerpages';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['desktop_banner', 'mobile_banner', 'work_culture_image1', 'work_culture_image2', 'work_culture_image3', 'work_culture_image4', 'work_culture_description', 'equal_opportunity_employer_background_image', 'equal_opportunity_employer_description', 'meta_title', 'meta_keyword', 'meta_description', 'meta_image'];

    
}

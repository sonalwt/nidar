<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
class Homepage extends Model
{
        use SoftDeletes;
        /* The database table used by the model.
        *
        * @var string
        */
        use  HasRoles;
       protected $table = 'homepages';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
     protected $fillable = ['banner', 'news1', 'news2', 'news3', 'work_culture_description', 'work_culture_image','equal_opportunity_employer_description', 'equal_opportunity_employer_image', 'careers_descriptione', 'careers_image', 'current_openings_image', 'meta_title', 'meta_keyword', 'meta_description', 'meta_image', 'news1for', 'news2image', 'news2for', 'news3for', 'work_culture_image', 'work_culture_description','news1title'];


    
}

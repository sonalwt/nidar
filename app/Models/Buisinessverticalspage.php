<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
class Buisinessverticalspage extends Model
{
        use SoftDeletes;
        /* The database table used by the model.
        *
        * @var string
        */
        use  HasRoles;
       protected $table = 'buisinessverticalspages';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['desktop_banner', 'description', 'meta_title', 'meta_keyword', 'meta_description', 'meta_image'];

    
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
class Brand extends Model
{
        use SoftDeletes;
        /* The database table used by the model.
        *
        * @var string
        */
        use  HasRoles;
       protected $table = 'brands';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['buisiness_vertical_id','featured_image', 'brand_logo', 'title', 'short_description', 'description', 'sort_order', 'slug', 'meta_title', 'meta_keyword', 'meta_description', 'meta_image', 'status','featured_image_mob','home_page_image','home_page_image_webp','home_page_image_webp_mobile','home_page_image_mobile'];
public function vertical(){
        return $this->belongsTo('App\Models\Buisinessvertical','buisiness_vertical_id');
    }
    
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Milestone;
use Illuminate\Http\Request;
use App\Authorizable;

class MilestonesController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $milestones = Milestone::where('featured_image', 'LIKE', "%$keyword%")
                ->orWhere('title', 'LIKE', "%$keyword%")
                ->orWhere('year', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $milestones = Milestone::latest()->paginate($perPage);
        }

        return view('admin.milestones.index', compact('milestones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.milestones.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'year' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
          
         if(isset($request->mapping) && !empty($request->mapping)){
           // dd($request->prop);
          foreach($request->mapping as $key=>$map){
           
              if(isset($map['featured_image']) && !empty($map['featured_image'])){
                     $filename=date('hisdmy').$map['featured_image']->getClientOriginalName();
                   $map['featured_image']->move(base_path('public/assets/images/ourimpactpage'), $filename);
                   $raw[$key]['featured_image']='assets/images/ourimpactpage/'.$filename;
               }else{
                $raw[$key]['title']=$map['featured_image_original'];
               }
           $raw[$key]['title']= $map['title'];           
           
          }
          $requestData['title']=json_encode($raw);
        }

        Milestone::create($requestData);

        return redirect('admin/milestones')->with('flash_message', 'Milestone added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $milestone = Milestone::findOrFail($id);

        return view('admin.milestones.show', compact('milestone'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $milestone = Milestone::findOrFail($id);

        return view('admin.milestones.edit', compact('milestone'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'year' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
             
          if(isset($request->mapping) && !empty($request->mapping)){
           // dd($request->prop);
          foreach($request->mapping as $key=>$map){
           
              if(isset($map['featured_image']) && !empty($map['featured_image'])){
                     $filename=date('hisdmy').$map['featured_image']->getClientOriginalName();
                   $map['featured_image']->move(base_path('public/assets/images/ourimpactpage'), $filename);
                   $raw[$key]['featured_image']='assets/images/ourimpactpage/'.$filename;
               }else{
                $raw[$key]['featured_image']=$map['featured_image_original'];
               }
           $raw[$key]['title']= $map['title'];           
           
          }
          $requestData['title']=json_encode($raw);
        }
        $milestone = Milestone::findOrFail($id);
        $milestone->update($requestData);

        return redirect('admin/milestones')->with('flash_message', 'Milestone updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Milestone::destroy($id);

        return redirect('admin/milestones')->with('flash_message', 'Milestone deleted!');
    }
}

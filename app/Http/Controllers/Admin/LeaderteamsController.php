<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Leaderteam;
use Illuminate\Http\Request;
use App\Authorizable;

class LeaderteamsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $leaderteams = Leaderteam::where('desktop_banner', 'LIKE', "%$keyword%")
                ->orWhere('mobile_banner', 'LIKE', "%$keyword%")
                ->orWhere('team_page_description', 'LIKE', "%$keyword%")
                ->orWhere('meta_title', 'LIKE', "%$keyword%")
                ->orWhere('meta_keyword', 'LIKE', "%$keyword%")
                ->orWhere('meta_description', 'LIKE', "%$keyword%")
                ->orWhere('meta_image', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $leaderteams = Leaderteam::latest()->paginate($perPage);
        }

        return view('admin.leaderteams.index', compact('leaderteams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.leaderteams.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
          if ($request->hasFile('desktop_banner')) {
                $name = date('hismdy').$request->file('desktop_banner')->getClientOriginalName();
                 $request->file('desktop_banner')->move(public_path('assets/images/buisinessoverviews'), $name);
                 $requestData['desktop_banner'] = 'assets/images/buisinessoverviews/'.$name;
        }
        if ($request->hasFile('mobile_banner')) {
                $name = date('hismdy').$request->file('mobile_banner')->getClientOriginalName();
                 $request->file('mobile_banner')->move(public_path('assets/images/buisinessoverviews'), $name);
                 $requestData['mobile_banner'] = 'assets/images/buisinessoverviews/'.$name;
        }
        
        if ($request->hasFile('meta_image')) {
                $name = date('hismdy').$request->file('meta_image')->getClientOriginalName();
                 $request->file('meta_image')->move(public_path('assets/images/buisinessoverviews'), $name);
                 $requestData['meta_image'] = 'assets/images/buisinessoverviews/'.$name;
        }

        Leaderteam::create($requestData);

        return redirect('admin/leaderteams')->with('flash_message', 'Leaderteam added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $leaderteam = Leaderteam::findOrFail($id);

        return view('admin.leaderteams.show', compact('leaderteam'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $leaderteam = Leaderteam::findOrFail($id);

        return view('admin.leaderteams.edit', compact('leaderteam'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
         if ($request->hasFile('desktop_banner')) {
                $name = date('hismdy').$request->file('desktop_banner')->getClientOriginalName();
                 $request->file('desktop_banner')->move(public_path('assets/images/buisinessoverviews'), $name);
                 $requestData['desktop_banner'] = 'assets/images/buisinessoverviews/'.$name;
        }
        if ($request->hasFile('mobile_banner')) {
                $name = date('hismdy').$request->file('mobile_banner')->getClientOriginalName();
                 $request->file('mobile_banner')->move(public_path('assets/images/buisinessoverviews'), $name);
                 $requestData['mobile_banner'] = 'assets/images/buisinessoverviews/'.$name;
        }
        
        if ($request->hasFile('meta_image')) {
                $name = date('hismdy').$request->file('meta_image')->getClientOriginalName();
                 $request->file('meta_image')->move(public_path('assets/images/buisinessoverviews'), $name);
                 $requestData['meta_image'] = 'assets/images/buisinessoverviews/'.$name;
        }

        $leaderteam = Leaderteam::findOrFail($id);
        $leaderteam->update($requestData);

        return redirect('admin/leaderteams')->with('flash_message', 'Leader Team Page updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Leaderteam::destroy($id);

        return redirect('admin/leaderteams')->with('flash_message', 'Leaderteam deleted!');
    }
}

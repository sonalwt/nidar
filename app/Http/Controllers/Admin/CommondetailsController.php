<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Commondetail;
use Illuminate\Http\Request;
use App\Authorizable;

class CommondetailsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $commondetails = Commondetail::where('logo', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $commondetails = Commondetail::latest()->paginate($perPage);
        }

        return view('admin.commondetails.index', compact('commondetails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.commondetails.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
               if ($request->hasFile('logo')) {
                $name = date('hismdy').$request->file('logo')->getClientOriginalName();
                 $request->file('logo')->move(public_path('assets/images/Homepage'), $name);
                 $requestData['logo'] = 'assets/images/Homepage/'.$name;
        }

        Commondetail::create($requestData);

        return redirect('admin/commondetails')->with('flash_message', 'Commondetail added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $commondetail = Commondetail::findOrFail($id);

        return view('admin.commondetails.show', compact('commondetail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $commondetail = Commondetail::findOrFail($id);

        return view('admin.commondetails.edit', compact('commondetail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
         if ($request->hasFile('logo')) {
                $name = date('hismdy').$request->file('logo')->getClientOriginalName();
                 $request->file('logo')->move(public_path('assets/images/Homepage'), $name);
                 $requestData['logo'] = 'assets/images/Homepage/'.$name;
        }

        $commondetail = Commondetail::findOrFail($id);
        $commondetail->update($requestData);

        return redirect('admin/commondetails/1/edit')->with('flash_message', 'Common details updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Commondetail::destroy($id);

        return redirect('admin/commondetails')->with('flash_message', 'Commondetail deleted!');
    }
}

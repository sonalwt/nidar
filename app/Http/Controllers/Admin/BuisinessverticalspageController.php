<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Buisinessverticalspage;
use Illuminate\Http\Request;
use App\Authorizable;

class BuisinessverticalspageController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $buisinessverticalspage = Buisinessverticalspage::where('desktop_banner', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('meta_title', 'LIKE', "%$keyword%")
                ->orWhere('meta_keyword', 'LIKE', "%$keyword%")
                ->orWhere('meta_description', 'LIKE', "%$keyword%")
                ->orWhere('meta_image', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $buisinessverticalspage = Buisinessverticalspage::latest()->paginate($perPage);
        }

        return view('admin.buisinessverticalspage.index', compact('buisinessverticalspage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.buisinessverticalspage.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
         if ($request->hasFile('desktop_banner')) {
                $name = date('hismdy').$request->file('desktop_banner')->getClientOriginalName();
                 $request->file('desktop_banner')->move(public_path('assets/images/aboutuspage'), $name);
                 $requestData['desktop_banner'] = 'assets/images/aboutuspage/'.$name;
        }
      
        if ($request->hasFile('meta_image')) {
                $name = date('hismdy').$request->file('meta_image')->getClientOriginalName();
                 $request->file('meta_image')->move(public_path('assets/images/aboutuspage'), $name);
                 $requestData['meta_image'] = 'assets/images/aboutuspage/'.$name;
        }

        Buisinessverticalspage::create($requestData);

        return redirect('admin/buisinessverticalspage')->with('flash_message', 'Buisinessverticalspage added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $buisinessverticalspage = Buisinessverticalspage::findOrFail($id);

        return view('admin.buisinessverticalspage.show', compact('buisinessverticalspage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $buisinessverticalspage = Buisinessverticalspage::findOrFail($id);

        return view('admin.buisinessverticalspage.edit', compact('buisinessverticalspage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
      if ($request->hasFile('desktop_banner')) {
                $name = date('hismdy').$request->file('desktop_banner')->getClientOriginalName();
                 $request->file('desktop_banner')->move(public_path('assets/images/aboutuspage'), $name);
                 $requestData['desktop_banner'] = 'assets/images/aboutuspage/'.$name;
        }
      
        if ($request->hasFile('meta_image')) {
                $name = date('hismdy').$request->file('meta_image')->getClientOriginalName();
                 $request->file('meta_image')->move(public_path('assets/images/aboutuspage'), $name);
                 $requestData['meta_image'] = 'assets/images/aboutuspage/'.$name;
        }

        $buisinessverticalspage = Buisinessverticalspage::findOrFail($id);
        $buisinessverticalspage->update($requestData);

        return redirect('admin/buisinessverticalspages/1/edit')->with('flash_message', 'Buisiness verticals page updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Buisinessverticalspage::destroy($id);

        return redirect('admin/buisinessverticalspage')->with('flash_message', 'Buisinessverticalspage deleted!');
    }
}

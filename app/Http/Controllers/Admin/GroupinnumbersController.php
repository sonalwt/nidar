<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Groupinnumber;
use Illuminate\Http\Request;
use App\Authorizable;

class GroupinnumbersController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $groupinnumbers = Groupinnumber::where('icon', 'LIKE', "%$keyword%")
                ->orWhere('title', 'LIKE', "%$keyword%")
                ->orWhere('count', 'LIKE', "%$keyword%")
                ->orWhere('unit', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $groupinnumbers = Groupinnumber::latest()->paginate($perPage);
        }

        return view('admin.groupinnumbers.index', compact('groupinnumbers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.groupinnumbers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'icon' => 'required',
			'title' => 'required',
			'count' => 'required',
			'unit' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
         if ($request->hasFile('icon')) {
                $name = date('hismdy').$request->file('icon')->getClientOriginalName();
                 $request->file('icon')->move(public_path('assets/images/buisinessoverviews'), $name);
                 $requestData['icon'] = 'assets/images/buisinessoverviews/'.$name;
        }
        Groupinnumber::create($requestData);

        return redirect('admin/groupinnumbers')->with('flash_message', 'Groupinnumber added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $groupinnumber = Groupinnumber::findOrFail($id);

        return view('admin.groupinnumbers.show', compact('groupinnumber'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $groupinnumber = Groupinnumber::findOrFail($id);

        return view('admin.groupinnumbers.edit', compact('groupinnumber'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'title' => 'required',
			'count' => 'required',
			'unit' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('icon')) {
                $name = date('hismdy').$request->file('icon')->getClientOriginalName();
                 $request->file('icon')->move(public_path('assets/images/buisinessoverviews'), $name);
                 $requestData['icon'] = 'assets/images/buisinessoverviews/'.$name;
        }

        $groupinnumber = Groupinnumber::findOrFail($id);
        $groupinnumber->update($requestData);

        return redirect('admin/groupinnumbers')->with('flash_message', 'Groupinnumber updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Groupinnumber::destroy($id);

        return redirect('admin/groupinnumbers')->with('flash_message', 'Groupinnumber deleted!');
    }
}

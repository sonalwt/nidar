<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Videocorner;
use Illuminate\Http\Request;
use App\Authorizable;
use App\Models\Buisinessvertical;
class VideocornersController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $videocorners = Videocorner::where('title', 'LIKE', "%$keyword%")
                ->orWhere('image_url', 'LIKE', "%$keyword%")
                ->orWhere('video_url', 'LIKE', "%$keyword%")
                ->orWhere('buisiness_vertical', 'LIKE', "%$keyword%")
                ->orWhere('business_vertical', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $videocorners = Videocorner::latest()->paginate($perPage);
        }

        return view('admin.videocorners.index', compact('videocorners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
         $verticals=Buisinessvertical::where('status_newsroom_video_page','Enabled')->pluck('title','id');
        return view('admin.videocorners.create',compact('verticals'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'title' => 'required',
			'image_url' => 'required',
			'video_url' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        
        Videocorner::create($requestData);

        return redirect('admin/videocorners')->with('flash_message', 'Videocorner added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $videocorner = Videocorner::findOrFail($id);

        return view('admin.videocorners.show', compact('videocorner'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $videocorner = Videocorner::findOrFail($id);
$verticals=Buisinessvertical::where('status_newsroom_video_page','Enabled')->pluck('title','id');
        return view('admin.videocorners.edit', compact('videocorner','verticals'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'title' => 'required',
			'image_url' => 'required',
			'video_url' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        
        $videocorner = Videocorner::findOrFail($id);
        $videocorner->update($requestData);

        return redirect('admin/videocorners')->with('flash_message', 'Videocorner updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Videocorner::destroy($id);

        return redirect('admin/videocorners')->with('flash_message', 'Videocorner deleted!');
    }
}

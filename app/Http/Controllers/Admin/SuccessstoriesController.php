<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Successstory;
use Illuminate\Http\Request;
use App\Authorizable;

class SuccessstoriesController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $successstories = Successstory::where('featured_image', 'LIKE', "%$keyword%")
                ->orWhere('name', 'LIKE', "%$keyword%")
                ->orWhere('course', 'LIKE', "%$keyword%")
                ->orWhere('total_earnings', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('sort_order', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $successstories = Successstory::latest()->paginate($perPage);
        }

        return view('admin.successstories.index', compact('successstories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.successstories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'featured_image' => 'required',
			'name' => 'required',
			'course' => 'required',
			'total_earnings' => 'required',
			'description' => 'required',
			'sort_order' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
           
        if ($request->hasFile('featured_image')) {
                $name = date('hismdy').$request->file('featured_image')->getClientOriginalName();
                 $request->file('featured_image')->move(public_path('assets/images/ourimpactpage'), $name);
                 $requestData['featured_image'] = 'assets/images/ourimpactpage/'.$name;
        }

        Successstory::create($requestData);

        return redirect('admin/successstories')->with('flash_message', 'Successstory added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $successstory = Successstory::findOrFail($id);

        return view('admin.successstories.show', compact('successstory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $successstory = Successstory::findOrFail($id);

        return view('admin.successstories.edit', compact('successstory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name' => 'required',
			'course' => 'required',
			'total_earnings' => 'required',
			'description' => 'required',
			'sort_order' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('featured_image')) {
                $name = date('hismdy').$request->file('featured_image')->getClientOriginalName();
                 $request->file('featured_image')->move(public_path('assets/images/ourimpactpage'), $name);
                 $requestData['featured_image'] = 'assets/images/ourimpactpage/'.$name;
        }
        $successstory = Successstory::findOrFail($id);
        $successstory->update($requestData);

        return redirect('admin/successstories')->with('flash_message', 'Successstory updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Successstory::destroy($id);

        return redirect('admin/successstories')->with('flash_message', 'Successstory deleted!');
    }
}

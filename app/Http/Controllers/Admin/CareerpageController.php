<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Careerpage;
use Illuminate\Http\Request;
use App\Authorizable;

class CareerpageController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $careerpage = Careerpage::where('desktop_banner', 'LIKE', "%$keyword%")
                ->orWhere('mobile_banner', 'LIKE', "%$keyword%")
                ->orWhere('work_culture_image1', 'LIKE', "%$keyword%")
                ->orWhere('work_culture_image2', 'LIKE', "%$keyword%")
                ->orWhere('work_culture_image3', 'LIKE', "%$keyword%")
                ->orWhere('work_culture_image4', 'LIKE', "%$keyword%")
                ->orWhere('work_culture_description', 'LIKE', "%$keyword%")
                ->orWhere('equal_opportunity_employer_background_image', 'LIKE', "%$keyword%")
                ->orWhere('equal_opportunity_employer_description', 'LIKE', "%$keyword%")
                ->orWhere('meta_title', 'LIKE', "%$keyword%")
                ->orWhere('meta_keyword', 'LIKE', "%$keyword%")
                ->orWhere('meta_description', 'LIKE', "%$keyword%")
                ->orWhere('meta_image', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $careerpage = Careerpage::latest()->paginate($perPage);
        }

        return view('admin.careerpage.index', compact('careerpage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.careerpage.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
       
      
        if ($request->hasFile('desktop_banner')) {
                $name = date('hismdy').$request->file('desktop_banner')->getClientOriginalName();
                 $request->file('desktop_banner')->move(public_path('assets/images/careerpage'), $name);
                 $requestData['desktop_banner'] = 'assets/images/careerpage/'.$name;
        }
        if ($request->hasFile('mobile_banner')) {
                $name = date('hismdy').$request->file('mobile_banner')->getClientOriginalName();
                 $request->file('mobile_banner')->move(public_path('assets/images/careerpage'), $name);
                 $requestData['mobile_banner'] = 'assets/images/careerpage/'.$name;
        }
        if ($request->hasFile('work_culture_image1')) {
                $name = date('hismdy').$request->file('work_culture_image1')->getClientOriginalName();
                 $request->file('work_culture_image1')->move(public_path('assets/images/careerpage'), $name);
                 $requestData['work_culture_image1'] = 'assets/images/careerpage/'.$name;
        }if ($request->hasFile('work_culture_image2')) {
                $name = date('hismdy').$request->file('work_culture_image2')->getClientOriginalName();
                 $request->file('work_culture_image2')->move(public_path('assets/images/careerpage'), $name);
                 $requestData['work_culture_image2'] = 'assets/images/careerpage/'.$name;
        }
        if ($request->hasFile('work_culture_image3')) {
                $name = date('hismdy').$request->file('work_culture_image3')->getClientOriginalName();
                 $request->file('work_culture_image3')->move(public_path('assets/images/careerpage'), $name);
                 $requestData['work_culture_image3'] = 'assets/images/careerpage/'.$name;
        }

          if ($request->hasFile('work_culture_image4')) {
                $name = date('hismdy').$request->file('work_culture_image4')->getClientOriginalName();
                 $request->file('work_culture_image4')->move(public_path('assets/images/careerpage'), $name);
                 $requestData['work_culture_image4'] = 'assets/images/careerpage/'.$name;
        }  if ($request->hasFile('equal_opportunity_employer_background_image')) {
                $name = date('hismdy').$request->file('equal_opportunity_employer_background_image')->getClientOriginalName();
                 $request->file('equal_opportunity_employer_background_image')->move(public_path('assets/images/careerpage'), $name);
                 $requestData['equal_opportunity_employer_background_image'] = 'assets/images/careerpage/'.$name;
        }  if ($request->hasFile('meta_image')) {
                $name = date('hismdy').$request->file('meta_image')->getClientOriginalName();
                 $request->file('meta_image')->move(public_path('assets/images/careerpage'), $name);
                 $requestData['meta_image'] = 'assets/images/careerpage/'.$name;
        }
        Careerpage::create($requestData);

        return redirect('admin/careerpages')->with('flash_message', 'Careerpage added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $careerpage = Careerpage::findOrFail($id);

        return view('admin.careerpage.show', compact('careerpage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $careerpage = Careerpage::findOrFail($id);

        return view('admin.careerpage.edit', compact('careerpage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
         if ($request->hasFile('desktop_banner')) {
                $name = date('hismdy').$request->file('desktop_banner')->getClientOriginalName();
                 $request->file('desktop_banner')->move(public_path('assets/images/careerpage'), $name);
                 $requestData['desktop_banner'] = 'assets/images/careerpage/'.$name;
        }
        if ($request->hasFile('mobile_banner')) {
                $name = date('hismdy').$request->file('mobile_banner')->getClientOriginalName();
                 $request->file('mobile_banner')->move(public_path('assets/images/careerpage'), $name);
                 $requestData['mobile_banner'] = 'assets/images/careerpage/'.$name;
        }
        if ($request->hasFile('work_culture_image1')) {
                $name = date('hismdy').$request->file('work_culture_image1')->getClientOriginalName();
                 $request->file('work_culture_image1')->move(public_path('assets/images/careerpage'), $name);
                 $requestData['work_culture_image1'] = 'assets/images/careerpage/'.$name;
        }if ($request->hasFile('work_culture_image2')) {
                $name = date('hismdy').$request->file('work_culture_image2')->getClientOriginalName();
                 $request->file('work_culture_image2')->move(public_path('assets/images/careerpage'), $name);
                 $requestData['work_culture_image2'] = 'assets/images/careerpage/'.$name;
        }
        if ($request->hasFile('work_culture_image3')) {
                $name = date('hismdy').$request->file('work_culture_image3')->getClientOriginalName();
                 $request->file('work_culture_image3')->move(public_path('assets/images/careerpage'), $name);
                 $requestData['work_culture_image3'] = 'assets/images/careerpage/'.$name;
        }

          if ($request->hasFile('work_culture_image4')) {
                $name = date('hismdy').$request->file('work_culture_image4')->getClientOriginalName();
                 $request->file('work_culture_image4')->move(public_path('assets/images/careerpage'), $name);
                 $requestData['work_culture_image4'] = 'assets/images/careerpage/'.$name;
        }  if ($request->hasFile('equal_opportunity_employer_background_image')) {
                $name = date('hismdy').$request->file('equal_opportunity_employer_background_image')->getClientOriginalName();
                 $request->file('equal_opportunity_employer_background_image')->move(public_path('assets/images/careerpage'), $name);
                 $requestData['equal_opportunity_employer_background_image'] = 'assets/images/careerpage/'.$name;
        }  if ($request->hasFile('meta_image')) {
                $name = date('hismdy').$request->file('meta_image')->getClientOriginalName();
                 $request->file('meta_image')->move(public_path('assets/images/careerpage'), $name);
                 $requestData['meta_image'] = 'assets/images/careerpage/'.$name;
        }
        $careerpage = Careerpage::findOrFail($id);
        $careerpage->update($requestData);

        return redirect('admin/careerpages/1/edit')->with('flash_message', 'Career Page Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Careerpage::destroy($id);

        return redirect('admin/careerpages')->with('flash_message', 'Careerpage deleted!');
    }
}

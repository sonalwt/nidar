<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Newsroompage;
use Illuminate\Http\Request;
use App\Authorizable;

class NewsroompagesController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $newsroompages = Newsroompage::where('desktop_banner', 'LIKE', "%$keyword%")
                ->orWhere('mobile_banner', 'LIKE', "%$keyword%")
                ->orWhere('meta_title', 'LIKE', "%$keyword%")
                ->orWhere('meta_keyword', 'LIKE', "%$keyword%")
                ->orWhere('meta_description', 'LIKE', "%$keyword%")
                ->orWhere('meta_image', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $newsroompages = Newsroompage::latest()->paginate($perPage);
        }

        return view('admin.newsroompages.index', compact('newsroompages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.newsroompages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
             if ($request->hasFile('desktop_banner')) {
                $name = date('hismdy').$request->file('desktop_banner')->getClientOriginalName();
                 $request->file('desktop_banner')->move(public_path('assets/images/newsroom'), $name);
                 $requestData['desktop_banner'] = 'assets/images/newsroom/'.$name;
        }
        if ($request->hasFile('mobile_banner')) {
                $name = date('hismdy').$request->file('mobile_banner')->getClientOriginalName();
                 $request->file('mobile_banner')->move(public_path('assets/images/newsroom'), $name);
                 $requestData['mobile_banner'] = 'assets/images/newsroom/'.$name;
        }
        
        if ($request->hasFile('meta_image')) {
                $name = date('hismdy').$request->file('meta_image')->getClientOriginalName();
                 $request->file('meta_image')->move(public_path('assets/images/newsroom'), $name);
                 $requestData['meta_image'] = 'assets/images/newsroom/'.$name;
        }

        Newsroompage::create($requestData);

        return redirect('admin/newsroompages')->with('flash_message', 'Newsroompage added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $newsroompage = Newsroompage::findOrFail($id);

        return view('admin.newsroompages.show', compact('newsroompage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $newsroompage = Newsroompage::findOrFail($id);

        return view('admin.newsroompages.edit', compact('newsroompage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
             if ($request->hasFile('desktop_banner')) {
                $name = date('hismdy').$request->file('desktop_banner')->getClientOriginalName();
                 $request->file('desktop_banner')->move(public_path('assets/images/newsroom'), $name);
                 $requestData['desktop_banner'] = 'assets/images/newsroom/'.$name;
        }
        if ($request->hasFile('mobile_banner')) {
                $name = date('hismdy').$request->file('mobile_banner')->getClientOriginalName();
                 $request->file('mobile_banner')->move(public_path('assets/images/newsroom'), $name);
                 $requestData['mobile_banner'] = 'assets/images/newsroom/'.$name;
        }
        
        if ($request->hasFile('meta_image')) {
                $name = date('hismdy').$request->file('meta_image')->getClientOriginalName();
                 $request->file('meta_image')->move(public_path('assets/images/newsroom'), $name);
                 $requestData['meta_image'] = 'assets/images/newsroom/'.$name;
        }

        $newsroompage = Newsroompage::findOrFail($id);
        $newsroompage->update($requestData);

        return redirect('admin/newsroompages/1/edit')->with('flash_message', 'Newsroompage updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Newsroompage::destroy($id);

        return redirect('admin/newsroompages')->with('flash_message', 'Newsroompage deleted!');
    }
}

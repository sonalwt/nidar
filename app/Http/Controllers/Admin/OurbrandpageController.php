<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Ourbrandpage;
use Illuminate\Http\Request;
use App\Authorizable;

class OurbrandpageController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $ourbrandpage = Ourbrandpage::where('desktop_banner', 'LIKE', "%$keyword%")
                ->orWhere('mobile_banner', 'LIKE', "%$keyword%")
                ->orWhere('overview', 'LIKE', "%$keyword%")
                ->orWhere('meta_title', 'LIKE', "%$keyword%")
                ->orWhere('meta_keyword', 'LIKE', "%$keyword%")
                ->orWhere('meta_description', 'LIKE', "%$keyword%")
                ->orWhere('meta_image', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $ourbrandpage = Ourbrandpage::latest()->paginate($perPage);
        }

        return view('admin.ourbrandpage.index', compact('ourbrandpage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.ourbrandpage.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
          if ($request->hasFile('desktop_banner')) {
                $name = date('hismdy').$request->file('desktop_banner')->getClientOriginalName();
                 $request->file('desktop_banner')->move(public_path('assets/images/ourbrandpage'), $name);
                 $requestData['desktop_banner'] = 'assets/images/ourbrandpage/'.$name;
        }
        if ($request->hasFile('mobile_banner')) {
                $name = date('hismdy').$request->file('mobile_banner')->getClientOriginalName();
                 $request->file('mobile_banner')->move(public_path('assets/images/ourbrandpage'), $name);
                 $requestData['mobile_banner'] = 'assets/images/ourbrandpage/'.$name;
        }
          if ($request->hasFile('meta_image')) {
                $name = date('hismdy').$request->file('meta_image')->getClientOriginalName();
                 $request->file('meta_image')->move(public_path('assets/images/ourbrandpage'), $name);
                 $requestData['meta_image'] = 'assets/images/ourbrandpage/'.$name;
        }

        Ourbrandpage::create($requestData);

        return redirect('admin/ourbrandpages')->with('flash_message', 'Ourbrandpage added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $ourbrandpage = Ourbrandpage::findOrFail($id);

        return view('admin.ourbrandpage.show', compact('ourbrandpage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $ourbrandpage = Ourbrandpage::findOrFail($id);

        return view('admin.ourbrandpage.edit', compact('ourbrandpage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
          if ($request->hasFile('desktop_banner')) {
                $name = date('hismdy').$request->file('desktop_banner')->getClientOriginalName();
                 $request->file('desktop_banner')->move(public_path('assets/images/ourbrandpage'), $name);
                 $requestData['desktop_banner'] = 'assets/images/ourbrandpage/'.$name;
        }
        if ($request->hasFile('mobile_banner')) {
                $name = date('hismdy').$request->file('mobile_banner')->getClientOriginalName();
                 $request->file('mobile_banner')->move(public_path('assets/images/ourbrandpage'), $name);
                 $requestData['mobile_banner'] = 'assets/images/ourbrandpage/'.$name;
        }
          if ($request->hasFile('meta_image')) {
                $name = date('hismdy').$request->file('meta_image')->getClientOriginalName();
                 $request->file('meta_image')->move(public_path('assets/images/ourbrandpage'), $name);
                 $requestData['meta_image'] = 'assets/images/ourbrandpage/'.$name;
        }

        $ourbrandpage = Ourbrandpage::findOrFail($id);
        $ourbrandpage->update($requestData);

        return redirect('admin/ourbrandpages/1/edit')->with('flash_message', 'Our brand page updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Ourbrandpage::destroy($id);

        return redirect('admin/ourbrandpage')->with('flash_message', 'Ourbrandpage deleted!');
    }
}

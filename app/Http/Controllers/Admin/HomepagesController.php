<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Homepage;
use Illuminate\Http\Request;
use App\Authorizable;
use App\Models\Pressrelease;
class HomepagesController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $homepages = Homepage::where('banner', 'LIKE', "%$keyword%")
                ->orWhere('news1', 'LIKE', "%$keyword%")
                ->orWhere('news2', 'LIKE', "%$keyword%")
                ->orWhere('news3', 'LIKE', "%$keyword%")
                ->orWhere('equal_opportunity_employer_description', 'LIKE', "%$keyword%")
                ->orWhere('equal_opportunity_employer_image', 'LIKE', "%$keyword%")
                ->orWhere('careers_descriptione', 'LIKE', "%$keyword%")
                ->orWhere('careers_image', 'LIKE', "%$keyword%")
                ->orWhere('current_openings_image', 'LIKE', "%$keyword%")
                ->orWhere('meta_title', 'LIKE', "%$keyword%")
                ->orWhere('meta_keyword', 'LIKE', "%$keyword%")
                ->orWhere('meta_description', 'LIKE', "%$keyword%")
                ->orWhere('meta_image', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $homepages = Homepage::latest()->paginate($perPage);
        }

        return view('admin.homepages.index', compact('homepages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $news=Pressrelease::where('status','Enabled')->pluck('title','id');
        return view('admin.homepages.create',compact('news'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
   
       if(isset($request->mapping) && !empty($request->mapping) && count($request->mapping)>0){
           // dd($request->prop);
          foreach($request->mapping as $key=>$map){
           
              if(isset($map['featured_image']) && !empty($map['featured_image'])){
                     $filename=date('hisdmy').$map['featured_image']->getClientOriginalName();
                   $map['featured_image']->move(base_path('public/assets/images/ourimpactpage'), $filename);
                   $raw[$key]['featured_image']='assets/images/ourimpactpage/'.$filename;
               }else{
                   if(isset($map['featured_image_original']) && !empty($map['featured_image_original'])){
                $raw[$key]['featured_image']=$map['featured_image_original'];
            }
               }
           if(isset($map['featured_image_webp']) && !empty($map['featured_image_webp'])){
                     $filename=date('hisdmy').$map['featured_image_webp']->getClientOriginalName();
                   $map['featured_image_webp']->move(base_path('public/assets/images/ourimpactpage'), $filename);
                   $raw[$key]['featured_image_webp']='assets/images/ourimpactpage/'.$filename;
               }else{
                  if(isset($map['featured_image_webp_original']) && !empty($map['featured_image_webp_original'])){
                $raw[$key]['featured_image_webp']=$map['featured_image_webp_original'];

               }
          }
          if(isset($raw)){
             $requestData['banner']=json_encode($raw);
          }
         
        }
    }
        if ($request->hasFile('equal_opportunity_employer_image')) {
                $name = date('hismdy').$request->file('equal_opportunity_employer_image')->getClientOriginalName();
                 $request->file('equal_opportunity_employer_image')->move(public_path('assets/images/Homepage'), $name);
                 $requestData['equal_opportunity_employer_image'] = 'assets/images/Homepage/'.$name;
        }if ($request->hasFile('careers_image')) {
                $name = date('hismdy').$request->file('careers_image')->getClientOriginalName();
                 $request->file('careers_image')->move(public_path('assets/images/Homepage'), $name);
                 $requestData['careers_image'] = 'assets/images/Homepage/'.$name;
        }if ($request->hasFile('current_openings_image')) {
                $name = date('hismdy').$request->file('current_openings_image')->getClientOriginalName();
                 $request->file('current_openings_image')->move(public_path('assets/images/Homepage'), $name);
                 $requestData['current_openings_image'] = 'assets/images/Homepage/'.$name;
        }
       if ($request->hasFile('meta_image')) {
                $name = date('hismdy').$request->file('meta_image')->getClientOriginalName();
                 $request->file('meta_image')->move(public_path('assets/images/Homepage'), $name);
                 $requestData['meta_image'] = 'assets/images/Homepage/'.$name;
        }

        Homepage::create($requestData);

        return redirect('admin/homepages')->with('flash_message', 'Homepage added!');
    }

    public function show($id)
    {
        $homepage = Homepage::findOrFail($id);

        return view('admin.homepages.show', compact('homepage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $homepage = Homepage::findOrFail($id);
        $news=Pressrelease::where('status','Enabled')->pluck('title','id');
        return view('admin.homepages.edit', compact('homepage','news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
         if(isset($request->mapping) && !empty($request->mapping) && count($request->mapping)>0){
           // dd($request->prop);
          foreach($request->mapping as $key=>$map){
           
              if(isset($map['featured_image']) && !empty($map['featured_image'])){
                     $filename=date('hisdmy').$map['featured_image']->getClientOriginalName();
                   $map['featured_image']->move(base_path('public/assets/images/ourimpactpage'), $filename);
                   $raw[$key]['featured_image']='assets/images/ourimpactpage/'.$filename;
               }else{
                   if(isset($map['featured_image_original']) && !empty($map['featured_image_original'])){
                $raw[$key]['featured_image']=$map['featured_image_original'];
            }
               }
           if(isset($map['featured_image_webp']) && !empty($map['featured_image_webp'])){
                     $filename=date('hisdmy').$map['featured_image_webp']->getClientOriginalName();
                   $map['featured_image_webp']->move(base_path('public/assets/images/ourimpactpage'), $filename);
                   $raw[$key]['featured_image_webp']='assets/images/ourimpactpage/'.$filename;
               }else{
                  if(isset($map['featured_image_webp_original']) && !empty($map['featured_image_webp_original'])){
                $raw[$key]['featured_image_webp']=$map['featured_image_webp_original'];

               }
          }
          if(isset($map['featured_image_mobile']) && !empty($map['featured_image_mobile'])){
                     $filename=date('hisdmy').$map['featured_image_mobile']->getClientOriginalName();
                   $map['featured_image_mobile']->move(base_path('public/assets/images/ourimpactpage'), $filename);
                   $raw[$key]['featured_image_mobile']='assets/images/ourimpactpage/'.$filename;
               }else{
                   if(isset($map['featured_image_mobile_original']) && !empty($map['featured_image_mobile_original'])){
                $raw[$key]['featured_image_mobile']=$map['featured_image_mobile_original'];
            }
               }
           if(isset($map['featured_image_mobile_webp']) && !empty($map['featured_image_mobile_webp'])){
                     $filename=date('hisdmy').$map['featured_image_mobile_webp']->getClientOriginalName();
                   $map['featured_image_mobile_webp']->move(base_path('public/assets/images/ourimpactpage'), $filename);
                   $raw[$key]['featured_image_mobile_webp']='assets/images/ourimpactpage/'.$filename;
               }else{
                  if(isset($map['featured_image_mobile_webp_original']) && !empty($map['featured_image_mobile_webp_original'])){
                $raw[$key]['featured_image_mobile_webp']=$map['featured_image_mobile_webp_original'];

               }
          }
          if(isset($raw)){
             $requestData['banner']=json_encode($raw);
          }
         
        }
    }
        if ($request->hasFile('equal_opportunity_employer_image')) {
                $name = date('hismdy').$request->file('equal_opportunity_employer_image')->getClientOriginalName();
                 $request->file('equal_opportunity_employer_image')->move(public_path('assets/images/Homepage'), $name);
                 $requestData['equal_opportunity_employer_image'] = 'assets/images/Homepage/'.$name;
        }if ($request->hasFile('careers_image')) {
                $name = date('hismdy').$request->file('careers_image')->getClientOriginalName();
                 $request->file('careers_image')->move(public_path('assets/images/Homepage'), $name);
                 $requestData['careers_image'] = 'assets/images/Homepage/'.$name;
        }if ($request->hasFile('current_openings_image')) {
                $name = date('hismdy').$request->file('current_openings_image')->getClientOriginalName();
                 $request->file('current_openings_image')->move(public_path('assets/images/Homepage'), $name);
                 $requestData['current_openings_image'] = 'assets/images/Homepage/'.$name;
        }
       if ($request->hasFile('meta_image')) {
                $name = date('hismdy').$request->file('meta_image')->getClientOriginalName();
                 $request->file('meta_image')->move(public_path('assets/images/Homepage'), $name);
                 $requestData['meta_image'] = 'assets/images/Homepage/'.$name;
        }
        if ($request->hasFile('work_culture_image')) {
                $name = date('hismdy').$request->file('work_culture_image')->getClientOriginalName();
                 $request->file('work_culture_image')->move(public_path('assets/images/Homepage'), $name);
                 $requestData['work_culture_image'] = 'assets/images/Homepage/'.$name;
        }
        if ($request->hasFile('news2image')) {
                $name = date('hismdy').$request->file('news2image')->getClientOriginalName();
                 $request->file('news2image')->move(public_path('assets/images/Homepage'), $name);
                 $requestData['news2image'] = 'assets/images/Homepage/'.$name;
        }
        $homepage = Homepage::findOrFail($id);
        $homepage->update($requestData);

        return redirect('admin/homepages/1/edit')->with('flash_message', 'Homepage updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Homepage::destroy($id);

        return redirect('admin/homepages')->with('flash_message', 'Homepage deleted!');
    }
}

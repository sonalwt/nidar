<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Contactuspage;
use Illuminate\Http\Request;
use App\Authorizable;

class ContactuspageController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $contactuspage = Contactuspage::where('desktop_banner', 'LIKE', "%$keyword%")
                ->orWhere('mobile_banner', 'LIKE', "%$keyword%")
                ->orWhere('india_image', 'LIKE', "%$keyword%")
                ->orWhere('india_address', 'LIKE', "%$keyword%")
                ->orWhere('india_mobile', 'LIKE', "%$keyword%")
                ->orWhere('uae_image', 'LIKE', "%$keyword%")
                ->orWhere('uae_address', 'LIKE', "%$keyword%")
                ->orWhere('uae_mobile', 'LIKE', "%$keyword%")
                ->orWhere('map', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $contactuspage = Contactuspage::latest()->paginate($perPage);
        }

        return view('admin.contactuspage.index', compact('contactuspage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.contactuspage.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
     
         if ($request->hasFile('desktop_banner')) {
                $name = date('hismdy').$request->file('desktop_banner')->getClientOriginalName();
                 $request->file('desktop_banner')->move(public_path('assets/images/contactpage'), $name);
                 $requestData['desktop_banner'] = 'assets/images/contactpage/'.$name;
        }
        if ($request->hasFile('mobile_banner')) {
                $name = date('hismdy').$request->file('mobile_banner')->getClientOriginalName();
                 $request->file('mobile_banner')->move(public_path('assets/images/contactpage'), $name);
                 $requestData['mobile_banner'] = 'assets/images/contactpage/'.$name;
        }
        if ($request->hasFile('india_image')) {
                $name = date('hismdy').$request->file('india_image')->getClientOriginalName();
                 $request->file('india_image')->move(public_path('assets/images/contactpage'), $name);
                 $requestData['uae_image'] = 'assets/images/contactpage/'.$name;
        }if ($request->hasFile('uae_image')) {
                $name = date('hismdy').$request->file('uae_image')->getClientOriginalName();
                 $request->file('uae_image')->move(public_path('assets/images/contactpage'), $name);
                 $requestData['uae_imageuae_image'] = 'assets/images/contactpage/'.$name;
        }

        Contactuspage::create($requestData);

        return redirect('admin/contactuspage')->with('flash_message', 'Contactuspage added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $contactuspage = Contactuspage::findOrFail($id);

        return view('admin.contactuspage.show', compact('contactuspage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $contactuspage = Contactuspage::findOrFail($id);

        return view('admin.contactuspage.edit', compact('contactuspage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        if ($request->hasFile('desktop_banner')) {
                $name = date('hismdy').$request->file('desktop_banner')->getClientOriginalName();
                 $request->file('desktop_banner')->move(public_path('assets/images/contactpage'), $name);
                 $requestData['desktop_banner'] = 'assets/images/contactpage/'.$name;
        }
        if ($request->hasFile('mobile_banner')) {
                $name = date('hismdy').$request->file('mobile_banner')->getClientOriginalName();
                 $request->file('mobile_banner')->move(public_path('assets/images/contactpage'), $name);
                 $requestData['mobile_banner'] = 'assets/images/contactpage/'.$name;
        }
        if ($request->hasFile('india_image')) {
                $name = date('hismdy').$request->file('india_image')->getClientOriginalName();
                 $request->file('india_image')->move(public_path('assets/images/contactpage'), $name);
                 $requestData['india_image'] = 'assets/images/contactpage/'.$name;
        }if ($request->hasFile('uae_image')) {
                $name = date('hismdy').$request->file('uae_image')->getClientOriginalName();
                 $request->file('uae_image')->move(public_path('assets/images/contactpage'), $name);
                 $requestData['uae_image'] = 'assets/images/contactpage/'.$name;
        }
        if ($request->hasFile('meta_image')) {
                $name = date('hismdy').$request->file('meta_image')->getClientOriginalName();
                 $request->file('meta_image')->move(public_path('assets/images/contactpage'), $name);
                 $requestData['meta_image'] = 'assets/images/contactpage/'.$name;
        }

        $contactuspage = Contactuspage::findOrFail($id);
        $contactuspage->update($requestData);

        return redirect('admin/contactuspages/1/edit')->with('flash_message', 'Contact Us Page Details updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Contactuspage::destroy($id);

        return redirect('admin/contactuspage')->with('flash_message', 'Contactuspage deleted!');
    }
}

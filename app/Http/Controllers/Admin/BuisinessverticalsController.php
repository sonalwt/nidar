<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Buisinessvertical;
use Illuminate\Http\Request;
use App\Authorizable;

class BuisinessverticalsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $buisinessverticals = Buisinessvertical::where('featured_image', 'LIKE', "%$keyword%")
                ->orWhere('title', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $buisinessverticals = Buisinessvertical::latest()->paginate($perPage);
        }

        return view('admin.buisinessverticals.index', compact('buisinessverticals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.buisinessverticals.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'title' => 'required'
		]);
        $requestData = $request->all();
      
         if ($request->hasFile('featured_image')) {
                $name = date('hismdy').$request->file('featured_image')->getClientOriginalName();
                 $request->file('featured_image')->move(public_path('assets/images/ourbrandpage'), $name);
                 $requestData['featured_image'] = 'assets/images/ourbrandpage/'.$name;
        }
        if ($request->hasFile('icon')) {
                $name = date('hismdy').$request->file('icon')->getClientOriginalName();
                 $request->file('icon')->move(public_path('assets/images/aboutus'), $name);
                 $requestData['icon'] = 'assets/images/aboutus/'.$name;
        }
         if ($request->hasFile('image')) {
                $name = date('hismdy').$request->file('image')->getClientOriginalName();
                 $request->file('image')->move(public_path('assets/images/ourbrandpage'), $name);
                 $requestData['image'] = 'assets/images/ourbrandpage/'.$name;
        }
        if ($request->hasFile('image2')) {
                $name = date('hismdy').$request->file('image2')->getClientOriginalName();
                 $request->file('image2')->move(public_path('assets/images/aboutus'), $name);
                 $requestData['image2'] = 'assets/images/aboutus/'.$name;
        }
        if(isset($request->mapping) && !empty($request->mapping)){
           // dd($request->prop);
          $raw=array();
          foreach($request->mapping as $key=>$map){
            
           if(isset($map) && !empty($map) && $map != ''){
              if(isset($map['featured_image']) && !empty($map['featured_image'])){
                     $filename=date('hisdmy').$map['featured_image']->getClientOriginalName();
                   $map['featured_image']->move(base_path('public/assets/images/ourimpactpage'), $filename);
                   $raw[$key]['featured_image']='assets/images/ourimpactpage/'.$filename;
               }else{
                $raw[$key]['featured_image']=$map['featured_image_original'];
               }
                $raw[$key]['title']= $map['title']; 
            $raw[$key]['sectors']= $map['sectors'];
           }
                     
           }
          }
          if(isset($raw) && !empty($raw)){
            $requestData['subsectors']=json_encode($raw);
          }else{
            $requestData['subsectors']=NULL;
          }
          
     
         if(isset($request->newmapping) && !empty($request->newmapping)){
           // dd($request->prop);
          $raw1=array();
          foreach($request->newmapping as $key=>$map){
           if(isset($map) && !empty($map) && $map != ''){
              if(isset($map['featured_image']) && !empty($map['featured_image'])){
                     $filename=date('hisdmy').$map['featured_image']->getClientOriginalName();
                   $map['featured_image']->move(base_path('public/assets/images/ourimpactpage'), $filename);
                   $raw[$key]['featured_image']='assets/images/ourimpactpage/'.$filename;
               }else{
                $raw1[$key]['featured_image']=$map['featured_image_original'];
               }
           $raw1[$key]['title']= $map['title'];    
           $raw1[$key]['count']= $map['count'];           
           $raw1[$key]['unit']= $map['unit'];    
           
          }
         
        }
      }
      if(isset($raw1) && !empty($raw1)){
            $requestData['fast_facts']=json_encode($raw1);
          }else{
            $requestData['fast_facts']=NULL;
          }
        Buisinessvertical::create($requestData);

        return redirect('admin/buisinessverticals')->with('flash_message', 'Buisiness vertical added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $buisinessvertical = Buisinessvertical::findOrFail($id);

        return view('admin.buisinessverticals.show', compact('buisinessvertical'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $buisinessvertical = Buisinessvertical::findOrFail($id);

        return view('admin.buisinessverticals.edit', compact('buisinessvertical'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'title' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('featured_image')) {
                $name = date('hismdy').$request->file('featured_image')->getClientOriginalName();
                 $request->file('featured_image')->move(public_path('assets/images/ourbrandpage'), $name);
                 $requestData['featured_image'] = 'assets/images/ourbrandpage/'.$name;
        }
        if ($request->hasFile('icon')) {
                $name = date('hismdy').$request->file('icon')->getClientOriginalName();
                 $request->file('icon')->move(public_path('assets/images/aboutus'), $name);
                 $requestData['icon'] = 'assets/images/aboutus/'.$name;
        }
         if ($request->hasFile('image')) {
                $name = date('hismdy').$request->file('image')->getClientOriginalName();
                 $request->file('image')->move(public_path('assets/images/ourbrandpage'), $name);
                 $requestData['image'] = 'assets/images/ourbrandpage/'.$name;
        }
        if ($request->hasFile('image2')) {
                $name = date('hismdy').$request->file('image2')->getClientOriginalName();
                 $request->file('image2')->move(public_path('assets/images/aboutus'), $name);
                 $requestData['image2'] = 'assets/images/aboutus/'.$name;
        }
         if(isset($request->mapping) && !empty($request->mapping)){
           // dd($request->prop);
          $raw=array();
          foreach($request->mapping as $key=>$map){
            
           if(isset($map) && !empty($map) && $map != ''){
              if(isset($map['featured_image']) && !empty($map['featured_image'])){
                     $filename=date('hisdmy').$map['featured_image']->getClientOriginalName();
                   $map['featured_image']->move(base_path('public/assets/images/ourimpactpage'), $filename);
                   $raw[$key]['featured_image']='assets/images/ourimpactpage/'.$filename;
               }else{
               if(isset($map['featured_image_original'])){
                   $raw[$key]['featured_image']=$map['featured_image_original'];
                }
               }
                $raw[$key]['title']= $map['title']; 
            $raw[$key]['sectors']= $map['sectors'];
           }
                     
           }
          }
          if(isset($raw) && !empty($raw)){
            $requestData['subsectors']=json_encode($raw);
          }else{
            $requestData['subsectors']=NULL;
          }
          
     
         if(isset($request->newmapping) && !empty($request->newmapping)){
           // dd($request->prop);
          $raw1=array();
          foreach($request->newmapping as $key=>$map){
           if(isset($map) && !empty($map) && $map != ''){
              if(isset($map['featured_image']) && !empty($map['featured_image'])){
                     $filename=date('hisdmy').$map['featured_image']->getClientOriginalName();
                   $map['featured_image']->move(base_path('public/assets/images/ourimpactpage'), $filename);
                   $raw[$key]['featured_image']='assets/images/ourimpactpage/'.$filename;
               }else{
                if(isset($map['featured_image_original'])){
                   $raw1[$key]['featured_image']=$map['featured_image_original'];
                }
               
               }
           $raw1[$key]['title']= $map['title'];    
           $raw1[$key]['count']= $map['count'];           
           $raw1[$key]['unit']= $map['unit'];    
           
          }
         
        }
      }
      if(isset($raw1) && !empty($raw1)){
            $requestData['fast_facts']=json_encode($raw1);
          }else{
            $requestData['fast_facts']=NULL;
          }
        $buisinessvertical = Buisinessvertical::findOrFail($id);
        $buisinessvertical->update($requestData);

        return redirect('admin/buisinessverticals')->with('flash_message', 'Buisiness vertical updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Buisinessvertical::destroy($id);

        return redirect('admin/buisinessverticals')->with('flash_message', 'Buisinessvertical deleted!');
    }
}

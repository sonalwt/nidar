<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Brand;
use Illuminate\Http\Request;
use App\Authorizable;
use App\Models\Buisinessvertical;
use Illuminate\Support\Str;
class BrandsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $brands = Brand::where('featured_image', 'LIKE', "%$keyword%")
                ->orWhere('brand_logo', 'LIKE', "%$keyword%")
                ->orWhere('title', 'LIKE', "%$keyword%")
                ->orWhere('short_description', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('sort_order', 'LIKE', "%$keyword%")
                ->orWhere('slug', 'LIKE', "%$keyword%")
                ->orWhere('meta_title', 'LIKE', "%$keyword%")
                ->orWhere('meta_keyword', 'LIKE', "%$keyword%")
                ->orWhere('meta_description', 'LIKE', "%$keyword%")
                ->orWhere('meta_image', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $brands = Brand::latest()->paginate($perPage);
        }

        return view('admin.brands.index', compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $verticals=Buisinessvertical::where('status','Enabled')->pluck('title','id');
        return view('admin.brands.create',compact('verticals'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'featured_image' => 'required',
			'brand_logo' => 'required',
			'title' => 'required|unique:brands',
			'short_description' => 'required',
			'description' => 'required',
			'sort_order' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        $requestData['slug'] = str_slug($request->title);
          if ($request->hasFile('featured_image')) {
                $name = date('hismdy').$request->file('featured_image')->getClientOriginalName();
                 $request->file('featured_image')->move(public_path('assets/images/ourbrandpage'), $name);
                 $requestData['featured_image'] = 'assets/images/ourbrandpage/'.$name;
        }
         if ($request->hasFile('brand_logo')) {
                $name = date('hismdy').$request->file('brand_logo')->getClientOriginalName();
                 $request->file('brand_logo')->move(public_path('assets/images/ourbrandpage'), $name);
                 $requestData['brand_logo'] = 'assets/images/ourbrandpage/'.$name;
        }
         if ($request->hasFile('meta_image')) {
                $name = date('hismdy').$request->file('meta_image')->getClientOriginalName();
                 $request->file('meta_image')->move(public_path('assets/images/ourbrandpage'), $name);
                 $requestData['meta_image'] = 'assets/images/ourbrandpage/'.$name;
        }
        if ($request->hasFile('featured_image_mob')) {
                $name = date('hismdy').$request->file('featured_image_mob')->getClientOriginalName();
                 $request->file('featured_image_mob')->move(public_path('assets/images/ourbrandpage'), $name);
                 $requestData['featured_image_mob'] = 'assets/images/ourbrandpage/'.$name;
        }
         if ($request->hasFile('home_page_image')) {
                $name = date('hismdy').$request->file('home_page_image')->getClientOriginalName();
                 $request->file('home_page_image')->move(public_path('assets/images/ourbrandpage'), $name);
                 $requestData['home_page_image'] = 'assets/images/ourbrandpage/'.$name;
        }

        Brand::create($requestData);

        return redirect('admin/brands')->with('flash_message', 'Brand added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $brand = Brand::findOrFail($id);

        return view('admin.brands.show', compact('brand'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
         $verticals=Buisinessvertical::where('status','Enabled')->pluck('title','id');
        $brand = Brand::findOrFail($id);

        return view('admin.brands.edit', compact('brand','verticals'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'title' => 'required',
			'short_description' => 'required',
			'description' => 'required',
			'sort_order' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
         $requestData['slug'] = str_slug($request->title);
        if ($request->hasFile('featured_image')) {
                $name = date('hismdy').$request->file('featured_image')->getClientOriginalName();
                 $request->file('featured_image')->move(public_path('assets/images/ourbrandpage'), $name);
                 $requestData['featured_image'] = 'assets/images/ourbrandpage/'.$name;
        }
         if ($request->hasFile('brand_logo')) {
                $name = date('hismdy').$request->file('brand_logo')->getClientOriginalName();
                 $request->file('brand_logo')->move(public_path('assets/images/ourbrandpage'), $name);
                 $requestData['brand_logo'] = 'assets/images/ourbrandpage/'.$name;
        }
         if ($request->hasFile('meta_image')) {
                $name = date('hismdy').$request->file('meta_image')->getClientOriginalName();
                 $request->file('meta_image')->move(public_path('assets/images/ourbrandpage'), $name);
                 $requestData['meta_image'] = 'assets/images/ourbrandpage/'.$name;
        }
        if ($request->hasFile('featured_image_mob')) {
                $name = date('hismdy').$request->file('featured_image_mob')->getClientOriginalName();
                 $request->file('featured_image_mob')->move(public_path('assets/images/ourbrandpage'), $name);
                 $requestData['featured_image_mob'] = 'assets/images/ourbrandpage/'.$name;
        }
         if ($request->hasFile('home_page_image')) {
                $name = date('hismdy').$request->file('home_page_image')->getClientOriginalName();
                 $request->file('home_page_image')->move(public_path('assets/images/ourbrandpage'), $name);
                 $requestData['home_page_image'] = 'assets/images/ourbrandpage/'.$name;
        }
        $brand = Brand::findOrFail($id);
        $brand->update($requestData);

        return redirect('admin/brands')->with('flash_message', 'Brand updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Brand::destroy($id);

        return redirect('admin/brands')->with('flash_message', 'Brand deleted!');
    }
}

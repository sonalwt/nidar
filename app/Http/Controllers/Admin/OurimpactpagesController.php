<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Ourimpactpage;
use Illuminate\Http\Request;
use App\Authorizable;

class OurimpactpagesController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $ourimpactpages = Ourimpactpage::where('desktop_banner', 'LIKE', "%$keyword%")
                ->orWhere('mobile_banner', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('the_cummunity_description', 'LIKE', "%$keyword%")
                ->orWhere('csr_initiatives_description', 'LIKE', "%$keyword%")
                ->orWhere('skill_development_part1', 'LIKE', "%$keyword%")
                ->orWhere('skill_development_part2', 'LIKE', "%$keyword%")
                ->orWhere('meta_title', 'LIKE', "%$keyword%")
                ->orWhere('meta_keyword', 'LIKE', "%$keyword%")
                ->orWhere('meta_description', 'LIKE', "%$keyword%")
                ->orWhere('meta_image', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $ourimpactpages = Ourimpactpage::latest()->paginate($perPage);
        }

        return view('admin.ourimpactpages.index', compact('ourimpactpages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.ourimpactpages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        if ($request->hasFile('desktop_banner')) {
                $name = date('hismdy').$request->file('desktop_banner')->getClientOriginalName();
                 $request->file('desktop_banner')->move(public_path('assets/images/ourimpactpage'), $name);
                 $requestData['desktop_banner'] = 'assets/images/ourimpactpage/'.$name;
        }
        if ($request->hasFile('mobile_banner')) {
                $name = date('hismdy').$request->file('mobile_banner')->getClientOriginalName();
                 $request->file('mobile_banner')->move(public_path('assets/images/ourimpactpage'), $name);
                 $requestData['mobile_banner'] = 'assets/images/ourimpactpage/'.$name;
        }
          if ($request->hasFile('meta_image')) {
                $name = date('hismdy').$request->file('meta_image')->getClientOriginalName();
                 $request->file('meta_image')->move(public_path('assets/images/ourimpactpage'), $name);
                 $requestData['meta_image'] = 'assets/images/ourimpactpage/'.$name;
        }

        if(isset($request->mapping) && !empty($request->mapping)){
           // dd($request->prop);
          foreach($request->mapping as $key=>$map){
           
              if(isset($map['featured_image']) && !empty($map['featured_image'])){
                     $filename=date('hisdmy').$map['featured_image']->getClientOriginalName();
                   $map['featured_image']->move(base_path('public/assets/images/ourimpactpage'), $filename);
                   $raw[$key]['featured_image']='assets/images/ourimpactpage/'.$filename;
               }else{
                $raw[$key]['featured_image']=$map['featured_image_original'];
               }
           $raw[$key]['title']= $map['title'];           
           
          }
          $requestData['cummunity_img']=json_encode($raw);
        }
        if(isset($request->newmapping) && !empty($request->newmapping)){
           // dd($request->prop);
          foreach($request->newmapping as $key=>$map){
           
              if(isset($map['featured_image']) && !empty($map['featured_image'])){
                     $filename=date('hisdmy').$map['featured_image']->getClientOriginalName();
                   $map['featured_image']->move(base_path('public/assets/images/ourimpactpage'), $filename);
                   $raw[$key]['featured_image']='assets/images/ourimpactpage/'.$filename;
               }else{
                $raw[$key]['featured_image']=$map['featured_image_original'];
               }
           $raw[$key]['title']= $map['title'];           
           
          }
          $requestData['coeareas']=json_encode($raw);
        }
        if(isset($request->sustain) && !empty($request->sustain)){
           // dd($request->prop);
          foreach($request->sustain as $key=>$map){
           
              if(isset($map['featured_image']) && !empty($map['featured_image'])){
                     $filename=date('hisdmy').$map['featured_image']->getClientOriginalName();
                   $map['featured_image']->move(base_path('public/assets/images/ourimpactpage'), $filename);
                   $raw[$key]['featured_image']='assets/images/ourimpactpage/'.$filename;
               }else{
                $raw[$key]['featured_image']=$map['featured_image_original'];
               }
           $raw[$key]['title']= $map['title'];           
           
          }
          $requestData['sustainability']=json_encode($raw);
        }

        Ourimpactpage::create($requestData);

        return redirect('admin/ourimpactpages')->with('flash_message', 'Ourimpactpage added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $ourimpactpage = Ourimpactpage::findOrFail($id);

        return view('admin.ourimpactpages.show', compact('ourimpactpage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $ourimpactpage = Ourimpactpage::findOrFail($id);

        return view('admin.ourimpactpages.edit', compact('ourimpactpage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        if ($request->hasFile('desktop_banner')) {
                $name = date('hismdy').$request->file('desktop_banner')->getClientOriginalName();
                 $request->file('desktop_banner')->move(public_path('assets/images/ourimpactpage'), $name);
                 $requestData['desktop_banner'] = 'assets/images/ourimpactpage/'.$name;
        }
        if ($request->hasFile('mobile_banner')) {
                $name = date('hismdy').$request->file('mobile_banner')->getClientOriginalName();
                 $request->file('mobile_banner')->move(public_path('assets/images/ourimpactpage'), $name);
                 $requestData['mobile_banner'] = 'assets/images/ourimpactpage/'.$name;
        }
          if ($request->hasFile('meta_image')) {
                $name = date('hismdy').$request->file('meta_image')->getClientOriginalName();
                 $request->file('meta_image')->move(public_path('assets/images/ourimpactpage'), $name);
                 $requestData['meta_image'] = 'assets/images/ourimpactpage/'.$name;
        }
        if(isset($request->mapping) && !empty($request->mapping)){
           // dd($request->prop);
          foreach($request->mapping as $key=>$map){
           
              if(isset($map['featured_image']) && !empty($map['featured_image'])){
                     $filename=date('hisdmy').$map['featured_image']->getClientOriginalName();
                   $map['featured_image']->move(base_path('public/assets/images/ourimpactpage'), $filename);
                   $raw[$key]['featured_image']='assets/images/ourimpactpage/'.$filename;
               }else{
                $raw[$key]['featured_image']=$map['featured_image_original'];
               }
           $raw[$key]['title']= $map['title'];           
           
          }
          $requestData['cummunity_img']=json_encode($raw);
        }
        if(isset($request->newmapping) && !empty($request->newmapping)){
           // dd($request->prop);
          foreach($request->newmapping as $key=>$map){
           
              if(isset($map['featured_image']) && !empty($map['featured_image'])){
                     $filename=date('hisdmy').$map['featured_image']->getClientOriginalName();
                   $map['featured_image']->move(base_path('public/assets/images/ourimpactpage'), $filename);
                   $raw[$key]['featured_image']='assets/images/ourimpactpage/'.$filename;
               }else{
                $raw[$key]['featured_image']=$map['featured_image_original'];
               }
           $raw[$key]['title']= $map['title'];           
           
          }
          $requestData['coeareas']=json_encode($raw);
        }
            if(isset($request->sustain) && !empty($request->sustain)){
           // dd($request->prop);
          foreach($request->sustain as $key=>$map){
           
              if(isset($map['featured_image']) && !empty($map['featured_image'])){
                     $filename=date('hisdmy').$map['featured_image']->getClientOriginalName();
                   $map['featured_image']->move(base_path('public/assets/images/ourimpactpage'), $filename);
                   $raw[$key]['featured_image']='assets/images/ourimpactpage/'.$filename;
               }else{
                $raw[$key]['featured_image']=$map['featured_image_original'];
               }
           $raw[$key]['title']= $map['title'];           
           
          }
          $requestData['sustainability']=json_encode($raw);
        }
        $ourimpactpage = Ourimpactpage::findOrFail($id);
        $ourimpactpage->update($requestData);

        return redirect('admin/ourimpactpages/1/edit')->with('flash_message', 'Our Impact Page Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Ourimpactpage::destroy($id);

        return redirect('admin/ourimpactpages')->with('flash_message', 'Ourimpactpage deleted!');
    }
}

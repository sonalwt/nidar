<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Pressrelease;
use Illuminate\Http\Request;
use App\Authorizable;
use App\Models\Buisinessvertical;
class PressreleasesController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $pressreleases = Pressrelease::where('title', 'LIKE', "%$keyword%")
                ->orWhere('image_url', 'LIKE', "%$keyword%")
                ->orWhere('news_link', 'LIKE', "%$keyword%")
                ->orWhere('release_date', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $pressreleases = Pressrelease::latest()->paginate($perPage);
        }

        return view('admin.pressreleases.index', compact('pressreleases'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $verticals=Buisinessvertical::where('status_newsroom_press_page','Enabled')->pluck('title','id');
        return view('admin.pressreleases.create', compact('verticals'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'title' => 'required',
			'image_url' => 'required',
			'news_link' => 'required',
			'release_date' => 'required',
			'description' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
               if ($request->hasFile('image_url')) {
                $name = date('hismdy').$request->file('image_url')->getClientOriginalName();
                 $request->file('image_url')->move(public_path('assets/images/newsroom'), $name);
                 $requestData['image_url'] = 'assets/images/newsroom/'.$name;
        }
        Pressrelease::create($requestData);

        return redirect('admin/pressreleases')->with('flash_message', 'Pressrelease added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $pressrelease = Pressrelease::findOrFail($id);

        return view('admin.pressreleases.show', compact('pressrelease'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $pressrelease = Pressrelease::findOrFail($id);
        $verticals=Buisinessvertical::where('status_newsroom_press_page','Enabled')->pluck('title','id');

        return view('admin.pressreleases.edit', compact('pressrelease','verticals'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'title' => 'required',
			'news_link' => 'required',
			'release_date' => 'required',
			'description' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
             if ($request->hasFile('image_url')) {
                $name = date('hismdy').$request->file('image_url')->getClientOriginalName();
                 $request->file('image_url')->move(public_path('assets/images/newsroom'), $name);
                 $requestData['image_url'] = 'assets/images/newsroom/'.$name;
        }
        $pressrelease = Pressrelease::findOrFail($id);
        $pressrelease->update($requestData);

        return redirect('admin/pressreleases')->with('flash_message', 'Pressrelease updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Pressrelease::destroy($id);

        return redirect('admin/pressreleases')->with('flash_message', 'Pressrelease deleted!');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Aboutuspage;
use Illuminate\Http\Request;
use App\Authorizable;

class AboutuspagesController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $aboutuspages = Aboutuspage::where('desktop_banner', 'LIKE', "%$keyword%")
                ->orWhere('mobile_banner', 'LIKE', "%$keyword%")
                ->orWhere('overview_part_1', 'LIKE', "%$keyword%")
                ->orWhere('overview_part_2', 'LIKE', "%$keyword%")
                ->orWhere('our_legacy_description', 'LIKE', "%$keyword%")
                ->orWhere('our_legacy_backgorund_image', 'LIKE', "%$keyword%")
                ->orWhere('key_strength_and_differentiators_main_description', 'LIKE', "%$keyword%")
                ->orWhere('key_strength_and_differentiators_image1', 'LIKE', "%$keyword%")
                ->orWhere('key_strength_and_differentiators_title1', 'LIKE', "%$keyword%")
                ->orWhere('key_strength_and_differentiators_description1', 'LIKE', "%$keyword%")
                ->orWhere('key_strength_and_differentiators_image2', 'LIKE', "%$keyword%")
                ->orWhere('key_strength_and_differentiators_title2', 'LIKE', "%$keyword%")
                ->orWhere('key_strength_and_differentiators_description2', 'LIKE', "%$keyword%")
                ->orWhere('key_strength_and_differentiators_image3', 'LIKE', "%$keyword%")
                ->orWhere('key_strength_and_differentiators_title3', 'LIKE', "%$keyword%")
                ->orWhere('key_strength_and_differentiators_description3', 'LIKE', "%$keyword%")
                ->orWhere('key_strength_and_differentiators_image4', 'LIKE', "%$keyword%")
                ->orWhere('key_strength_and_differentiators_title4', 'LIKE', "%$keyword%")
                ->orWhere('key_strength_and_differentiators_description4', 'LIKE', "%$keyword%")
                ->orWhere('meta_title', 'LIKE', "%$keyword%")
                ->orWhere('meta_keyword', 'LIKE', "%$keyword%")
                ->orWhere('meta_description', 'LIKE', "%$keyword%")
                ->orWhere('meta_image', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $aboutuspages = Aboutuspage::latest()->paginate($perPage);
        }

        return view('admin.aboutuspages.index', compact('aboutuspages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.aboutuspages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
          if ($request->hasFile('desktop_banner')) {
                $name = date('hismdy').$request->file('desktop_banner')->getClientOriginalName();
                 $request->file('desktop_banner')->move(public_path('assets/images/aboutuspage'), $name);
                 $requestData['desktop_banner'] = 'assets/images/aboutuspage/'.$name;
        }
        if ($request->hasFile('mobile_banner')) {
                $name = date('hismdy').$request->file('mobile_banner')->getClientOriginalName();
                 $request->file('mobile_banner')->move(public_path('assets/images/aboutuspage'), $name);
                 $requestData['mobile_banner'] = 'assets/images/aboutuspage/'.$name;
        }
         if ($request->hasFile('our_legacy_backgorund_image')) {
                $name = date('hismdy').$request->file('our_legacy_backgorund_image')->getClientOriginalName();
                 $request->file('our_legacy_backgorund_image')->move(public_path('assets/images/aboutuspage'), $name);
                 $requestData['our_legacy_backgorund_image'] = 'assets/images/aboutuspage/'.$name;
        }
        if ($request->hasFile('key_strength_and_differentiators_image1')) {
                $name = date('hismdy').$request->file('key_strength_and_differentiators_image1')->getClientOriginalName();
                 $request->file('key_strength_and_differentiators_image1')->move(public_path('assets/images/aboutuspage'), $name);
                 $requestData['key_strength_and_differentiators_image1'] = 'assets/images/aboutuspage/'.$name;
        } if ($request->hasFile('key_strength_and_differentiators_image2')) {
                $name = date('hismdy').$request->file('key_strength_and_differentiators_image2')->getClientOriginalName();
                 $request->file('key_strength_and_differentiators_image2')->move(public_path('assets/images/aboutuspage'), $name);
                 $requestData['key_strength_and_differentiators_image2'] = 'assets/images/aboutuspage/'.$name;
        }
        if ($request->hasFile('key_strength_and_differentiators_image3')) {
                $name = date('hismdy').$request->file('key_strength_and_differentiators_image3')->getClientOriginalName();
                 $request->file('key_strength_and_differentiators_image3')->move(public_path('assets/images/aboutuspage'), $name);
                 $requestData['key_strength_and_differentiators_image3'] = 'assets/images/aboutuspage/'.$name;
        } if ($request->hasFile('key_strength_and_differentiators_image4')) {
                $name = date('hismdy').$request->file('key_strength_and_differentiators_image4')->getClientOriginalName();
                 $request->file('key_strength_and_differentiators_image4')->move(public_path('assets/images/aboutuspage'), $name);
                 $requestData['key_strength_and_differentiators_image4'] = 'assets/images/aboutuspage/'.$name;
        }
        if ($request->hasFile('meta_image')) {
                $name = date('hismdy').$request->file('meta_image')->getClientOriginalName();
                 $request->file('meta_image')->move(public_path('assets/images/aboutuspage'), $name);
                 $requestData['meta_image'] = 'assets/images/aboutuspage/'.$name;
        }
        
  

        Aboutuspage::create($requestData);

        return redirect('admin/aboutuspages')->with('flash_message', 'Aboutuspage added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $aboutuspage = Aboutuspage::findOrFail($id);

        return view('admin.aboutuspages.show', compact('aboutuspage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $aboutuspage = Aboutuspage::findOrFail($id);

        return view('admin.aboutuspages.edit', compact('aboutuspage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
              if ($request->hasFile('desktop_banner')) {
                $name = date('hismdy').$request->file('desktop_banner')->getClientOriginalName();
                 $request->file('desktop_banner')->move(public_path('assets/images/aboutuspage'), $name);
                 $requestData['desktop_banner'] = 'assets/images/aboutuspage/'.$name;
        }
        if ($request->hasFile('mobile_banner')) {
                $name = date('hismdy').$request->file('mobile_banner')->getClientOriginalName();
                 $request->file('mobile_banner')->move(public_path('assets/images/aboutuspage'), $name);
                 $requestData['mobile_banner'] = 'assets/images/aboutuspage/'.$name;
        }
         if ($request->hasFile('our_legacy_backgorund_image')) {
                $name = date('hismdy').$request->file('our_legacy_backgorund_image')->getClientOriginalName();
                 $request->file('our_legacy_backgorund_image')->move(public_path('assets/images/aboutuspage'), $name);
                 $requestData['our_legacy_backgorund_image'] = 'assets/images/aboutuspage/'.$name;
        }
        if ($request->hasFile('key_strength_and_differentiators_image1')) {
                $name = date('hismdy').$request->file('key_strength_and_differentiators_image1')->getClientOriginalName();
                 $request->file('key_strength_and_differentiators_image1')->move(public_path('assets/images/aboutuspage'), $name);
                 $requestData['key_strength_and_differentiators_image1'] = 'assets/images/aboutuspage/'.$name;
        } if ($request->hasFile('key_strength_and_differentiators_image2')) {
                $name = date('hismdy').$request->file('key_strength_and_differentiators_image2')->getClientOriginalName();
                 $request->file('key_strength_and_differentiators_image2')->move(public_path('assets/images/aboutuspage'), $name);
                 $requestData['key_strength_and_differentiators_image2'] = 'assets/images/aboutuspage/'.$name;
        }
        if ($request->hasFile('key_strength_and_differentiators_image3')) {
                $name = date('hismdy').$request->file('key_strength_and_differentiators_image3')->getClientOriginalName();
                 $request->file('key_strength_and_differentiators_image3')->move(public_path('assets/images/aboutuspage'), $name);
                 $requestData['key_strength_and_differentiators_image3'] = 'assets/images/aboutuspage/'.$name;
        } if ($request->hasFile('key_strength_and_differentiators_image4')) {
                $name = date('hismdy').$request->file('key_strength_and_differentiators_image4')->getClientOriginalName();
                 $request->file('key_strength_and_differentiators_image4')->move(public_path('assets/images/aboutuspage'), $name);
                 $requestData['key_strength_and_differentiators_image4'] = 'assets/images/aboutuspage/'.$name;
        }
        if ($request->hasFile('meta_image')) {
                $name = date('hismdy').$request->file('meta_image')->getClientOriginalName();
                 $request->file('meta_image')->move(public_path('assets/images/aboutuspage'), $name);
                 $requestData['meta_image'] = 'assets/images/aboutuspage/'.$name;
        }
        $aboutuspage = Aboutuspage::findOrFail($id);
        $aboutuspage->update($requestData);

        return redirect('admin/aboutuspages')->with('flash_message', 'About us page updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Aboutuspage::destroy($id);

        return redirect('admin/aboutuspages')->with('flash_message', 'Aboutuspage deleted!');
    }
}

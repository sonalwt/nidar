<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contactuspage;
use App\Models\Careerpage;
use App\Models\Ourimpactpage;
use App\Models\Successstory;
use App\Models\Ourbrandpage;
use App\Models\Brand;
use App\Models\Buisinessvertical;
use App\Models\Aboutuspage;
use App\Models\Groupinnumber;
use App\Models\Milestone;
use App\Models\Buisinessoverview;
use App\Models\Team;
use App\Models\Leaderteam;
use App\Models\Buisinessverticalspage;
use App\Models\Pressrelease;
use App\Models\Newsroompage;
use App\Models\Videocorner;
use App\Models\Homepage;
use App\Subscribeform;
use App\Contactform;
class FrontendController extends Controller
{
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function contactus()
    {
        $contact=Contactuspage::where('id',1)->first();
        return view('frontend.contact_us',compact('contact'));
    }
     public function privacypolicy()
    {
        return view('frontend.privacy_policy');
    }
    public function legaldisclaimer(){
      return view('frontend.legal_disclaimer');
    }

    public function index(){
      $news1='';
      $home=Homepage::where('id',1)->first();
      if(!empty($home))
      {
         $news1=Pressrelease::where('id',$home->news1)->first();
         $news2=Pressrelease::where('id',$home->news2)->first();
         $news3=Pressrelease::where('id',$home->news3)->first();
      }
      $brands=Brand::where('home_page_status','Enabled')->get();
        $recents=Pressrelease::where('mostly_viewed_status','Enabled')->orderBy('id','desc')->get();
        return view('frontend.index',compact('home','brands','news1','news2','news3','recents'));
    }

     public function career()
    {
        $career=Careerpage::where('id',1)->first();
        return view('frontend.career',compact('career'));
    }

     public function our_impact()
    {
        $impact=Ourimpactpage::where('id',1)->first();
        $stories=Successstory::where('status','Enabled')->get();
        return view('frontend.our_impact',compact('impact','stories'));
    }
    public function our_brands(){
        $brandpage=Ourbrandpage::where('id',1)->first();
        $verticals=Buisinessvertical::where('status','Enabled')->get();
         $brands=Brand::where('status','Enabled')->get();
         //dd($verticals);
          return view('frontend.our_brand',compact('brandpage','verticals','brands'));
    }
    public function about_us(){
         $about=Aboutuspage::where('id',1)->first();
        $verticals=Buisinessvertical::where('status_about_us','Enabled')->get();
         
          return view('frontend.about_us',compact('about','verticals'));
    }
    public function buisiness_overview(){
        $bo=Buisinessoverview::where('id',1)->first();
        $numbers=Groupinnumber::where('status','Enabled')->get();
        $milestones=Milestone::where('status','Enabled')->get();
        return view('frontend.buisiness_overview',compact('bo','numbers','milestones'));
    }
    
    public function leader_team(){
        $leader=Leaderteam::where('id',1)->first();
         $teams=Team::where('status','Enabled')->get();
         return view('frontend.leader_team',compact('leader','teams'));
    }
    public function business_verticals(){
        $bv=Buisinessverticalspage::where('id',1)->first();
        $verticals=Buisinessvertical::where('status_buisiness_vertical','Enabled')->get();
        return view('frontend.buisiness_vertical_page',compact('bv','verticals'));
    }
    public function newsroom(){
        $newsroom=Newsroompage::where('id',1)->first();
        $recents=Pressrelease::where('recent_article_status','Enabled')->get();
        $mosts=Pressrelease::where('mostly_viewed_status','Enabled')->orderBy('id','desc')->get();
        $pverticals=Buisinessvertical::where('status_newsroom_press_page','Enabled')->get();
        // dd($pverticals);
        $vverticals=Buisinessvertical::where('status_newsroom_video_page','Enabled')->get();
        $videos=Videocorner::where('status','Enabled')->get();
        return view('frontend.newsroom',compact('newsroom','pverticals','vverticals','videos','recents','mosts'));
    }
    public function ajax_submit_subscribe_page_form(Request $request){
      $requestData = $request->all();
      Subscribeform::create($requestData);
          echo 1;
    }
    public function ajax_submit_contact_page_form(Request $request){
       $requestData = $request->all();
      Contactform::create($requestData);
          echo 1;
    }
    public function articles(){
       $pverticals=Buisinessvertical::where('status_newsroom_press_page','Enabled')->get();
      $news=Pressrelease::where('status','Enabled')->get();
      return view('frontend.recent_articles',compact('news','pverticals'));
    }
}
